package eu.gaiax.notarization.environment;

import eu.gaiax.notarization.domain.RequestSession;
import eu.gaiax.notarization.domain.Role;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import java.util.Arrays;

@ApplicationScoped
public class PersonManagement {

    @Inject
    RequestManagement requestManagement;

    @Inject
    KeycloakManagement keycloakManagement;

    @Inject
    ProfileManagement profileManagement;

    private Person currentRequestor;
    private Person currentOperator;
    private String operatorProfile;


    public Person iAmARequestor() {
        this.currentRequestor = aRequestor();
        return this.currentRequestor;
    }

    public Person aRequestor() {
        var person = new PersonImpl(Role.Requestor);
        this.currentRequestor = person;
        return this.currentRequestor;
    }

    public Person iAmAnOperator(String profileId) {
        this.currentOperator = anOperator(profileId);
        this.operatorProfile = profileId;
        return this.currentOperator;
    }

    public Person anOperator(String username, String password, String profileId) {
        var person = new PersonImpl(Role.Notary);
        var user = new KeycloakManagement.User(username, password);
        var bearerToken = keycloakManagement.getBearerToken(person, () -> user, Arrays.asList(profileId));
        return anOpertor(person, profileId, bearerToken);
    }

    public Person anOperator(String profileId) {
        var person = new PersonImpl(Role.Notary);
        var bearerToken = keycloakManagement.getBearerToken(person, Arrays.asList(profileId));
        return anOpertor(person, profileId, bearerToken);
    }

    private Person anOpertor(PersonImpl person, String profileId, String bearerToken) {
        var authToken = String.format("Bearer %s", bearerToken);
        person.setAuthToken(authToken);

        this.currentOperator = person;
        this.operatorProfile = profileId;

        return this.currentOperator;
    }

    @Before
    public void before(Scenario scenario) {
        currentRequestor = null;
        currentOperator = null;
    }

    public Person currentRequestor() {
        if (currentRequestor == null) {
            this.currentRequestor = aRequestor();
        }
        return this.currentRequestor;
    }

    public Person currentOperator() {
        if (currentOperator == null) {
            // just an operator without profile or access token
            this.currentOperator = new PersonImpl(Role.Notary);
        }
        return this.currentOperator;
    }

    public String viewableProfile() {
        if (currentOperator == null || operatorProfile == null) {
            throw new IllegalStateException("You're not logged in as operator.");
        }
        return this.operatorProfile;
    }

    private class PersonImpl implements Person {

        private final Role role;
        private String authToken;

        private PersonImpl(Role role) {
            this(role, null);
        }

        private PersonImpl(Role role, String authToken) {
            this.role = role;
            this.authToken = authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        @Override
        public Role role() {
            return role;
        }

        @Override
        public RequestSpecification given() {
            var specBuilder = RestAssured.given();
            if (authToken != null) {
                specBuilder.header("Authorization", authToken);
            }
            else if (role == Role.Requestor) {
                RequestSession currentRequest;
                try {
                    currentRequest = requestManagement.currentRequest(this);
                } catch(IllegalArgumentException ex) {
                    currentRequest = null;
                }
                if (currentRequest != null && currentRequest.token() != null) {
                    specBuilder.header("token", String.format(currentRequest.token()));
                }
            }
            return specBuilder;
        }

    }

}
