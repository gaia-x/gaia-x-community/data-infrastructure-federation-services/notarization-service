package eu.gaiax.notarization.environment;

import java.net.URL;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "acceptance")
public interface Configuration {

    Notarization notarization();
    Profile profile();
    AcaPyHolder holder();
    AcaPyIssuer issuer();
    Revocation revocation();

    Keycloak keycloak();

    String portalProfileId();
    String portalProfileDecryptionKey();
    String profileIdWithoutTasks();
    String profileIdWithIdentificationPrecondition();
    String profileIdAip10();

    public static interface Revocation {
        URL url();
    }
    public static interface Notarization {
        URL url();
    }
    public static interface Profile {
        URL url();
    }
    public static interface AcaPyHolder {
        URL url();
    }
    public static interface AcaPyIssuer {
        URL url();
    }
    public static interface Keycloak {
        URL url();
        Admin admin();

        String realm();

        String clientId();
        String clientSecret();

        public static interface Admin {
            String realm();
            String username();
            String password();
            String clientId();
            String clientSecret();
        }
    }
}
