
# New task type development

Currently, the notarization API supports three task types:

- requestor identification via browser
- requestor identification via verifiable credentials (VC), 
- upload files and validate signatures

The configuration of tasks is described in the admin guide for [profiles](../admin/profiles.md).

The development of a new task type requires the modification or development of the following microservices:

## Profile service

The following changes must be made to support a new task type:

1. Extend the code model
   1. The string value representing the new task type must be added to the enum `TaskType`.
1. Add support in the REST API
   1. The OpenAPI documentation on the enum `TaskType` must include the new task type.
1. Add test
    1. Add unit test(s) validating the support for the new task in the profile configurability.
    1. Add unit test(s) validation the REST support for the new task type.

## Request processing service

1. Adopt the model changes to the profile service.
1. Introduce a new Java interface that inherits the task-related operations from `TaskHandler`: create, cancel, finish successfully, finish with an error.
1. Implement a task handler for the new task type. Either
   1. Implement the new interface for the new task type and ensure it is CDI injectable. Or,
   1. If the successful completion of the new task type leads to the transmission of an identity, then the current functionality in `IdentifyingTaskHandler` can be reused.
      A new factory method, as in `IdentifyingTaskHandlerFactory`, must be developed to configure a new instance of `IdentifyingTaskHandler` for the new task type.
1. The implementation of the new interface must be injected via CDI into `TaskManager`.
   The mapping of `TaskType` to `TaskHandler` in the method `TaskManager.getHandlerByType(TaskType type)` must be extended to map the new task type to the new task interface.
1. Add tests.
