# Container images

For each service a `README` is provided where it is stated how to build container images.

# Java services

The following services are implemented with Java:

* profile
* request-processing
* oidc-identity-resolver
* scheduler
* revocation

For those services some properties are set:

```properties
quarkus.container-image.registry=registry.gitlab.com
quarkus.container-image.group=gaia-x/data-infrastructure-federation-services/not/notarization-service
quarkus.container-image.name=${quarkus.application.name}
```

Those properties are set to push the container images to the Gitlab registry.

The images can also be built by using the `build-container-images.sh` script (see `scripts` folder).
You can build all Java service images by running this script.
You can also build a single image by using the `--service` argument and specifying the service.

The script is also used in the CI/CD pipeline.

# NodeJS services

The following services are implemented with NodeJS:

* ssi-issuance

The images can be build by using the `build-nodejs-images.sh` script (see `scripts` folder).
You can build all NodeJS service images by running this script.
You can also build a single image by using the `--service` argument and specifying the service.
