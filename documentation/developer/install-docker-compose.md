# Installation via Docker Compose

The installation via Docker Compose is only recommended for people developing on the Notarization system and is not supposed to be used in a productive environment.
This kind of installation can be easily used for a local setup of the Notarization system.
Make sure that a current version of Docker Compose is installed (>= 2.5.0), for more information about Docker-Compose, see [here](https://docs.docker.com/compose/install/).

## Using manage script

The script to manage the Docker compose stack is located at `scripts/manage.sh`.

### Synopsis
```bash
$ ./scripts/manage.sh <command> [<options>]
```

### Description

This shell script is used to control the Docker compose stack of the Notarization system.
Use it to start, stop, restart and rebuild parts of the system.

#### Commands

* `scripts/manage.sh start [--local-ssi] [--build] [--force-recreate]`

    Starts the whole stack. Firstly, it starts a local ledger (Indy blockchain), and then, starts the rest of the stack.

  * `--local-ssi` makes the script to skip the creation of the SSI issuance container, assuming it's going to be run locally on the developer machine.
  * `--build` forces the rebuild of docker image (if applicable)
  * `--force-recreate` forces the recreation of docker containers


* `scripts/manage.sh stop [--clean]`

    Stops the stack and shuts down the local ledger.

    * `--clean` removes also the volumes used by the containers. This could be used to remove the saved state of parts of the stack.


* `scripts/manage.sh logs`

    Show logs from docker containers


* `scripts/manage.sh start-ledger`

    Only start the local ledger (Indy blockchain)


* `scripts/manage.sh stop-ledger`

    Only stop the local ledger


* `scripts/manage.sh cli`

    Starts a CLI client for the local ledger. This only could be called if the ledger was started (either with `start` or `start-ledger` commands)


## Third-party components used

| Component                 | Version           |
|---------------------------|-------------------|
| PostgreSQL                | 14                |
| Keycloak                  | 18.0.0            |
| OpenTelemetry Collector   | 0.50.0            |
| JaegerTracing             | 1.34.1            |
| RabbitMQ                  | 3.9.17            |
| Aries Cloudagent          | py36-1.16-1_0.7.4 |
| Digital Signature Service | 5.10.1            |
| Indy Node                 | 1.12-4            |
