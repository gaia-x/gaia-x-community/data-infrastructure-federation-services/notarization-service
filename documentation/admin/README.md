# Admin guide

This guide covers topics that are interesting when installing and configuring the Notarization system. 

## Get started

### Installation guides

Two possible types of installation are represented:

* [Installation via Docker-Compose](../developer/install-docker-compose.md)
* [Installation on Kubernetes](install-k8s.md)

## Monitoring

The monitoring guide can be found [documentation/admin/monitoring.md](./monitoring.md)

## Logging

The logging guide can be found [documentation/admin/logging.md](./logging.md)

## Operations concept

Important information about operating the notarization system can be found [documentation/admin/operations.md](./operations.md)
