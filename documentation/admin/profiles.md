# Profiles

When a business owner wants to submit a `notarization request`, a profile must be chosen. 
In this document, a detailed look at profiles is provided.
A profile defines the AIP (Aries Interop Profile) version, the tasks that must be fulfilled by the business owner and the schema that is used for the JSON data that are passed in a `notarization request`.
Also, the notaries which are responsible for `notarization requests` that use the profile are defined.
A profile can look like this:

```yml
- name: GAIA-X Employee Credential
    aip: "1.0"
    id: demo-gaia-x-employee-credential-v1
    description: This credential is used for employees in the GAIA-X demonstration.
    notaries:
     [...] (see admin documentation for details on this  part)
    is-revocable: true
    valid-for: P1Y
    task-descriptions: >
        [
            {
                "name": "eID-Validation",
                "type": "browserIdentificationTask",
                "description": "Identification via browser using eID means."
            },
            {
                "name": "DocumentUpload",
                "type": "fileProvisionTask",
                "description": "Upload of documents proofing the case."
            }
        ]
    tasks: >
        {
            "taskName" : "DocumentUpload"
        }
    precondition-tasks: >
        {
            "taskName" : "eID-Validation"
        }
    document-template: >
        {
            "evidenceDocument": <documents:{doc|"<doc.sha.hex>"}; separator=" ,">
        }
    template: >
        {
            "attributes": [
                "Claims",
                "FederationName",
                "EmpId",
                "FederationId",
                "EmpEmail",
                "EmpFirstName",
                "EmpLastName",
                "evidenceDocument"
            ]
        }
```

A profile is identified by the `id` property.
This identifier must be used when a business owner is submitting a `notarization request`. 
Only notaries that have the profile ID as role are allowed to view and review `notarization requests` from this profile.

The property `notaries` contains JWKs that are used to encrypt the identity data of the business owner.
The private key is stored by the notary or the notary client.
For more information about the key handling, see [./admin/operations.md](./admin/operations.md).

With the property `is-revocable` it is indicated if the resulting credential can be revoked or not.
The property `valid-for` indicates how long the verifiable credential is valid and can be used.

The properties `task-descriptions`, `tasks` and `precondition-tasks` contain information about the tasks that must be fulfilled by the business owner.
`task-descriptions` are used to define tasks and describe them.
Also, their type is set in this part.
Currently, the following types exist:

| Type | Description |
| ---- | ----------- |
|`browserIdentificationTask`|This type describes a task where the requestor has to identify himself via an authentication process within a browser. An external IdP can be used with this task.|
|`VC-Validation`|This type describes a task where the requester has to identify himself with a verifiable credential.|
|`fileProvisionTask`|This type describes a task for uploading additional documents.|

The development of new task types is outlined in the developer documentation [here](../developer/task-type-development.md).

Tasks which are defined in the `task-description` element can be used within `tasks` and `precondition-tasks` and are referenced by their name.
Both of these elements can contain a tree of tasks that has to be fulfilled before a `notarization request` can be marked as ready.
`precondition tasks` have to be fulfilled before any data can be submitted or `tasks` can be started.
In the profile shown above the trees each contain only one task:

```json
    tasks: >
        {
            "taskName" : "DocumentUpload"
        }
    precondition-tasks: >
        {
            "taskName" : "eID-Validation"
        }
```

A tree is fulfilled when there is one path from root to leaf containing fulfilled tasks. 
Here the `precondition-tasks` would be fulfilled when the eID-Validation is fulfilled.
The possibility to define trees allows however to create more complex scenarios:

```json
 precondition-tasks: >
    {
        "oneOf": [
            { "taskName" : "eID-Validation" },
            { "taskName" : "VC-Validation" }
        ]
    }
```
This would allow the identification via one of both methods and once one of the tasks is fulfilled, the whole tree is fulfilled.
In contrast to `oneOf` it is also possible to define `allOf`, which means that all contained subtrees have to be fulfilled.
Both `oneOf` and `allOf` are lists of trees and can be nested arbitrarily.
It is also possible to define optional tasks, by using `oneOf` in combination with an empty tree node, since empty nodes are regarded as fulfilled:

```json
    tasks: >
    {
        "oneOf": [
            { "taskName" : "DocumentUpload" },
            {}
        ]
    }
```

The `template` property is used to define the schema of the JSON data that are submitted in a `notarization request`.
In the example above, data of an employee is needed.
A `notarization request` for the template above can look like the following:

```json
{
    "data": {
        "Claims" : "admin",
        "FederationName" : "Simple Federation",
        "EmpId" : "5989240124",
        "FederationId": "30129",
        "EmpEmail" : "example@email.com",
        "EmpFirstName" : "Jane",
        "EmpLastName" : "Doe"
    },
    "holder": "did:key:z6MkokM7AWa3dQvhtyLKPp2b4SsHTrAy4CWqquUA9Bmd8Khy",
    "invitation": "http://own.wallet?c_i=eyJAdHlwZSI6ICJkaWQ6c292OkJ"
}
```

The property `document-template` is optional and can be used in conjunction with the `DocumentUpload` task.
With `document-template` it is possible to make the hash of an uploaded document part of the resulting credential.
Thereby, it is possible to define how the hash can look like.
In the example above, `sha` indicates that an SHA-256 hash will be provided.
Currently, there are no other hash methods supported.
In the example above, the SHA-256 hash will be printed in the hex format.
In general, the following formats are available:

* hex
* base64
* base64URLSafe

If the business owner uploaded several documents, the hashes are separated by the separator which is provided in the `document-template` property.

The notarization system also supports chained credentials that allow data in a verifiable credential (VC) to be traced back to its origin while retaining its verifiable quality.
The [Aries RFC 0104](https://github.com/hyperledger/aries-rfcs/blob/main/concepts/0104-chained-credentials/README.md) provides some further information to chained credentials.

Further examples of profiles, including one with chained credentials, are provided [here](./services/profile-config-example.yaml)
