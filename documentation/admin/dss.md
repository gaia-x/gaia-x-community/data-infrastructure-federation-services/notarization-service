# Digital Signature Service

The digital signature service is used to validate document signatures.
A demonstration of the digital signature service is provided [here](https://github.com/esig/dss-demonstrations.git).

## Image Build

There is already an image published (registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/dss-demo-webapp:5.10.1), but you can build your own Docker Image for this service by following the mentioned steps:

```bash
$ git clone https://github.com/esig/dss-demonstrations.git
$ cd dss-demonstrations/
$ mvn clean install -P java17
```

With the following Dockerfile, you can build an image of the digital signature service:

```docker
FROM tomcat:9-jdk17-openjdk-buster

COPY ./dss-demo-webapp/target/dss-demo-webapp-5.10.1.war /usr/local/tomcat/webapps/ROOT.war
```

Finally, you only have to run:

```bash
$ docker build -t registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/dss-demo-webapp:5.10.1 .
```

## Configuration

There is no special configuration for the digital signature service needed.
You only have to set the `QUARKUS_REST_CLIENT_DSS_API_URL` environment variable for the `request-processing` service. 
