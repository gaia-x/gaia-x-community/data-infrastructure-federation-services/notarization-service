# Notarization System Documentation

The Notarization System is documented here.

- [Notarization System Documentation](#notarization-system-documentation)
- [Usage guide](#usage-guide)
- [Administrator guide](#administrator-guide)
- [Developer guide](#developer-guide)
- [eIDAS Bridge](#eidas-bridge)
- [Design Documentation](#design-documentation)
  - [FAQ](#faq)
  - [Glossary](#glossary)

# Usage guide

The usage of the Notarization System as a client, consuming and interacting with the public APIs, is outlined in [usage.md](./usage.md).

# Administrator guide

The documentation to building, installing and operating the notarization system can be found in [admin/README.md](admin/README.md).

# Developer guide

The guide for developers can be found in [developer/README.md](developer/README.md).

# eIDAS Bridge

The guide for integrating an eIDAS Bridge can be found in [eidas-bridge/README.md](eidas-bridge/README.md).

# Design Documentation

The design documentation can be found in [design/README.md](design/README.md).

## FAQ

Frequently asked questions are answered in [faq.md](faq.md).

## Glossary

For the glossary refer to [glossary.md](glossary.md).
