/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.infrastructure.resource;

import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;
import eu.gaiax.notarization.profile.domain.entity.ProfileDid;
import eu.gaiax.notarization.profile.infrastructure.config.MultiProfileTestProfile;
import eu.gaiax.notarization.profile.infrastructure.rest.client.MockRevocationResource;
import eu.gaiax.notarization.profile.infrastructure.rest.client.MockSsiIssuanceResource;
import eu.gaiax.notarization.profile.infrastructure.rest.client.RevocationWireMock;
import eu.gaiax.notarization.profile.infrastructure.rest.client.SsiIssuanceWireMock;
import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import org.junit.jupiter.api.BeforeEach;

import io.quarkus.test.vertx.RunOnVertxContext;
import io.quarkus.test.vertx.UniAsserter;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import javax.ws.rs.core.MediaType;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.*;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
@QuarkusTest
@TestProfile(MultiProfileTestProfile.class)
@QuarkusTestResource(MockSsiIssuanceResource.class)
@QuarkusTestResource(MockRevocationResource.class)
public class RoutinesResourceTest {

    public static Logger logger = Logger.getLogger(RoutinesResourceTest.class);

    private static final String PROFILE_VARIABLE = "profileId";
    private static final String DID_PATH = "/api/v1/profiles/{profileId}/ssi-data";
    private static final String INIT_PROFILES_PATH = "api/v1/routines/request-init-profiles";

    @SsiIssuanceWireMock
    WireMockServer mockSsiIssuance;

    @RevocationWireMock
    WireMockServer mockRevocation;

    @BeforeEach
    @RunOnVertxContext
    @ReactiveTransactional
    public void setUp(UniAsserter asserter) {

        asserter.execute(() -> ProfileDid.deleteAll());

        mockSsiIssuance.resetAll();

        mockSsiIssuance.addMockServiceRequestListener(
                (in, out) -> requestReceived(in, out, "ssi-issuance"));

        mockRevocation.resetAll();

        mockRevocation.addMockServiceRequestListener(
                (in, out) -> requestReceived(in, out, "revocation"));

    }

    @Test
    @RunOnVertxContext
    @ReactiveTransactional
    public void hasPersistedDidsAfterTriggeringInitialization(UniAsserter asserter) {
        stubSsiProfileInitialization();
        stubRevocationProfileInitializationSuccess();

        given()
                .when()
                .post(INIT_PROFILES_PATH)
                .then()
                .statusCode(200)
                .body("totalChanges", is(MultiProfileTestProfile.values.size()));

        asserter.assertEquals(() -> ProfileDid.count(), (long)MultiProfileTestProfile.values.size());
    }

    @Test
    public void verifyRevocationInitializedAfterTriggeringInitialization() {
        stubSsiProfileInitialization();
        stubRevocationProfileInitializationSuccess();

        given()
                .when()
                .post(INIT_PROFILES_PATH)
                .then()
                .statusCode(200)
                .body("totalChanges", is(MultiProfileTestProfile.values.size()));

        this.mockRevocation.verify(postRequestedFor(urlPathEqualTo("/management/lists")).withQueryParam("profile", equalTo(MultiProfileTestProfile.values.get(0).id())));
        this.mockRevocation.verify(postRequestedFor(urlPathEqualTo("/management/lists")).withQueryParam("profile", equalTo(MultiProfileTestProfile.values.get(1).id())));
    }

    @Test
    public void alreadyInitializedRevocationListDoesNotPreventInitialization() {
        stubSsiProfileInitialization();
        stubRevocationProfileInitializationAlreadyCreated(MultiProfileTestProfile.values.get(0).id());
        stubRevocationProfileInitializationSuccess();

        given()
                .when()
                .post(INIT_PROFILES_PATH)
                .then()
                .statusCode(200)
                .body("totalChanges", is(MultiProfileTestProfile.values.size()));
    }

    @Test
    public void canProvideDidsAfterTriggeringInitialization() {
        var givenIssuingDid = "abcd:def:ghi-issuing";
        var givenRevocatingDid = "abcg:def:ghi-revocating";

        stubSsiProfileInitialization(givenIssuingDid, givenRevocatingDid);
        stubRevocationProfileInitializationSuccess();

        given()
                .when()
                .post(INIT_PROFILES_PATH)
                .then()
                .statusCode(200)
                .body("totalChanges", is(MultiProfileTestProfile.values.size()));


        var resultResponse = given()
                .accept(ContentType.JSON)
                .pathParam(PROFILE_VARIABLE, MultiProfileTestProfile.values.get(0).id())
                .when().get(DID_PATH)
                .then()
                .statusCode(200)
                .extract();

        assertThat(resultResponse.path("issuingDid"), is(givenIssuingDid));
        assertThat(resultResponse.path("revocatingDid"), is(givenRevocatingDid));
    }

    @Test
    public void givenUninitializedProfileThenDidIsMissing() {

        given()
                .accept(ContentType.JSON)
                .pathParam(PROFILE_VARIABLE, MultiProfileTestProfile.values.get(0).id())
                .when().get(DID_PATH)
                .then()
                .statusCode(404);
    }

    @Test
    public void failureRevocationListDoesNotPreventInitialization() {
        stubSsiProfileInitialization();
        stubRevocationProfileInitializationStatus(MultiProfileTestProfile.values.get(0).id(), 400);
        stubRevocationProfileInitializationSuccess();

        given()
                .when()
                .post(INIT_PROFILES_PATH)
                .then()
                .statusCode(200)
                .body("totalChanges", is(MultiProfileTestProfile.values.size() - 1));
    }

    @Test
    public void failureDidInitializatoinesNotPreventInitialization() {
        stubSsiProfileInitialization(MultiProfileTestProfile.values.get(0).id(), 400);
        stubSsiProfileInitialization();
        stubRevocationProfileInitializationSuccess();

        given()
                .when()
                .post(INIT_PROFILES_PATH)
                .then()
                .statusCode(200)
                .body("totalChanges", is(MultiProfileTestProfile.values.size() - 1));
    }

    private void stubSsiProfileInitialization() {
        mockSsiIssuance.stubFor(post(urlPathEqualTo("/profile/init"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                        .withBody("""
                                                          {
                                                                  "issuingDid": "abc:def:ghi-issuing",
                                                                  "revocatingDid": "abc:def:ghi-revocating"
                                                          }
                                                          """)
                ).atPriority(100));
    }

    private void stubSsiProfileInitialization(String profileId, int status) {
        mockSsiIssuance.stubFor(post(urlPathEqualTo("/profile/init"))
                .withRequestBody(matchingJsonPath("$.profileID", equalTo(profileId)))
                .willReturn(aResponse()
                        .withStatus(status)
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                        .withBody("""
                                                          {
                                                                  "issuingDid": "abc:def:ghi-issuing",
                                                                  "revocatingDid": "abc:def:ghi-revocating"
                                                          }
                                                          """)
                ).atPriority(5));
    }

    private void stubSsiProfileInitialization(String givenIssuingDid, String givenRevocatingDid) {
        mockSsiIssuance.stubFor(post(urlPathEqualTo("/profile/init"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                        .withBody(String.format("""
                                                                        {
                                                                                "issuingDid": "%s",
                                                                                "revocatingDid": "%s"
                                                                        }
                                                                        """, givenIssuingDid, givenRevocatingDid))
                ));
    }

    private void stubRevocationProfileInitializationSuccess() {
            mockRevocation.stubFor(post(urlPathEqualTo("/management/lists"))
                    .withQueryParam("profile", matching(".+"))
                .willReturn(aResponse()
                        .withStatus(200)
                ).atPriority(100));
    }

    private void stubRevocationProfileInitializationAlreadyCreated(String profileId) {
            mockRevocation.stubFor(post(urlPathEqualTo("/management/lists"))
                    .withQueryParam("profile", equalTo(profileId))
                .willReturn(aResponse()
                        .withStatus(409)
                ).atPriority(5));
    }

    private void stubRevocationProfileInitializationStatus(String profileId, int status) {
            mockRevocation.stubFor(post(urlPathEqualTo("/management/lists"))
                    .withQueryParam("profile", equalTo(profileId))
                .willReturn(aResponse()
                        .withStatus(status)
                ).atPriority(1));
    }

    protected void requestReceived(Request inRequest, Response inResponse, String serviceName) {
        logger.debugv(" WireMock stub {1} request at URL: {0}", inRequest.getAbsoluteUrl(), serviceName);
        logger.debugv(" WireMock stub {1} request headers: \n{0}", inRequest.getHeaders(), serviceName);
        logger.debugv(" WireMock stub {1} request body: \n{0}", inRequest.getBodyAsString(), serviceName);
        logger.debugv(" WireMock stub {1} response body: \n{0}", inResponse.getBodyAsString(), serviceName);
        logger.debugv(" WireMock stub {1} response headers: \n{0}", inResponse.getHeaders(), serviceName);
    }

}
