/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.infrastructure.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.profile.domain.model.Profile;
import static eu.gaiax.notarization.profile.infrastructure.config.ProfileMatcher.equalsProfile;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import javax.inject.Inject;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Neil Crossley
 */
@QuarkusTest
@TestProfile(SingleProfileTestProfile.class)
public class SingleProfileTest {

    private static final String PROFILE_VARIABLE = "profileId";
    private static final String PROFILE_SINGLE_PATH = "/api/v1/profiles/{profileId}";
    private static final String PROFILES_ALL_PATH = "/api/v1/profiles";

    @Inject
    ConfigBackedProfileService profileService;

    @Inject
    ObjectMapper objectMapper;

    @Test
    public void hasSingleProfile() {
        assertThat(profileService.list(), hasSize(1));
    }

    @Test
    public void hasExpectedItem() {
        assertThat(profileService.list(), hasItem(equalsProfile(SingleProfileTestProfile.singleValue)));
    }

    @Test
    public void canFetchAllProfiles() {
        var expected = SingleProfileTestProfile.singleValue;

        given()
                .accept(ContentType.JSON)
                .when().get(PROFILES_ALL_PATH)
                .then()
                .statusCode(200)
                .body("", hasSize(1))
                .body("id", contains(equalTo(expected.id())));
    }

    @Test
    public void canFetchSingleProfile() throws JsonProcessingException {
        var resultResponse = given()
                .accept(ContentType.JSON)
                .pathParam(PROFILE_VARIABLE, SingleProfileTestProfile.singleValue.id())
                .when().get(PROFILE_SINGLE_PATH)
                .then()
                .statusCode(200)
                .extract();

        var resultProfile = objectMapper.readValue(resultResponse.asString(), Profile.class);
        assertThat(resultProfile, equalsProfile(SingleProfileTestProfile.singleValue));
    }

    @Test
    public void cannotFetchUnknownProfile() throws JsonProcessingException {
        final var inputUnknownId = SingleProfileTestProfile.singleValue.id() + "unknown";

        given()
                .accept(ContentType.JSON)
                .pathParam(PROFILE_VARIABLE, inputUnknownId)
                .when().get(PROFILE_SINGLE_PATH)
                .then()
                .statusCode(404);

    }
}
