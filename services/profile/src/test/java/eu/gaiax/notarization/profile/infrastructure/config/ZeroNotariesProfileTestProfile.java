/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.infrastructure.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.profile.domain.model.Profile;
import eu.gaiax.notarization.profile.domain.model.ProfileTaskTree;
import eu.gaiax.notarization.profile.domain.model.AipVersion;
import eu.gaiax.notarization.profile.domain.model.TaskDescription;
import eu.gaiax.notarization.profile.domain.model.TaskType;
import io.quarkus.test.junit.QuarkusTestProfile;
import java.io.StringReader;
import java.time.Period;
import java.util.List;
import java.util.Map;
import static java.util.Map.entry;
import javax.json.Json;
import org.jboss.logging.Logger;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.lang.JoseException;

/**
 *
 * @author Neil Crossley
 */
public class ZeroNotariesProfileTestProfile implements QuarkusTestProfile {

    public static final Profile singleValue;
    public static final Logger logger = Logger.getLogger(ZeroNotariesProfileTestProfile.class);

    private static final PublicJsonWebKey publicKey1;

    static {

        try {
            publicKey1 = PublicJsonWebKey.Factory.newPublicJwk(
                            """
                            {"kty":"EC",
                             "crv":"P-256",
                             "x":"MKBCTNIcKUSDii11ySs3526iDZ8AiTo7Tu6KPAqv7D4",
                             "y":"4Etl6SRW2YiLUrN5vfvVHuhp7x8PxltmWWlbbM4IFyM",
                             "use":"enc",
                             "kid":"1"}
                            """);
        } catch (JoseException ex) {
            throw new RuntimeException(ex);
        }
        singleValue = new Profile("someId",
                AipVersion.V2_0,
                "some Name",
                "some description",
                "A256GCM",
                List.of(),
                Period.ofYears(1),
                true,
                Json.createReader(
                        new StringReader("""
                                         {"something":"other"}
                                         """)).readObject(),
                null,
                List.of(
                    new TaskDescription("name", TaskType.BROWSER_IDENTIFICATION_TASK, "desc"),
                    new TaskDescription("name2", TaskType.BROWSER_IDENTIFICATION_TASK, "desc")
                ),
                new ProfileTaskTree(),
                new ProfileTaskTree()
        );
    }

    private static ObjectMapper mapper = new ObjectMapper();
    @Override
    public Map<String, String> getConfigOverrides() {
        try{

            var prefix = "gaia-x.profile.config[0].";
            return Map.ofEntries(
                entry(prefix + "id", singleValue.id()),
                entry(prefix + "aip", singleValue.aip().toString()),
                entry(prefix + "name", singleValue.name()),
                entry(prefix + "description", singleValue.description()),
                entry(prefix + "valid-for", singleValue.validFor().toString()),
                entry(prefix + "template", singleValue.template().toString()),
                entry(prefix + "tasks", mapper.writeValueAsString(singleValue.tasks())),
                entry(prefix + "precondition-tasks", mapper.writeValueAsString(singleValue.preconditionTasks())),
                entry(prefix + "task-descriptions", mapper.writeValueAsString(singleValue.taskDescriptions()))
            );
        } catch (JsonProcessingException ex) {
            throw new RuntimeException(ex);
        }
    }
}
