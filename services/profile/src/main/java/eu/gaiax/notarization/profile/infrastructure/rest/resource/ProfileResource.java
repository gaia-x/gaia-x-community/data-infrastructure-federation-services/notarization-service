/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.infrastructure.rest.resource;

import com.fasterxml.jackson.databind.JsonNode;
import eu.gaiax.notarization.profile.domain.entity.ProfileDid;
import eu.gaiax.notarization.profile.domain.exception.UnknownProfileException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jboss.resteasy.reactive.ResponseStatus;

import eu.gaiax.notarization.profile.domain.model.Profile;
import eu.gaiax.notarization.profile.domain.service.ProfileService;
import eu.gaiax.notarization.profile.infrastructure.rest.Api;
import io.smallrye.mutiny.Uni;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;

/**
 *
 * @author Neil Crossley
 */
@Path(Api.Path.PROFILE_RESOURCE)
public class ProfileResource {

    private final ProfileService profileService;

    public ProfileResource(ProfileService profileService) {
        this.profileService = profileService;
    }

    @ServerExceptionMapper
    public RestResponse<String> mapException(UnknownProfileException x) {
        return RestResponse.status(Response.Status.NOT_FOUND, String.format("Unknown profile %s", x.profileId));
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "All profiles", description = "Fetches all available profiles.")
    @ResponseStatus(200)
    @APIResponse(responseCode = "200", description = "A list of all profiles.", content = @Content(schema = @Schema(implementation = Profile.class)))
    public List<Profile> list() {
        return profileService.list();
    }

    @GET
    @Path(Api.Param.PROFILE_ID_PARAM)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Single profile", description = "Fetches a single profile.")
    @ResponseStatus(200)
    @APIResponse(responseCode = "200", description = "A single profile.", content = @Content(schema = @Schema(implementation = Profile.class)))
    public Profile fetchProfile(@RestPath(Api.Param.PROFILE_ID) String identifier) throws UnknownProfileException {
        var found = profileService.fetchProfile(identifier);
        if (found == null) {
            throw new UnknownProfileException(identifier);
        }
        return found;
    }

    @GET
    @Path(Api.Param.PROFILE_ID_PARAM + "/ssi-data")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "DID summary of a profile", description = "Fetches the DID summary for the identified profile.")
    @ResponseStatus(200)
    @APIResponse(responseCode = "200", description = "A DID summary of a profile.", content = @Content(schema = @Schema(type = SchemaType.OBJECT)))
    public Uni<JsonNode> fetchDids(@RestPath(Api.Param.PROFILE_ID) String identifier) throws UnknownProfileException {

        return ProfileDid.findByProfileId(identifier).map(profile -> profile.issuanceContent);
    }
}
