/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.infrastructure.config;

import eu.gaiax.notarization.profile.domain.model.ProfileTaskTree;
import eu.gaiax.notarization.profile.domain.model.AipVersion;
import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithConverter;
import io.smallrye.config.WithDefault;
import java.time.Period;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.json.JsonArray;
import javax.json.JsonObject;
import org.jose4j.jwk.PublicJsonWebKey;

/**
 *
 * @author Neil Crossley
 */
@ConfigMapping(prefix = "gaia-x.profile")
public interface ProfileSourceConfig {

    Map<String, String> defaultAlgorithms();

    List<ProfileConfig> config();

    public interface ProfileConfig {

        String id();

        @WithConverter(AipVersionConverter.class)
        AipVersion aip();

        String name();

        String description();

        @WithDefault(value = "A256GCM")
        String encryption();

        List<NotaryAccessConfig> notaries();

        Period validFor();

        JsonObject template();

        Optional<String> documentTemplate();

        JsonArray taskDescriptions();

        @WithDefault(value = "true")
        boolean isRevocable();

        @WithConverter(ProfileTaskTreeConverter.class)
        ProfileTaskTree tasks();
        @WithConverter(ProfileTaskTreeConverter.class)
        ProfileTaskTree preconditionTasks();
    }

    public interface NotaryAccessConfig {
        Optional<String> algorithm();
        PublicJsonWebKey jwk();
    }
}
