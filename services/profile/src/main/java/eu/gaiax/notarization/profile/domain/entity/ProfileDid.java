/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.domain.entity;

import com.fasterxml.jackson.databind.JsonNode;
import eu.gaiax.notarization.profile.domain.exception.UnknownProfileException;
import io.quarkiverse.hibernate.types.json.JsonType;
import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.smallrye.mutiny.Uni;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NoResultException;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

/**
 *
 * @author Neil Crossley
 */
@Entity
@TypeDef(name = "json", typeClass = JsonType.class)
public class ProfileDid extends PanacheEntityBase {

    @Id
    public UUID id;

    @Column(unique = true)
    public String profileId;

    @Type(type = "json")
    @Column(columnDefinition = "jsonb")
    public JsonNode issuanceContent;

    @ReactiveTransactional
    public static Uni<ProfileDid> findByProfileId(String profileId) {
        return ProfileDid.<ProfileDid>find("profileId", profileId).singleResult()
                .onFailure(NoResultException.class).transform((t) -> new UnknownProfileException(profileId, t));
    }
}
