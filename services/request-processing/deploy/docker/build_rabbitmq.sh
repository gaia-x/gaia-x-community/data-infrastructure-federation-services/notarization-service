#!/bin/bash

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

docker build -f "${SCRIPT_DIR}/Dockerfile.rabbitmq" -t registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/rabbitmq-dev:3.9-management .
