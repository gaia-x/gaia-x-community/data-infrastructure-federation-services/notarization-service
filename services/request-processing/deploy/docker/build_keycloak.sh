#!/bin/bash

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

docker build -f "${SCRIPT_DIR}/Dockerfile.keycloak" -t registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/keycloak-jq:18.0.0 .
