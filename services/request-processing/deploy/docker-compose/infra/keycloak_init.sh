#!/bin/bash

# SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

echo "Waiting for the keycloak service to be initialised..."


HEALTHCHECK_ENDPOINT="http://${KC_HOSTNAME}:${KEYCLOAK_PORT}/realms/master"
echo "HTTP $HEALTHCHECK_ENDPOINT"

curl --ipv4 --retry 30 --retry-delay 5 --retry-connrefused "$HEALTHCHECK_ENDPOINT"

echo "Starting Provisioing"

cd /opt/keycloak/bin || exit 1

#Authenticate with the Admin Server
./kcadm.sh config credentials --server "http://${KC_HOSTNAME}:${KEYCLOAK_PORT}" --realm master --user "${KEYCLOAK_ADMIN}" --password ${KEYCLOAK_ADMIN_PASSWORD}

#Create Realm Notarization-Realm
./kcadm.sh create realms -s realm=notarization-realm -s enabled=true

#Create Notarizatoin-API Client
./kcadm.sh create clients -r notarization-realm -s clientId=notarization-client -s bearerOnly="true" -s enabled=true -s directAccessGrantsEnabled=true -s clientAuthenticatorType=client-secret -s secret=notarization-api-secret-12345

#Create Portal Client
./kcadm.sh create clients -r notarization-realm -s clientId=portal-client -s bearerOnly="false" -s enabled=true -s directAccessGrantsEnabled=true -s implicitFlowEnabled=true -s clientAuthenticatorType=client-secret -s secret=portal-secret-12345 -s 'redirectUris=["http://localhost:8085/login"]'

user_index=1
while [ "$user_index" -le 1 ]
do
    username="notary-0${user_index}"
    # Create notary user

    echo "Creating user ${username}"

    ./kcadm.sh create users -r notarization-realm -s username="${username}" -s enabled=true -s email="${username}@email.com"
    #Set password
    ./kcadm.sh set-password -r notarization-realm --username "${username}" --new-password "${username}-pw"

    # Create role representing a notarization config profile
    profilename="profile-0${user_index}"

    echo "Creating role ${profilename}"
    ./kcadm.sh create roles -r notarization-realm -s name="${profilename}"

    # Give new role and all previous roles to current user 
    profile_index=1
    while [ "${profile_index}" -le "${user_index}" ]
    do
        profilename="profile-0${profile_index}"

        echo "Assigning role ${profilename} to ${username}"
        ./kcadm.sh add-roles --uusername "${username}" --rolename "profile-0${profile_index}" -r notarization-realm
        ((profile_index++))
    done

    ((user_index++))
done

# Quarkus does not support claims in nested/deep structures.
# The following changes the mapped name of the realm roles to work with quarkus.

# Find the client scopes with the new roles.
client_scope_oidc_roles=$(./kcadm.sh get -x "client-scopes" -r notarization-realm --fields "id,name,description,protocolMappers(id,name,config(*))" | jq -c 'map(select(.name == "roles"))' | jq '.[0]')

client_scope_oidc_roles_id=$(echo $client_scope_oidc_roles | jq -r '.id')

# Find the protocol mapper used for the realm roles.
realm_role_mapper_id=$(echo $client_scope_oidc_roles | jq -r '.protocolMappers[] | select(.name == "realm roles") | .id')

echo "Changing the claim name of the realm roles to 'roles'"

# Change the mapped name of the claims from the default "realm_access.roles" to "roles".
./kcadm.sh update "client-scopes/${client_scope_oidc_roles_id}/protocol-mappers/models/${realm_role_mapper_id}" -r notarization-realm -s "config.\"claim.name\"=\"roles\""

echo "Completed initialisation of keycloak"
