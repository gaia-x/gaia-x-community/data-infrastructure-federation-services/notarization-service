/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.model.taskprocessing;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.gaiax.notarization.request_processing.domain.model.serialization.TaskIdDeserializer;
import eu.gaiax.notarization.request_processing.domain.model.serialization.TaskIdSerializer;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author Florian Otto
 */
@Schema(type = SchemaType.STRING, description = "The identifier of a task. "
        + "The task description is found in the profile of a notarization request.")
@JsonDeserialize(using = TaskIdDeserializer.class)
@JsonSerialize(using = TaskIdSerializer.class)
public record TaskId(@NotNull UUID id) {
    public TaskId(String id){
        this(UUID.fromString(id));
    }
}
