/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.client;

import eu.gaiax.notarization.request_processing.domain.exception.NotFoundException;
import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import eu.gaiax.notarization.request_processing.domain.services.RevocationService;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.ListMapping;
import io.quarkus.cache.CacheResult;
import io.smallrye.mutiny.Uni;
import java.util.List;

/**
 *
 * @author Florian Otto
 */
@ApplicationScoped
public class RevocationServiceClient implements RevocationService {

    private final RevocationServiceRestClient revocationClient;

    public RevocationServiceClient(
        @RestClient RevocationServiceRestClient profileClient
    ) {
        this.revocationClient = profileClient;
    }

    @Override
    @CacheResult(cacheName = "listcred-profile-mapping-list")
    public Uni<List<ListMapping>> getList() {
        return revocationClient.getList();
    }

    @Override
    public Uni<Void> revoke(String profileName, Long idx) {
        return revocationClient.revoke(profileName, idx);
    }

    @CacheResult(cacheName = "listcred-profile-mapping-name-resolved")
    public Uni<String> getProfileName(String listName){
        return revocationClient.getList()
            .onItem().transform(l -> {
                return l.stream()
                    .filter((e) -> e.listName.equals(listName))
                    .map(m -> m.profileName)
                    .findFirst()
                    .orElseThrow(()-> new NotFoundException("Associated profile")
            );
        });
    }
}
