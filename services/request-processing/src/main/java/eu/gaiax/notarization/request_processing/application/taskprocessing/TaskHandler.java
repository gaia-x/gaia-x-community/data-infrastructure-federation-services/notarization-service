/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.application.taskprocessing;

import com.fasterxml.jackson.databind.JsonNode;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.entity.SessionTask;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskId;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskInstance;
import io.smallrye.mutiny.Uni;

/**
 *
 * @author Florian Otto
 */
public interface TaskHandler {

    public Uni<TaskInstance> createTask(TaskId id, JsonNode data, Session profile);
    public Uni<Void> cancelTask(TaskId id);

    public Uni<Void> finishTaskSuccess(SessionTask sessionTask, JsonNode data);
    public Uni<Void> finishTaskFail(SessionTask sessionTask, JsonNode data);

}
