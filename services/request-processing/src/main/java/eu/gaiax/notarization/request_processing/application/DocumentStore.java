/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.application;

import java.util.Objects;

import javax.enterprise.context.ApplicationScoped;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.request_processing.domain.entity.DocumentStoreDocument;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.exception.AuthorizationException;
import eu.gaiax.notarization.request_processing.domain.exception.InvalidRequestStateException;
import eu.gaiax.notarization.request_processing.domain.exception.NotFoundException;
import eu.gaiax.notarization.request_processing.domain.model.DocumentId;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskId;
import eu.gaiax.notarization.request_processing.domain.services.DownloadService;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.DocumentUploadByLink;
import eu.gaiax.notarization.request_processing.infrastructure.rest.resource.NotarizationRequestSubmissionResource.AccessPathParameters;
import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.smallrye.mutiny.Uni;
import java.util.Base64;
import java.util.UUID;
import eu.gaiax.notarization.request_processing.application.taskprocessing.TaskManager;
import eu.gaiax.notarization.request_processing.domain.entity.OngoingDocumentTask;
import eu.gaiax.notarization.request_processing.domain.exception.BadParameterException;
import eu.gaiax.notarization.request_processing.domain.exception.BusinessException;
import eu.gaiax.notarization.request_processing.domain.exception.InvalidFileException;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.DocumentUpload;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.DocumentView;
import io.vertx.pgclient.PgException;
import java.io.IOException;
import java.nio.file.Files;
import javax.persistence.PersistenceException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FilenameUtils;
import org.hibernate.HibernateException;

/**
 *
 * @author Florian Otto
 */
@ApplicationScoped
public class DocumentStore {

    private final ObjectMapper objectMapper;
    private final DownloadService downloadService;
    private final VerificationService verificationService;
    private final Base64.Encoder encoder = Base64.getUrlEncoder();
    private final TaskManager taskManager;

    public DocumentStore(
        ObjectMapper objectMapper,
        DownloadService downloadService,
        VerificationService verificationService,
        TaskManager taskManager
    ) {
        this.objectMapper = objectMapper;
        this.downloadService = downloadService;
        this.verificationService = verificationService;
        this.taskManager = taskManager;
    }

    private void checkTokenAndState(Session dbSession, AccessPathParameters params) {
        if (!Objects.equals(dbSession.accessToken, params.asSessionInfo().token().token)) {
            throw new AuthorizationException("Not authorized", params.asSessionInfo().id());
        }
        if (!dbSession.state.isUploadDocumentAllowed()) {
            throw new InvalidRequestStateException(params.asSessionInfo().id(), NotarizationRequestState.uploadDocumentsStates, dbSession.state);
        }
    }

    @ReactiveTransactional
    public Uni<Void> startUploadTask(TaskId taskId){
        var ongoingTask = new OngoingDocumentTask();
        ongoingTask.taskId = taskId.id();
        return ongoingTask
            .persistAndFlush()
            .replaceWithVoid()
            ;

    }

    @ReactiveTransactional
    public Uni<Void> cancelTask(TaskId taskId){
        return
            Uni.createFrom().voidItem()
            .chain(()-> OngoingDocumentTask.deleteById(taskId.id()))
            .chain(()-> {
                return DocumentStoreDocument.delete("taskId", taskId.id());
            })
            .replaceWithVoid()
            ;
    }


    @ReactiveTransactional
    public Uni<Void> upload(DocumentUpload document, TaskId taskId, AccessPathParameters accessParams){
        return Session.<Session>findById(accessParams.asSessionInfo().id().id)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .chain(session -> {
                checkTokenAndState(session, accessParams);
                return OngoingDocumentTask.<OngoingDocumentTask>findById(taskId.id())
                        .onItem().ifNull().failWith(() -> new NotFoundException("Task"))
                        .onItem().ifNotNull().transformToUni((ot) -> {
                        var doc = new DocumentStoreDocument();
                        doc.id = document.id.id;
                        doc.taskId = taskId.id();
                        try {
                            doc.content = Files.readAllBytes(document.content.uploadedFile());
                        } catch (IOException ex) {
                            throw new InvalidFileException("The given file could not be processed", ex);
                        }
                        doc.title = document.title;
                        doc.mimetype = document.content.contentType();
                        doc.extension = FilenameUtils.getExtension(document.content.fileName());
                        doc.title = document.title;
                        doc.shortDescription = document.shortDescription;
                        doc.longDescription = document.longDescription;

                        return verificationService.verify(doc.content)
                            .chain((verificationResult)->{
                                doc.hash = Hex.encodeHexString(verificationResult.digestResult());
                                doc.verificationReport = encoder.encodeToString(verificationResult.verificationReport());
                                return doc.persistAndFlush()
                                    .onFailure(PersistenceException.class).transform(e -> {
                                        var cause = e.getCause();
                                        if (cause instanceof HibernateException) {
                                            cause = cause.getCause();
                                            if (cause instanceof PgException) {
                                                var code = ((PgException) cause).getCode();
                                                if ("23505".equals(code)) {
                                                    return new BadParameterException("DocumentId exists.");
                                                }
                                            }
                                        }
                                        return e;
                                    })
                                    .replaceWithVoid()
                                    ;
                            });
                    });
            });

    }

    @ReactiveTransactional
    public Uni<Void> uploadByLink(DocumentUploadByLink document, TaskId taskId, AccessPathParameters accessParams){
        return Session.<Session>findById(accessParams.asSessionInfo().id().id)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .chain(session -> {
                checkTokenAndState(session, accessParams);
                return OngoingDocumentTask.<OngoingDocumentTask>findById(taskId.id())
                    .onItem().ifNull().failWith(() -> new NotFoundException("Task"))
                    .onItem().ifNotNull().transformToUni((ot) -> {
                        var doc = new DocumentStoreDocument();
                        doc.id = document.id.id;
                        doc.taskId = taskId.id();
                        doc.title = document.title;
                        doc.mimetype = document.mimetype;
                        doc.extension = document.extension;
                        doc.shortDescription = document.shortDescription;
                        doc.longDescription = document.longDescription;

                        return downloadService.download(document.location)
                            .onFailure().recoverWithNull()
                            .onItem().ifNull().failWith(()-> new BadParameterException("Downloadlink not valid"))
                            .chain((fileDownload)-> {
                                try{
                                    doc.content = fileDownload.readAllBytes();
                                    fileDownload.reset();
                                } catch (IOException e){
                                    throw new BusinessException("Problem downloading file.");
                                }

                                return verificationService.verify(fileDownload);
                            })
                            .chain((verificationResult)->{
                                doc.hash = Hex.encodeHexString(verificationResult.digestResult());
                                doc.verificationReport = encoder.encodeToString(verificationResult.verificationReport());
                                return doc.persistAndFlush()
                                    .onFailure(PersistenceException.class).transform(e -> {
                                        var cause = e.getCause();
                                        if (cause instanceof HibernateException) {
                                            cause = cause.getCause();
                                            if (cause instanceof PgException) {
                                                var code = ((PgException) cause).getCode();
                                                if ("23505".equals(code)) {
                                                    return new BadParameterException("DocumentId exists.");
                                                }
                                            }
                                        }
                                        return e;
                                    })
                                    .replaceWithVoid()
                                    ;
                            });

                    });

            });
    }

    @ReactiveTransactional
    public Uni<DocumentView> get(UUID documentId, AccessPathParameters accessParams) {
        return Session.<Session>findById(accessParams.asSessionInfo().id().id)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .chain(session -> {
                checkTokenAndState(session, accessParams);
                return DocumentStoreDocument.<DocumentStoreDocument>findById(documentId)
                    .onItem().ifNull().failWith(() -> new NotFoundException("Document"))
                    .map(storedDoc -> {
                        var res = new DocumentView();
                        res.id = new DocumentId(storedDoc.id);
                        res.title = storedDoc.title;
                        res.shortDescription = storedDoc.shortDescription;
                        res.longDescription = storedDoc.longDescription;
                        return res;
                    });
            });
    }

    @ReactiveTransactional
    public Uni<Void> delete(UUID documentId, AccessPathParameters accessParams) {
        return Session.<Session>findById(accessParams.asSessionInfo().id().id)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .chain(session -> {
                checkTokenAndState(session, accessParams);
                return DocumentStoreDocument.deleteById(documentId).replaceWithVoid();
            });
    }

    @ReactiveTransactional
    public Uni<Void> finish(TaskId taskId, AccessPathParameters accessParams) {
        return Session.<Session>findById(accessParams.asSessionInfo().id().id)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .chain(session -> {
                checkTokenAndState(session, accessParams);
                return DocumentStoreDocument.<DocumentStoreDocument>find("taskId", taskId.id())
                    .stream().collect().asList()
                    .chain((docs)->{
                        var jNode = objectMapper.convertValue(docs, JsonNode.class);
                        return taskManager.finishTaskSuccess(taskId, jNode);
                    });
            });
    }
}
