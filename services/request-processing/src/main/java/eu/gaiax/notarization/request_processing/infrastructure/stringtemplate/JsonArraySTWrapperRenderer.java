/*
 *
 */
package eu.gaiax.notarization.request_processing.infrastructure.stringtemplate;

import java.util.Locale;
import org.stringtemplate.v4.AttributeRenderer;

/**
 *
 * @author Neil Crossley
 */
public class JsonArraySTWrapperRenderer implements AttributeRenderer<JsonArraySTWrapper> {

    @Override
    public String toString(JsonArraySTWrapper wrapper, String formatString, Locale locale) {
        var model = wrapper.json();
        return model.encode();
    }

}
