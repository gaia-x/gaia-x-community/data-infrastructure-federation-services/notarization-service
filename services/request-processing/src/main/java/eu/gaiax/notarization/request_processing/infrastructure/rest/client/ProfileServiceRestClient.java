/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.client;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import eu.gaiax.notarization.request_processing.domain.exception.NotFoundException;
import eu.gaiax.notarization.request_processing.domain.model.Profile;
import io.quarkus.rest.client.reactive.ClientExceptionMapper;
import io.smallrye.mutiny.Uni;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;

/**
 *
 * @author Neil Crossley
 */
@Path("/api/v1/profiles")
@RegisterRestClient(configKey = "profile-api")
public interface ProfileServiceRestClient {

    @GET
    @Path("/{profileId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @ClientHeaderParam(name = "Accept", value = MediaType.APPLICATION_JSON)
    Uni<Profile> findProfileAsUni(@PathParam("profileId") String profileId);

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @ClientHeaderParam(name = "Accept", value = MediaType.APPLICATION_JSON)
    Uni<List<Profile>> findAllProfilesAsUni();

    @ClientExceptionMapper
    static NotFoundException toException(Response response) {
        if (response.getStatus() == 404) {
            return new NotFoundException("The remote service responded with HTTP 404");
        }
        return null;
    }
}
