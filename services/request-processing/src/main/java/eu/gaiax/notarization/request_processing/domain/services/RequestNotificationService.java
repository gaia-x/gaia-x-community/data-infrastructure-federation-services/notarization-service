/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.services;

import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestId;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.domain.model.SessionId;
import java.net.URI;

public interface RequestNotificationService {

    void onReadyForReview(NotarizationRequestId request, ProfileId profile);

    void onRejected(SessionId identifier);

    void onPendingDid(SessionId identifier);

    void onAccepted(SessionId identifier);
    void onAccepted(SessionId identifier, String invite);

    void onDeleted(SessionId identifier);

    void onTerminated(SessionId identifier);

    void onExternalTask(SessionId identifier, URI redirect);

    void onContactUpdate(SessionId identifier, String contact);

}
