/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author Florian Otto
 */
@Schema(implementation = ProfileTaskTree.TaskTreeNode.class)
public class ProfileTaskTree {

        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        public Set<ProfileTaskTree> allOf;
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        public Set<ProfileTaskTree> oneOf;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        public String taskName;

        public ProfileTaskTree(){
            this.allOf = new HashSet<>();
            this.oneOf= new HashSet<>();
            this.taskName = null;
        }

        public boolean isEmpty(){
            return
                this.taskName == null
                && this.allOf.isEmpty()
                && this.oneOf.isEmpty()
                ;
        }

        public boolean containsTaskByName(String taskName){
            if(this.taskName != null && this.taskName.equals(taskName)){
                return true;
            } else {
                return Stream.concat(allOf.stream(), oneOf.stream())
                    .filter(childTree -> childTree.containsTaskByName(taskName))
                    .findAny()
                    .isPresent()
                    ;
            }

        }

    public boolean treeFulfilledBySession(Session s){
        var calcTree = TaskTreeForCalculating.buildTree(
            this,
            s.tasks
        );
        return calcTree.fulfilled();
    }

    @Schema(
        oneOf = {
        ProfileTaskTree.NodeEmpty.class,
        ProfileTaskTree.NodeAllOf.class,
        ProfileTaskTree.NodeOneOf.class,
        ProfileTaskTree.NodeTask.class})
    public static class TaskTreeNode {

    }

    @Schema(example = """
                      {  }
                      """)
    public static class NodeEmpty {

    }

    @Schema(example = """
                      { "oneOf": [
                          { "taskName": "vcIdentification" },
                          { "taskName": "eID" }
                      ] }
                      """)
    public static class NodeOneOf {

        @Schema(required = true)
        public List<TaskTreeNode> oneOf;
    }
    @Schema(example = """
                      { "allOf": [
                          { "taskName": "eID" },
                          { "taskName": "signedFormUpload" }
                      ] }
                      """)
    public static class NodeAllOf {
        @Schema(required = true)
        public List<TaskTreeNode> allOf;
    }
    @Schema(example = """
                      { "taskName": "eID" }
                      """)
    public static class NodeTask {
        @Schema(required = true)
        public String taskName;
    }
}
