/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.mappers;

import eu.gaiax.notarization.request_processing.domain.exception.InvalidRequestStateException;
import eu.gaiax.notarization.request_processing.infrastructure.rest.Api;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mappers.problem_details.InvalidRequestStateProblemDetails;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mappers.problem_details.ProblemDetails;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestResponse;
import org.jboss.resteasy.reactive.RestResponse.ResponseBuilder;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;

/**
 *
 * @author Neil Crossley
 */
@Provider
public class InvalidRequestStateMapper implements ExceptionMapper<InvalidRequestStateException> {

    private static final Logger logger = Logger.getLogger(InvalidRequestStateMapper.class);

    @Override
    @APIResponse(responseCode = "400", description = "Invalid Notarization Request State",
            content = {
                @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = InvalidRequestStateProblemDetails.class))
            }
    )
    @Produces(MediaType.APPLICATION_JSON)
    public Response toResponse(InvalidRequestStateException x) {
        return Response.status(InvalidRequestStateProblemDetails.STATUS)
                .entity(asDetails(x))
                .header(Api.Param.SESSION, x.id.id)
                .build();
    }

    @ServerExceptionMapper
    public RestResponse<ProblemDetails> mapException(InvalidRequestStateException x) {

        return ResponseBuilder.<ProblemDetails>create(InvalidRequestStateProblemDetails.STATUS, asDetails(x))
            .header(Api.Param.SESSION, x.id.id)
            .build();
    }

    private static InvalidRequestStateProblemDetails asDetails(InvalidRequestStateException x) {
        logger.debug("Handled invalid request state exception", x);
        return new InvalidRequestStateProblemDetails(
                ProblemDetails.ABOUT_BLANK, "Invalid Notarization Request State", x.getMessage(), null, x.expected, x.actual);
    }

}
