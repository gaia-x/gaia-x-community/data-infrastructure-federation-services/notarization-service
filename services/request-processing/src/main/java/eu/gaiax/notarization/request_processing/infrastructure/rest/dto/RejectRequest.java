/*
 *
 */
package eu.gaiax.notarization.request_processing.infrastructure.rest.dto;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author Neil Crossley
 */
public record RejectRequest(@Schema(description = "The reason that the notarization request is rejected") String reason) {
}
