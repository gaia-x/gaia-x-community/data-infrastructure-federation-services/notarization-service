
package eu.gaiax.notarization.request_processing.infrastructure.rest.dto;

import java.net.URI;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author Florian Otto
 */
public record InvitationUrlResponse(
        @Schema(description = "The invitation URL is present if the issuer began issuing the credential, but an invitation URL from the holder was not provided.") URI inviteUrl){}
