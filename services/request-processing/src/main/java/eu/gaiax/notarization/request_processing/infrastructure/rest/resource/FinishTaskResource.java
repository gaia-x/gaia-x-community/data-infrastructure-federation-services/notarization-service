/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.resource;

import com.fasterxml.jackson.databind.JsonNode;
import eu.gaiax.notarization.request_processing.application.taskprocessing.TaskManager;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.infrastructure.rest.Api;
import io.smallrye.mutiny.Uni;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import javax.ws.rs.PathParam;
import static eu.gaiax.notarization.request_processing.infrastructure.rest.Api.Path.FINISH_TASK_RESOURCE_WITH_NONCE;
import eu.gaiax.notarization.request_processing.infrastructure.rest.feature.audit.Auditable;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;

/**
 *
 * @author Florian Otto
 */
@Tag(name = Api.Tags.FINISH_TASK)
@Path(FINISH_TASK_RESOURCE_WITH_NONCE)
public class FinishTaskResource {

    @Inject
    TaskManager taskManager;

    @Auditable(action = NotarizationRequestAction.TASK_FINISH_SUCCESS)
    @POST
    @Path(Api.Path.SUCCESS)
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The task completion was successfully processed.")
    @RequestBody(description = "The task-specific payload upon success. For example, for an identification task, this would be the identity of the requestor.")
    public Uni<Void> success(
            @PathParam(Api.Param.NONCE) String nonce,
            JsonNode data
    ) {
        return taskManager.finishTaskSuccess(nonce, data);
    }

    @Auditable(action = NotarizationRequestAction.TASK_FINISH_FAIL)
    @POST
    @Path(Api.Path.FAIL)
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The task failure was successfully processed.")
    @RequestBody(description = "The task-specific payload upon success, if any.")
    public Uni<Void> fail(
            @PathParam(Api.Param.NONCE) String nonce,
            JsonNode data
    ) {
        return taskManager.finishTaskFail(nonce, data);
    }
}
