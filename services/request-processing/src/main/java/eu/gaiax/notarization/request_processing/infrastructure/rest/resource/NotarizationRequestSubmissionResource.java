/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.resource;

import java.util.UUID;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.fasterxml.jackson.databind.JsonNode;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirements;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestHeader;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestQuery;
import org.jboss.resteasy.reactive.RestResponse;
import org.jboss.resteasy.reactive.RestResponse.ResponseBuilder;

import eu.gaiax.notarization.request_processing.application.NotarizationRequestStore;
import eu.gaiax.notarization.request_processing.application.domain.SessionInfo;
import eu.gaiax.notarization.request_processing.domain.exception.AuthorizationException;
import eu.gaiax.notarization.request_processing.domain.exception.CannotReuseTokenException;
import eu.gaiax.notarization.request_processing.domain.exception.InvalidJsonLdException;
import eu.gaiax.notarization.request_processing.domain.exception.InvalidRequestStateException;
import eu.gaiax.notarization.request_processing.domain.exception.NotFoundException;
import eu.gaiax.notarization.request_processing.domain.model.AccessToken;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.InvitationUrlResponse;
import eu.gaiax.notarization.request_processing.domain.model.MarkReadyResponse;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestId;
import eu.gaiax.notarization.request_processing.domain.model.SessionId;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskId;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskInstance;
import eu.gaiax.notarization.request_processing.infrastructure.rest.Api;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.*;
import eu.gaiax.notarization.request_processing.infrastructure.rest.feature.audit.Auditable;
import io.micrometer.core.instrument.MeterRegistry;
import io.smallrye.mutiny.Uni;
import java.net.URI;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

@Tag(name = Api.Tags.SUBMISSION)
@Path(Api.Path.SESSION_RESOURCE)
@PermitAll
@SecurityRequirements()
public class NotarizationRequestSubmissionResource {

    public static final String SESSION_PATH = "/" + Api.Param.SESSION_PARAM;
    public static final String SUBMISSION_PATH = SESSION_PATH + "/" + Api.Path.SUBMISSION;
    public static final String IDENTIFY_PATH = SESSION_PATH + "/" + Api.Path.IDENTIFY;
    public static final String REQUEST_PATH = SUBMISSION_PATH;
    public static final String TASK_PATH = SESSION_PATH + "/" + Api.Path.TASK;

    private final NotarizationRequestStore requestStore;
    private final MeterRegistry registry;

    public NotarizationRequestSubmissionResource(
            NotarizationRequestStore requestStore,
            MeterRegistry registry
    ) {
        this.requestStore = requestStore;
        this.registry = registry;
    }

    @Auditable(action = NotarizationRequestAction.CREATE_SESSION)
    @POST
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Create a new session", description = "Creates a new session for submitting a new notarization request.")
    @ResponseStatus(201)
    @APIResponse(responseCode = "201", description = "Details of the new session.", content = @Content(schema = @Schema(implementation = SessionInfoResponse.class)))
    public Uni<RestResponse<SessionInfoResponse>> create(@Context UriInfo request,
            @NotNull @Valid SessionSubmission body) {
        this.registry.counter("request.submission.create-token.call").increment();
        return requestStore.createNewSession(body.profileId())
                .map(session -> {
                    var response = ResponseBuilder.<SessionInfoResponse>created(
                            request.getBaseUriBuilder().path(Api.Path.SESSION_RESOURCE).path(SESSION_PATH).build(session.id))
                            .entity(SessionInfoResponse.valueOf(session))
                            // HACK: align this operation with the other auditable requestor operations
                            .header(Api.Param.SESSION, session.id)
                            .build();
                    return response;
                });
    }

    @Auditable(action = NotarizationRequestAction.REVOKE)
    @DELETE
    @Path(REQUEST_PATH)
    @Operation(summary = "Revoke notarization request", description = "Revokes the identified notarization request.")
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The notarization request was successfully revoked.")
    public Uni<Void> delete(
            @Valid @BeanParam AccessPathParameters pathParams) throws AuthorizationException, InvalidRequestStateException {
        this.registry.counter("request.submission.revoke-request.call").increment();
        return requestStore.softDeleteSession(pathParams.asSessionInfo());
    }

    @GET
    @Path(SESSION_PATH)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Fetch the session", description = "Fetches the identified session.")
    @ResponseStatus(200)
    @APIResponse(responseCode = "200", description = "A summary of the session.", content = @Content(schema = @Schema(implementation = SessionSummary.class)))
    public Uni<SessionSummary> fetchSession(
            @Context UriInfo request,
            @Valid @BeanParam AccessPathParameters pathParams)
            throws AuthorizationException, NotFoundException, InvalidRequestStateException {
        this.registry.counter("request.submission.session.call").increment();
        return requestStore.fetchSession(pathParams.asSessionInfo());
    }

    @Auditable(action = NotarizationRequestAction.UPDATE_CONTACT)
    @PUT
    @Path(SESSION_PATH + "/updateContact")
    @Consumes(MediaType.TEXT_PLAIN)
    @Operation(summary = "Update contact", description = "Updates contact information for receiving notifications.")
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The notarization request was successfully updated.")
    public Uni<Void> updateContact(
            @Context UriInfo request,
            @Valid @BeanParam AccessPathParameters pathParams,
            String contact
    )
            throws AuthorizationException, NotFoundException, InvalidRequestStateException {
        return requestStore.updateContact(pathParams.asSessionInfo(), contact);
    }

    @Auditable(action = NotarizationRequestAction.TASK_START)
    @POST
    @Path(TASK_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Start task", description = "Starts a task and returns information how to fullfill it.")
    @ResponseStatus(201)
    @APIResponse(responseCode = "201", description = "Information on how to fulfill the task.", content = @Content(schema = @Schema(implementation = TaskInstance.class)))
    @RequestBody(content = @Content(schema = @Schema(implementation = VcTaskStart.class)))
    public Uni<TaskInstance> startTask(
            @Context UriInfo request,
            @Valid @BeanParam AccessPathParameters pathParams,
            @Valid @RestQuery TaskId taskId,
            @Valid JsonNode data)
            throws AuthorizationException, InvalidRequestStateException {

        return requestStore.startTask(pathParams.asSessionInfo(), taskId, data);
    }

    @Auditable(action = NotarizationRequestAction.TASK_CANCEL)
    @DELETE
    @Path(TASK_PATH)
    @Operation(summary = "Cancel task", description = "Cancels a task.")
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The task was successfully cancelled.")
    public Uni<Void> cancelTask(
            @Context UriInfo request,
            @Valid @BeanParam AccessPathParameters pathParams,
            @RestQuery TaskId taskId)
            throws AuthorizationException, InvalidRequestStateException {

        return requestStore.cancelTask(pathParams.asSessionInfo(), taskId);
    }

    @Auditable(action = NotarizationRequestAction.SUBMIT)
    @POST
    @Path(SUBMISSION_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Submit a new notarization request", description = "Adds the submitted notarization request to the identified session.")
    @ResponseStatus(201)
    @APIResponse(responseCode = "201", description = "Information about the submission.", content = @Content(schema = @Schema(implementation = SubmissionResponse.class)))
    @APIResponse(ref = Api.Response.CANNOT_REUSE_TOKEN)
    public Uni<RestResponse<SubmissionResponse>> submit(
            @Context UriInfo request,
            @Valid @BeanParam AccessPathParameters pathParams,
            @Valid SubmitNotarizationRequest submissionRequest)
            throws NotFoundException, InvalidJsonLdException, CannotReuseTokenException {
        this.registry.counter("request.submission.create-request.call").increment();

        var requestId = UUID.randomUUID();

        return requestStore.submitNewRequest(new NotarizationRequestId(requestId),
                pathParams.asSessionInfo(),
                submissionRequest)
                .map(createdRequest
                        -> ResponseBuilder.<SubmissionResponse>created(
                        request.getBaseUriBuilder().path(Api.Path.SESSION_RESOURCE).path(REQUEST_PATH)
                                .build(pathParams.accessToken.token))
                        .entity(SubmissionResponse.from(createdRequest))
                        .build()
                );
    }

    @Auditable(action = NotarizationRequestAction.FETCH)
    @GET
    @Path(REQUEST_PATH)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Fetch notarization request", description = "Fetches the information of the identified notarization request.")
    @ResponseStatus(200)
    @APIResponse(responseCode = "200", description = "Information about the current request associated with the identified session.", content = @Content(schema = @Schema(implementation = NotarizationRequestView.class)))
    public Uni<NotarizationRequestView> fetchNotarizationRequest(
            @Valid @BeanParam AccessPathParameters params) throws NotFoundException, AuthorizationException {
        this.registry.counter("request.submission.fetch-notarizationrequest-request.call").increment();
        return requestStore.fetchNotarizationRequest(params.asSessionInfo());
    }

    @Auditable(action = NotarizationRequestAction.UPDATE)
    @PUT
    @Path(REQUEST_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Update notarization request", description = "Updates the notarization request, replacing previous information.")
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The notarization request was successfully updated.")
    @RequestBody(description = "The proposed content of the credential to be issued.")
    public Uni<Void> update(
            @Valid @BeanParam AccessPathParameters params,
            @Valid JsonNode data) throws InvalidRequestStateException, NotFoundException, InvalidJsonLdException, AuthorizationException {
        this.registry.counter("request.submission.update-request.call").increment();
        return requestStore.updateRequest(
                params.asSessionInfo(),
                data
        );
    }

    @Auditable(action = NotarizationRequestAction.ASSIGN_DID)
    @PUT
    @Path(REQUEST_PATH + "/did-holder")
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Assign DID holder", description = "Assigns the DID holder to the identified notarization request.")
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The holder information was successfully updated.")
    public Uni<Void> assignDidHolder(
            @Valid @BeanParam AccessPathParameters params,
            @Schema(description = "The target holder of the credential to be issued") @RestQuery String didHolder,
            @Schema(description = "An invitation URL of the holder")
            @RestQuery String invitation) throws InvalidRequestStateException, NotFoundException, AuthorizationException {
        this.registry.counter("request.submission.assign-did-holder.call").increment();
        return requestStore.assignDidHolder(params.asSessionInfo(), didHolder, invitation);
    }

    @Auditable(action = NotarizationRequestAction.MARK_READY)
    @POST
    @Path(REQUEST_PATH + "/ready")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Mark ready", description = "Marks the identified notarization request as ready for notarization. If 'manualRelease' is true, an URL to release an accepted credential is returned.")
    @ResponseStatus(200)
    @APIResponse(responseCode = "200", description = "URL allowing to release the credential after acceptance by the notary if 'manualRelease' was set to true.", content = @Content(schema = @Schema(implementation = MarkReadyResponse.class)))
    public Uni<MarkReadyResponse> markReady(
        @Valid @BeanParam AccessPathParameters params,
        @Schema(defaultValue = "false") @RestQuery Boolean manualRelease
    ) throws InvalidRequestStateException, NotFoundException, AuthorizationException {
        this.registry.counter("request.submission.mark-ready-request.call").increment();
        return this.requestStore.markReady(params.asSessionInfo(), manualRelease == null ? false : manualRelease);
    }

    @Auditable(action = NotarizationRequestAction.MARK_UNREADY)
    @POST
    @Path(REQUEST_PATH + "/unready")
    @Operation(summary = "Mark unready", description = "Marks the identified notarization request as unready.")
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The status was successfully updated.")
    public Uni<Void> markUnready(
            @Valid @BeanParam AccessPathParameters params)
            throws InvalidRequestStateException, NotFoundException, AuthorizationException {
        this.registry.counter("request.submission.mark-unready-request.call").increment();
        return this.requestStore.markUnready(params.asSessionInfo());
    }

    public static class AccessPathParameters {

        @NotNull
        @RestHeader(Api.Header.ACCESS_TOKEN)
        @Schema(type = SchemaType.STRING)
        AccessToken accessToken;

        @NotNull
        @RestPath(Api.Param.SESSION)
        SessionId session;

        public SessionInfo asSessionInfo() {
            return new SessionInfo(session, accessToken);
        }
    }

    @Auditable(action = NotarizationRequestAction.MANUAL_RELEASE)
    @POST
    @Path(Api.Path.ISSUE_MANUALY + "/" + Api.Param.RELEASE_TOKEN_PARAM)
    @Operation(summary = "Trigger issuance", description = "Triggers the issuance of an accepted notarization request.")
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The manual release was successfully triggered.")
    public Uni<Void> manualRelease(
        @PathParam(Api.Param.RELEASE_TOKEN) String releaseToken
    ) throws NotFoundException {
        this.registry.counter("request.submission.release-issuance.call").increment();
        return this.requestStore.manualRelease(releaseToken);
    }

    @GET
    @Path(REQUEST_PATH + "/ssiInviteUrl")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get ssi generated invite url", description = "Gets ssi generated invite url - will be empty if invite was given at start of the process by requestor.")
    @ResponseStatus(200)
    @APIResponse(responseCode = "200", description = "Inviation URL for the issued credential.", content = @Content(schema = @Schema(implementation = InvitationUrlResponse.class)))
    public Uni<InvitationUrlResponse> getSsiInviteUrl(
        @Valid @BeanParam AccessPathParameters params
    ) throws InvalidRequestStateException, NotFoundException, AuthorizationException {
        return this.requestStore.getSsiInvitationUrl(params.asSessionInfo()).map(foundInvite -> {
            var invite = foundInvite == null
                    ? null
                    : URI.create(foundInvite);
            return new InvitationUrlResponse(invite);
        });
    }
}
