/*
 *
 */
package eu.gaiax.notarization.request_processing.infrastructure.stringtemplate;

import eu.gaiax.notarization.request_processing.application.template.DocumentModel;
import eu.gaiax.notarization.request_processing.domain.entity.Document;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.model.Profile;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.compiler.STException;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class ProfileTemplateRenderer {

    public String render(Profile profile, final String documentTemplate, Set<Document> docs, NotarizationRequest value) {
        var group = getGroup();
        var errorListener = new LastSTErrorListener();
        group.setListener(errorListener);
        ST template;
        try {
            group.defineTemplate(profile.id(), "documents,credentialAugmentation", documentTemplate);
            template = group.getInstanceOf(profile.id());
            template.add("documents", DocumentModel.fromDocument(docs));
        } catch(STException ex) {
            throw new IllegalStateException(
                    "The template contains errors: " + errorListener.lastError().toString(), ex);
        }
        template.add("credentialAugmentation", JsonObjectSTWrapper.from(value.credentialAugmentation));
        return template.render();
    }

    STGroup getGroup() {
        STGroup stGroup = new STGroup();
        stGroup.registerModelAdaptor(JsonObjectSTWrapper.class, new JsonObjectSTWrapperAdapter());
        stGroup.registerRenderer(JsonObjectSTWrapper.class, new JsonObjectSTWrapperRenderer());
        stGroup.registerRenderer(JsonArraySTWrapper.class, new JsonArraySTWrapperRenderer());

        return stGroup;
    }
}
