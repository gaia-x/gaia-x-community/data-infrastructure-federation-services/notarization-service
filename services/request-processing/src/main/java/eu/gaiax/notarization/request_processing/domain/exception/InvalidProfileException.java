/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.exception;

import eu.gaiax.notarization.request_processing.domain.model.ProfileId;

public class InvalidProfileException extends BusinessException {

    public final ProfileId profile;

    public InvalidProfileException(ProfileId profile) {
        super(asMessage(profile));
        this.profile = profile;
    }

    public InvalidProfileException(ProfileId profile, Throwable cause) {
        super(asMessage(profile), cause);
        this.profile = profile;
    }
    

    private static String asMessage(ProfileId profile) {
        return String.format("The given profile identification is invalid: %s", profile);
    }
}
