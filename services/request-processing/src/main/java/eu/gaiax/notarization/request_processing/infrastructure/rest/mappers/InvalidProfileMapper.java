/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.mappers;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.RestResponse;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;

import eu.gaiax.notarization.request_processing.domain.exception.InvalidProfileException;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mappers.problem_details.InvalidProfileDetails;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mappers.problem_details.InvalidRequestStateProblemDetails;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mappers.problem_details.ProblemDetails;
import javax.ws.rs.ext.Provider;
import org.jboss.logging.Logger;

@Provider
public class InvalidProfileMapper implements ExceptionMapper<InvalidProfileException> {

    private static final Logger logger = Logger.getLogger(InvalidProfileMapper.class);

    @Override
    @APIResponse(responseCode = "400", description = "Invalid Profile Identification",
            content = {
                @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = InvalidRequestStateProblemDetails.class))
            }
    )
    @Produces(MediaType.APPLICATION_JSON)
    public Response toResponse(InvalidProfileException x) {
        logger.debug("Handled invalid profile exception", x);
        return Response.status(InvalidRequestStateProblemDetails.STATUS)
                .entity(asDetails(x))
                .build();
    }

    @ServerExceptionMapper
    public RestResponse<InvalidProfileDetails> mapException(InvalidProfileException x) {
        logger.debug("Handled invalid profile exception", x);
        return RestResponse.status(Response.Status.BAD_REQUEST, asDetails(x));
    }

    private InvalidProfileDetails asDetails(InvalidProfileException x) {
        return new InvalidProfileDetails(x.profile, ProblemDetails.ABOUT_BLANK, "Invalid profile identification", "The provided profile identification does not identify a known profile.", null);
    }
}
