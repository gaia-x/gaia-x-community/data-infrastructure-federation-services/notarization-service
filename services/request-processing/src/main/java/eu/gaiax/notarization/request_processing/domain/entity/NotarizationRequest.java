/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.entity;

import eu.gaiax.notarization.request_processing.domain.model.AipVersion;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestId;
import eu.gaiax.notarization.request_processing.domain.model.Profile;
import eu.gaiax.notarization.request_processing.domain.model.SessionId;
import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import io.smallrye.mutiny.Uni;
import io.vertx.core.json.JsonObject;
import java.time.OffsetDateTime;
import java.util.UUID;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.reactive.mutiny.Mutiny;

/**
 *
 * @author Neil Crossley
 */
@Entity
@Table(name = "notarizationrequest")
@TypeDef(
     name = "json",
     typeClass = JsonType.class
)
public class NotarizationRequest extends PanacheEntityBase {

    @Id
    public UUID id;
    public String did;

    public String requestorInvitationUrl;
    public String ssiInvitationUrl;

    @OneToOne()
    @JoinColumn(name = "session_id", unique=true)
    public Session session;

    //Needed due to not working query builder
    @Basic
    @Column(insertable=false, updatable=false, unique=true)
    public String session_id;

    public String rejectComment;

    @Convert(converter = StringConverter.class)
    public String data;

    @Type(type = "json")
    public JsonObject credentialAugmentation;

    public String claimedBy;

    @CreationTimestamp
    public OffsetDateTime createdAt;
    @UpdateTimestamp
    public OffsetDateTime lastModified;

    public boolean hasHolderAccess(Profile profile) {
        if (profile.aip() == AipVersion.V1_0) {
            return true;
        }
        return this.did != null && !this.did.isBlank();
    }

    public static Uni<NotarizationRequest> findById(NotarizationRequestId id) {
        return NotarizationRequest.findById(id.id);
    }

    public static Uni<NotarizationRequest> findWithDocuments(SessionId sessionId){
        return NotarizationRequest.<NotarizationRequest>find("session_id", sessionId.id)
            .singleResult()
            .call((r)->Mutiny.fetch(r.session.documents));
    }

    public Uni<NotarizationRequest> loadDocuments() {
        return Mutiny.fetch(this.session.documents).onItem().transform((d) -> this);
    }

    public Uni<NotarizationRequest> loadCredentialAugmentation() {
        return Mutiny.fetch(this.credentialAugmentation).onItem().transform((d) -> this);
    }

}
