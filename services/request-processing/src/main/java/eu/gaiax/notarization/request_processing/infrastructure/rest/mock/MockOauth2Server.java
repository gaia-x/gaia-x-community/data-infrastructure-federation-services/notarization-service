/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.mock;

import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState.Notary;
import io.quarkus.arc.profile.UnlessBuildProfile;
import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import java.net.URL;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
@UnlessBuildProfile("prod")
public class MockOauth2Server {

    @Inject
    Logger logger;

    @ConfigProperty(name = "quarkus.oidc.client-id")
    String givenClientId;

    @ConfigProperty(name = "quarkus.oidc.credentials.secret")
    String givenClientSecret;

    @ConfigProperty(name = "quarkus.oidc.introspection-path")
    URL givenIntrospectionUrl;

    WireMockServer wireMockServer;

    void startup(@Observes StartupEvent event) {
        logger.info("Preparing mock oauth2 server");
        wireMockServer = new WireMockServer(wireMockConfig().port(givenIntrospectionUrl.getPort()));

        for (Notary notary : MockState.notaries) {
            wireMockServer.stubFor(post(urlEqualTo(givenIntrospectionUrl.getPath()))
                    .atPriority(1)
                    .withBasicAuth(givenClientId, givenClientSecret)
                    .withRequestBody(containing("token=" + notary.token()))
                    .willReturn(aResponse()
                            .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                            .withBody(notary.introspectionToken())
                            .withStatus(200)));
        }

        wireMockServer.stubFor(post(urlEqualTo(givenIntrospectionUrl.getPath()))
                .atPriority(10)
                .withBasicAuth(givenClientId, givenClientSecret)
                .withRequestBody(matching(".*"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                        .withBody("""
                                  {"active":false}
                                  """)
                        .withStatus(200)));

        wireMockServer.addMockServiceRequestListener(
                this::requestReceived);
        wireMockServer.start();
        logger.info("Prepared mock oauth2 server");
    }

    void onStop(@Observes ShutdownEvent ev) {
        logger.info("Stopping mock oauth2 server");
        wireMockServer.stop();
    }

    protected void requestReceived(Request inRequest, Response inResponse) {
        logger.debugv("OAuth2 WireMock request at URL: {0}", inRequest.getAbsoluteUrl());
        logger.debugv("OAuth2 WireMock request headers: \n{0}", inRequest.getHeaders());
        logger.debugv("OAuth2 WireMock response body: \n{0}", inResponse.getBodyAsString());
        logger.debugv("OAuth2 WireMock response headers: \n{0}", inResponse.getHeaders());
    }

}
