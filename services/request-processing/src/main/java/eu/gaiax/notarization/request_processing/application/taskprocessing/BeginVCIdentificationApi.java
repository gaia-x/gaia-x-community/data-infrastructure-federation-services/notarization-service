/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.application.taskprocessing;
import io.smallrye.mutiny.Uni;
import java.net.URI;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jboss.resteasy.reactive.RestQuery;
/**
 *
 * @author Florian Otto
 */
@Path("")
public interface BeginVCIdentificationApi {

    @POST
    @Path("")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<BeginTaskResponse> beginTask(
            @RestQuery("successURL") URI success,
            @RestQuery("failureURL") URI failure,
        BeginIdentificationRequest request
    );

    public static class BeginIdentificationRequest {
          public URI invitationURL;
          public String profileID;
          public String holderDID;
    }
}
