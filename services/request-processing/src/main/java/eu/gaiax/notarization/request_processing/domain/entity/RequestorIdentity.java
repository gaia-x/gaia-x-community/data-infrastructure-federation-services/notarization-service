/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.entity;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import java.time.OffsetDateTime;
import java.util.UUID;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;

/**
 *
 * @author Neil Crossley
 */
@Entity
@Table(name = "requestor_identity")
public class RequestorIdentity extends PanacheEntityBase {

    @Id
    public UUID id;

    @Convert(converter = StringConverter.class)
    public String data;

    @ManyToOne
    public Session session;

    @Convert(converter = StringConverter.class)
    public String algorithm;

    @Convert(converter = StringConverter.class)
    public String encryption;

    @Convert(converter = StringConverter.class)
    public String jwk;

    @CreationTimestamp
    public OffsetDateTime createdAt;
}
