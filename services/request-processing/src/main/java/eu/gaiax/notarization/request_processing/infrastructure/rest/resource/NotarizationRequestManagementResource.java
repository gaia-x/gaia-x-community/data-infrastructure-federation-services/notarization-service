/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.resource;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;
import java.util.Set;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.openapi.annotations.tags.Tags;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestPath;

import eu.gaiax.notarization.request_processing.application.NotarizationManagementStore;
import eu.gaiax.notarization.request_processing.domain.exception.InvalidRequestStateException;
import eu.gaiax.notarization.request_processing.domain.exception.NotFoundException;
import eu.gaiax.notarization.request_processing.domain.model.DocumentId;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestId;
import eu.gaiax.notarization.request_processing.domain.model.Profile;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.domain.model.RequestFilter;
import eu.gaiax.notarization.request_processing.domain.services.ProfileService;
import eu.gaiax.notarization.request_processing.infrastructure.rest.Api;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.DocumentFull;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.IdentityView;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.NotarizationRequestView;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.PagedNotarizationRequestSummary;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.RejectRequest;
import eu.gaiax.notarization.request_processing.infrastructure.rest.feature.audit.Auditable;
import io.micrometer.core.instrument.MeterRegistry;
import io.smallrye.mutiny.Uni;
import io.vertx.core.json.JsonObject;
import java.net.URISyntaxException;
import javax.ws.rs.Consumes;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;

/**
 *
 * @author Neil Crossley
 */
@Tag(name = Api.Tags.MANAGEMENT)
@Path(Api.Path.V1_PREFIX)
@RolesAllowed("**")
@SecurityRequirement(name = "notary", scopes = {Api.Role.NOTARY})
public class NotarizationRequestManagementResource {

    public static final String REQUESTS = "/requests";
    public static final String REQUEST = "/profiles/"
            + Api.Param.PROFILE_ID_PARAM
            + "/requests/"
            + Api.Param.NOTARIZATION_REQUEST_ID_PARAM;
    public static final String DOC_REQUEST = REQUEST
        + "/document/"
        + Api.Param.NOTARIZATION_REQUEST_DOCUMENT_ID_PARAM;
    public static final String CREDENTIAL_AUGMENTATION_PATH = REQUEST
            + Api.Path.CREDENTIAL_AUGMENTATION;

    public static final String REVOKE = "/revoke";

    private final NotarizationManagementStore managementStore;
    private final MeterRegistry registry;
    private final ProfileService profiles;
    private final Logger logger;

    public NotarizationRequestManagementResource(
        NotarizationManagementStore managementStore,
        MeterRegistry registry,
        ProfileService profiles,
        Logger logger
    ){
        this.managementStore = managementStore;
        this.registry = registry;
        this.profiles = profiles;
        this.logger = logger;
    }

    @Tags({
        @Tag(name = Api.Tags.SUBMISSION),
        @Tag(name = Api.Tags.MANAGEMENT)
    })
    @Path("/profiles")
    @PermitAll
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "All profiles",
            description = "Fetches all profiles."
    )
    @ResponseStatus(200)
    @APIResponse(responseCode = "200", description = "A list of all profiles.", content = @Content(schema = @Schema(implementation = Profile.class)))
    public Uni<List<Profile>> list() {
        return this.profiles.findAll();
    }

    @Auditable(action = NotarizationRequestAction.NOTARY_FETCH_ALL)
    @GET
    @Path(REQUESTS)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "Query notarization requests",
            description = "Querys the available notarization requests. This operation will page the resuls and supports filtering."
    )
    @ResponseStatus(200)
    @APIResponse(responseCode = "200", description = "A result list of notarization requests.", content = @Content(schema = @Schema(implementation = PagedNotarizationRequestSummary.class)))
    public Uni<PagedNotarizationRequestSummary> fetchAvailableRequests(
        @Schema(defaultValue = "0") @Min(value=0) @QueryParam("offset") int offset,
        @Min(value=1) @QueryParam("limit") int limit,
        @QueryParam("filter") RequestFilter filter,
        @Context SecurityContext securityContext
    ) {
        this.registry.counter("request.management.fetch-available-requests.call").increment();
        var effectiveFilter = filter != null ? filter : RequestFilter.available;
        return managementStore.fetchAvailableRequests(offset, limit, effectiveFilter, securityContext);
    }

    @Auditable(action = NotarizationRequestAction.NOTARY_FETCH_REQ)
    @GET
    @Path(REQUEST)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "Fetch available notarization request",
            description = "Fetches the available notarization request (not claimed by any notary operator)."
    )
    @ResponseStatus(200)
    @APIResponse(responseCode = "200", description = "A notarization request.", content = @Content(schema = @Schema(implementation = NotarizationRequestView.class)))
    public Uni<NotarizationRequestView> fetchAvailableRequest(
            @NotNull @RestPath(Api.Param.PROFILE_ID) ProfileId profile,
            @NotNull @RestPath(Api.Param.NOTARIZATION_REQUEST_ID) NotarizationRequestId id
    ) throws NotFoundException {
        this.registry.counter("request.management.fetch-request.call").increment();
        return managementStore.fetchAvailableRequest(profile, id);
    }

    @Auditable(action = NotarizationRequestAction.NOTARY_FETCH_DOC)
    @GET
    @Path(DOC_REQUEST)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "Fetch document",
            description = "Fetches an available document."
    )
    @ResponseStatus(200)
    @APIResponse(responseCode = "200", description = "A document.", content = @Content(schema = @Schema(implementation = DocumentFull.class)))
    public Uni<DocumentFull> fetchAvailableDocument(
            @NotNull @RestPath(Api.Param.PROFILE_ID) ProfileId profile,
            @NotNull @RestPath(Api.Param.NOTARIZATION_REQUEST_ID) NotarizationRequestId notReqId,
            @NotNull @RestPath(Api.Param.DOCUMENTID) DocumentId docId) throws NotFoundException {
        this.registry.counter("request.management.fetch-document.call").increment();
        return managementStore.fetchAvailableDocument(profile, notReqId, docId);
    }

    @Auditable(action = NotarizationRequestAction.CLAIM)
    @POST
    @Path(REQUEST + "/claim")
    @Operation(
            summary = "Claim available notarization request",
            description = "Claims a notarization request current available (not claimed) by any notary operator."
    )
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The request was successfully claimed.")
    public Uni<Void> claimAvailableRequest(
            @NotNull @RestPath(Api.Param.PROFILE_ID) ProfileId profile,
            @NotNull @RestPath(Api.Param.NOTARIZATION_REQUEST_ID) NotarizationRequestId id,
            @Context SecurityContext securityContext
        ) throws InvalidRequestStateException, NotFoundException  {
        this.registry.counter("request.management.claim-request.call").increment();
        return managementStore.claimAvailableRequest(profile, id, securityContext);
    }

    @Auditable(action = NotarizationRequestAction.FETCH_IDENTITY)
    @GET
    @Path(REQUEST + "/identity")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "Fetch the identity assigned to the notarization request",
            description = "Fetches the encrypted identity assigned to the notarization request."
    )
    @ResponseStatus(200)
    @APIResponse(responseCode = "200", description = "An identity.", content = @Content(schema = @Schema(implementation = IdentityView.class)))
    public Uni<Set<IdentityView>> identity(
            @NotNull @RestPath(Api.Param.PROFILE_ID) ProfileId profile,
            @NotNull @RestPath(Api.Param.NOTARIZATION_REQUEST_ID) NotarizationRequestId id) throws InvalidRequestStateException, NotFoundException  {
        this.registry.counter("request.management.get-identity.call").increment();
        return managementStore.getIdentity(profile, id);
    }

    @Auditable(action = NotarizationRequestAction.ACCEPT)
    @POST
    @Path(REQUEST + "/accept")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.WILDCARD})
    @Operation(
            summary = "Accept the notarization request",
            description = "Accepts and approves the notarization request."
    )
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The notarization request was successfully accepted.")
    public Uni<Void> acceptWithExtension(
            @NotNull @RestPath(Api.Param.PROFILE_ID) ProfileId profile,
            @NotNull @RestPath(Api.Param.NOTARIZATION_REQUEST_ID) NotarizationRequestId id) throws InvalidRequestStateException, NotFoundException  {
        this.registry.counter("request.management.accept.call").increment();
        return managementStore.acceptAvailableRequest(profile, id);
    }

    @Auditable(action = NotarizationRequestAction.REJECT)
    @POST
    @Path(REQUEST + "/reject")
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "Reject the notarization request",
            description = "Rejects the notarization request in its current state. This does not delete the request. The business owner may alter and resubmit in cases where only details lead to rejection."
    )
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The notarization request was successfully rejected.")
    public Uni<Void> reject(
            @NotNull @RestPath(Api.Param.PROFILE_ID) ProfileId profile,
            @NotNull @RestPath(Api.Param.NOTARIZATION_REQUEST_ID) NotarizationRequestId id,
            RejectRequest request) throws InvalidRequestStateException, NotFoundException  {
        this.registry.counter("request.management.reject.call").increment();
        return managementStore.rejectAvailableRequest(profile, id, request == null ? null : request.reason());
    }

    @Auditable(action = NotarizationRequestAction.NOTARY_DELETE)
    @DELETE
    @Path(REQUEST)
    @Operation(
            summary = "Delete the notarization request",
            description = "Deletes the notarization request in its current state."
    )
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The notarization request was successfully deleted.")
    public Uni<Void> delete(
            @NotNull @RestPath(Api.Param.PROFILE_ID) ProfileId profile,
            @NotNull @RestPath(Api.Param.NOTARIZATION_REQUEST_ID) NotarizationRequestId id) throws InvalidRequestStateException, NotFoundException  {
        this.registry.counter("request.management.delete.call").increment();
        return managementStore.deleteRequest(profile, id);
    }

    @Auditable(action = NotarizationRequestAction.NOTARY_REVOKE)
    @POST
    @Path(REVOKE)
    @Operation(
            summary = "Revoke the given credential",
            description = "Revokes the given credential."
    )
    @ResponseStatus(204)
    @APIResponse(responseCode = "204", description = "The credential was successfully revoked.")
    @RequestBody(description = "The credential to be revoked. Must be issued with the fields 'statusListCredential' and 'statusListIndex'")
    public Uni<Void> revoke(
            @Context SecurityContext securityContext,
            JsonNode credential
    ) throws URISyntaxException {
        this.registry.counter("request.management.revoke-credential.call").increment();
        return managementStore.revokeRequest(securityContext, credential);
    }

    @Auditable(action = NotarizationRequestAction.CREDENTIAL_AUGMENTATION_PUT)
    @PUT
    @Path(CREDENTIAL_AUGMENTATION_PATH)
    @Operation(
            summary = "Assign a credential augmentation to the request",
            description = "Replace or set the structure that shall augment the requested credentials."
    )
    @ResponseStatus(204)
    @Consumes(MediaType.APPLICATION_JSON)
    @APIResponse(responseCode = "204", description = "The payload was accepted.")
    @RequestBody(description = "The payload to augment the requsted credential."
            + " The result of the augmentation must still conform to the schema of the credential."
            + " The augmentation and validation occur as a part of the issuing process.",
            content = @Content(schema = @Schema(type = SchemaType.OBJECT)))
    public Uni<Void> assignCredentialAugmentation(
            @NotNull @RestPath(Api.Param.PROFILE_ID) ProfileId profile,
            @NotNull @RestPath(Api.Param.NOTARIZATION_REQUEST_ID) NotarizationRequestId id,
            JsonObject credentialOverride
    ) throws URISyntaxException {
        this.registry.counter("request.management.credential-override.put.call").increment();
        return managementStore.assignCredentialAugmentation(profile, id, credentialOverride);
    }
}
