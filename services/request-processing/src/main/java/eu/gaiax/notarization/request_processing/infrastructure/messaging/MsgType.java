/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.messaging;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 *
 * @author Florian Otto
 */
public enum MsgType {
       READY_FOR_REVIEW(Name.READY_FOR_REVIEW),
       REQUEST_REJECTED(Name.REQUEST_REJECTED),
       REQUEST_ACCEPTED_PENDING_DID(Name.REQUEST_ACCEPTED_PENDING_DID),
       REQUEST_ACCEPTED(Name.REQUEST_ACCEPTED),
       REQUEST_DELETED(Name.REQUEST_DELETED),
       REQUEST_TERMINATED(Name.REQUEST_TERMINATED),
       EXTERNAL_TASK_STARTED(Name.EXTERNAL_TASK_STARTED),
       CONTACT_UPDATE(Name.CONTACT_UPDATE);

    private final String value;

    MsgType(String value) {
        this.value = value;
    }
    public static MsgType fromString(String s){
        switch (s) {
            case Name.READY_FOR_REVIEW : return READY_FOR_REVIEW;
            case Name.REQUEST_REJECTED : return REQUEST_REJECTED;
            case Name.REQUEST_ACCEPTED_PENDING_DID : return REQUEST_ACCEPTED_PENDING_DID;
            case Name.REQUEST_ACCEPTED : return REQUEST_ACCEPTED;
            case Name.REQUEST_DELETED: return REQUEST_DELETED;
            case Name.REQUEST_TERMINATED: return REQUEST_TERMINATED;
            case Name.EXTERNAL_TASK_STARTED : return EXTERNAL_TASK_STARTED;
            case Name.CONTACT_UPDATE : return CONTACT_UPDATE;
            default : return null;
        }
    }

    @Override
    @JsonValue
    public String toString(){
        return String.valueOf(value);
    }

    public static class Name {
       public static final String READY_FOR_REVIEW = "READY_FOR_REVIEW";
       public static final String REQUEST_REJECTED = "REQUEST_REJECTED";
       public static final String REQUEST_ACCEPTED_PENDING_DID = "REQUEST_ACCEPTED_PENDING_DID";
       public static final String REQUEST_ACCEPTED = "REQUEST_ACCEPTED";
       public static final String REQUEST_DELETED = "REQUEST_DELETED";
       public static final String REQUEST_TERMINATED = "REQUEST_TERMINATED";
       public static final String EXTERNAL_TASK_STARTED = "EXTERNAL_TASK_STARTED";
       public static final String CONTACT_UPDATE = "CONTACT_UPDATE";
    }
}
