/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.model.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import org.jose4j.jwk.PublicJsonWebKey;

/**
 *
 * @author Neil Crossley
 */
public class PublicJsonWebKeySerializer extends StdSerializer<PublicJsonWebKey> {

    public PublicJsonWebKeySerializer() {
        super(PublicJsonWebKey.class);
    }

    @Override
    public void serialize(PublicJsonWebKey t, JsonGenerator jg, SerializerProvider sp) throws IOException {
        jg.writeRawValue(t.toJson());
    }
}
