/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.mappers.problem_details;

import java.net.URI;
import java.net.URISyntaxException;

import io.quarkus.logging.Log;
import javax.validation.constraints.NotNull;

public class ProblemDetails {

    public static URI ABOUT_BLANK;

    static {
        URI aboutBlank;
        try {
            aboutBlank = new URI("about:blank");
        } catch (URISyntaxException e) {
            aboutBlank = null;
            Log.error("Could not create the 'about:blank' URI!", e);
        }
        ABOUT_BLANK = aboutBlank;
    }

    @NotNull
    public final URI type;
    @NotNull
    public final String title;
    @NotNull
    public final int status;
    @NotNull
    public final String detail;
    public final String instance;

    public ProblemDetails(URI type, String title, int status, String detail, String instance) {
        this.type = type;
        this.title = title;
        this.status = status;
        this.detail = detail;
        this.instance = instance;
    }

    public ProblemDetails(String title, int status, String detail, String instance) {
        this(ABOUT_BLANK, title, status, detail, instance);
    }
}
