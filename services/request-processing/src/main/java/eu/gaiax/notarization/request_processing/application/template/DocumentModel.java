/*
 *
 */
package eu.gaiax.notarization.request_processing.application.template;

import eu.gaiax.notarization.request_processing.domain.entity.Document;
import eu.gaiax.notarization.request_processing.domain.model.TemplateModel;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Neil Crossley
 */
public class DocumentModel implements TemplateModel.Document {

    private final Document document;

    public DocumentModel(Document document) {
        this.document = document;
    }

    @Override
    public String getId() {
        return document.id.toString();
    }

    @Override
    public String getTitle() {
        return document.title;
    }

    @Override
    public String getShortDescription() {
        return document.shortDescription;
    }

    @Override
    public String getMimetype() {
        return document.mimetype;
    }

    @Override
    public String getExtension() {
        return document.extension;
    }

    @Override
    public TemplateModel.DocumentHash getSha3_256() {
        return DocumentHashModel.fromHex(document.hash);
    }

    @Override
    public TemplateModel.DocumentHash getSha3() {
        return DocumentHashModel.fromHex(document.hash);
    }

    @Override
    public TemplateModel.DocumentHash getSha() {
        return DocumentHashModel.fromHex(document.hash);
    }

    public static List<DocumentModel> fromDocument(Set<Document> documents) {
        var result = new ArrayList<DocumentModel>(documents.size());
        for (Document document : documents) {
            result.add(new DocumentModel(document));
        }
        result.sort(DocumentModelComparator.INSTANCE);
        return result;
    }

    @Override
    public TemplateModel.Document getSelf() {
        return this;
    }

    private static class DocumentModelComparator implements Comparator<DocumentModel> {

        private static final DocumentModelComparator INSTANCE = new DocumentModelComparator();

        @Override
        public int compare(DocumentModel left, DocumentModel right) {
            return left.getId().compareTo(right.getId());
        }

    }
}
