/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.dto;

import eu.gaiax.notarization.request_processing.domain.entity.SessionTask;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskType;
import java.util.UUID;

/**
 *
 * @author Florian Otto
 */
public class SessionTaskSummary {

    public UUID taskId;

    public String name;
    public TaskType type;

    public boolean fulfilled;
    public boolean running;

   public static SessionTaskSummary valueOf(SessionTask sessTask){

       var summary = new SessionTaskSummary();
       summary.taskId = sessTask.taskId;
       summary.name = sessTask.name;
       summary.type = sessTask.type;
       summary.fulfilled = sessTask.fulfilled;
       summary.running = sessTask.running;
       return summary;

   }

}
