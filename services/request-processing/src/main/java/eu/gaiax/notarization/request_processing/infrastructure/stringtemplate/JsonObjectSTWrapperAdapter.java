/*
 *
 */
package eu.gaiax.notarization.request_processing.infrastructure.stringtemplate;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.jboss.logging.Logger;
import org.stringtemplate.v4.Interpreter;
import org.stringtemplate.v4.ModelAdaptor;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.misc.STNoSuchPropertyException;

/**
 *
 * @author Neil Crossley
 */
public class JsonObjectSTWrapperAdapter implements ModelAdaptor<JsonObjectSTWrapper> {

    public static final Logger logger = Logger.getLogger(JsonObjectSTWrapperAdapter.class);

    @Override
    public Object getProperty(Interpreter interp, ST self, JsonObjectSTWrapper wrapper, Object property, String propertyName) throws STNoSuchPropertyException {
        var model = wrapper.json();
        if (model.containsKey(propertyName)) {
            var found = model.getValue(propertyName);
            if (found instanceof JsonObject jsonObject) {
                return new JsonObjectSTWrapper(jsonObject);
            }
            if (found instanceof JsonArray jsonArray) {
                return new JsonArraySTWrapper(jsonArray);
            }
            return found;
        }

        throw new STNoSuchPropertyException(null, model, propertyName);
    }

}
