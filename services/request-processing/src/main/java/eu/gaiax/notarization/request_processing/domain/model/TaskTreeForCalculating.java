/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.model;

import eu.gaiax.notarization.request_processing.domain.entity.SessionTask;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Florian Otto
 */
public class TaskTreeForCalculating {

        public boolean fulfilled;
        public Set<TaskTreeForCalculating> allOf;
        public Set<TaskTreeForCalculating> oneOf;

        public TaskTreeForCalculating(){
            this.fulfilled = false;
            this.allOf = new HashSet<>();
            this.oneOf= new HashSet<>();
        }

        public static TaskTreeForCalculating buildTree(ProfileTaskTree profileTree, Set<SessionTask> tasks){

            var calcTree = new TaskTreeForCalculating();

            if(profileTree.isEmpty()){
                calcTree.fulfilled = true;
            } else {

                if(profileTree.taskName != null){
                    calcTree.fulfilled = tasks.stream()
                        .filter((t)->t.name.equals(profileTree.taskName))
                        .filter((t)->t.fulfilled)
                        .findFirst()
                        .isPresent();

                } else {
                    calcTree.allOf =
                        profileTree.allOf.stream()
                        .map((childProfileTree) -> buildTree(childProfileTree, tasks))
                        .collect(Collectors.toSet());

                    calcTree.oneOf =
                        profileTree.oneOf.stream()
                        .map((childProfileTree) -> buildTree(childProfileTree, tasks))
                        .collect(Collectors.toSet());
                }
            }

            return calcTree;
        }

        public boolean fulfilled(){

            if(this.fulfilled){
                return true;
            } else if(!this.allOf.isEmpty()){
                return this.allOf.stream().allMatch(t->t.fulfilled());
            } else if(!this.oneOf.isEmpty()){
                return this.oneOf.stream().anyMatch(t->t.fulfilled());
            }
            return false;

        }

   }
