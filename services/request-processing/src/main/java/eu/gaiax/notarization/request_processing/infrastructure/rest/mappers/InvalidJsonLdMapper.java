/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.mappers;

import eu.gaiax.notarization.request_processing.domain.exception.InvalidJsonLdException;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mappers.problem_details.InvalidJsonLdDetails;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mappers.problem_details.InvalidRequestStateProblemDetails;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mappers.problem_details.ProblemDetails;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestResponse;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;

/**
 *
 * @author Neil Crossley
 */
@Provider
public class InvalidJsonLdMapper implements ExceptionMapper<InvalidJsonLdException> {

    private static final Logger logger = Logger.getLogger(InvalidJsonLdMapper.class);

    @Override
    @APIResponse(responseCode = "400", description = "Invalid JSON-LD",
            content = {
                @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = InvalidJsonLdDetails.class))
            }
    )
    @Produces(MediaType.APPLICATION_JSON)
    public Response toResponse(InvalidJsonLdException x) {
        logger.debug("Handled invalid json-ld exception", x);
        return Response.status(InvalidRequestStateProblemDetails.STATUS)
                .entity(asDetails())
                .build();
    }

    public static InvalidJsonLdDetails asDetails() {
        return new InvalidJsonLdDetails(
                ProblemDetails.ABOUT_BLANK, "Invalid JSON-LD", "Provided JSON-LD is not valid", null);
    }

    @ServerExceptionMapper
    public RestResponse<ProblemDetails> mapException(InvalidJsonLdException x) {
        logger.debug("Handled invalid json-ld exception", x);
        return RestResponse.status(Response.Status.BAD_REQUEST, asDetails());
    }

}
