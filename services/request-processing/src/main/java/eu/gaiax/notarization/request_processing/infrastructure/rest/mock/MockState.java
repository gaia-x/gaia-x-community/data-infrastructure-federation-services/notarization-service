/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.mock;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.request_processing.domain.model.NotaryAccess;
import eu.gaiax.notarization.request_processing.domain.model.Profile;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.domain.model.ProfileTaskTree;
import eu.gaiax.notarization.request_processing.domain.model.AipVersion;
import eu.gaiax.notarization.request_processing.domain.model.TaskDescription;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskType;
import java.io.StringReader;
import java.time.Instant;
import java.time.Period;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.json.Json;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.lang.JoseException;

/**
 *
 * @author Neil Crossley
 */
public class MockState {

    public static final ProfileId someProfileId = new ProfileId("someProfile");
    public static final ProfileId augmentingProfileId = new ProfileId("augmentingProfile");
    public static final ProfileId profileId1 = new ProfileId("profile-01");
    public static final ProfileId profileId2 = new ProfileId("profile-02");
    public static final ProfileId profileId3 = new ProfileId("profile-03");
    public static final ProfileId profileId4 = new ProfileId("profile-04");
    public static final ProfileId profileWithOptionalPreConditionId = new ProfileId("profile-with-opt-pre-con");
    public static final ProfileId profileWithOnlyPreConditionId = new ProfileId("profile-with-only-pre-con");

    public static final Profile someProfile;
    public static final Profile augmentingProfile;
    public static final Profile profile1;
    public static final Profile profile2;
    public static final Profile profile3;
    public static final Profile profile4;
    public static final Profile profileWithOptionalPreCondition;
    public static final Profile profileWithOnlyPreCondition;

    public static final ObjectMapper mapper = new ObjectMapper();

    public static final Set<ProfileId> profileIds = Set.of(someProfileId,
            augmentingProfileId,
            profileId1,
            profileId2,
            profileId3,
            profileId4,
            profileWithOptionalPreConditionId,
            profileWithOnlyPreConditionId);

    public static final Notary notary1 = new Notary(
            UUID.fromString("d6b8bd85-95c5-43d0-a93f-5bb034aef7e4"),
            "notary-01",
            Set.of(someProfileId, profileId1),
            "notary-01-access-token-12345");

    public static final Notary notary2 = new Notary(
            UUID.fromString("653813be-5a71-47be-ba24-c3c838df4fdd"),
            "notary-02", Set.of(someProfileId, profileId1, profileId2),
            "notary-02-access-token-23456");

    public static final Notary notary3 = new Notary(
            UUID.fromString("3aadbd54-9253-4018-8b62-890e9a8b46ff"),
            "notary-03", Set.of(someProfileId, profileId1, profileId2, profileId3),
            "notary-03-access-token-34567");

    public static final Notary notary4 = new Notary(
            UUID.fromString("be4770ac-eba0-43cb-af0f-cf873885c446"),
            "notary-04", Set.of(augmentingProfileId, profileId1, profileId2, profileId3, profileId4),
            "notary-04-access-token-45678");

    public static final Set<Notary> notaries = Set.of(
            notary1,
            notary2,
            notary3,
            notary4
    );
    public static final Set<Notary> notariesWithSomeProfile = notaries.stream()
        .filter(notary -> notary.roles().contains(someProfileId))
        .collect(Collectors.toSet());

    public static final Map<UUID, Notary> notariesBySubject = notaries.stream().collect(
            Collectors.toMap(n -> n.subject(), n -> n));

    public static final Set<Profile> profiles;


    public static ProfileTaskTree neededTasks_oneof_identiyandupload_identiyandotherupload() {

        try {
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

            return mapper.readValue("""
                                            {"oneOf":[{"allOf":[{"taskName":"identify"},{"taskName":"upload"}]},{"allOf":[{"taskName":"identify"},{"taskName":"upload_other"}]}]}
                                            """,
                ProfileTaskTree.class);
        } catch (JsonProcessingException ex) {
            throw new IllegalArgumentException(ex);
        }

    }

    public static ProfileTaskTree precondTasks_optional() {

        try {
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

            return mapper.readValue("""
                                            { "oneOf": [{"taskName":"identify"}, {}] }
                                            """,
                ProfileTaskTree.class);
        } catch (JsonProcessingException ex) {
            throw new IllegalArgumentException(ex);
        }

    }

    public static ProfileTaskTree precondTasks_identify() {

        try {
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

            return mapper.readValue("""
                                            {"taskName":"identify"}
                                            """,
                ProfileTaskTree.class);
        } catch (JsonProcessingException ex) {
            throw new IllegalArgumentException(ex);
        }

    }

    public static ProfileTaskTree neededTasks_oneOfTwoDocTasks() {

        try {
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

            return mapper.readValue("""
                                            {"oneOf":[{"taskName":"upload_sufficient"},{"taskName":"upload_combination_of"}]}
                                            """,
                ProfileTaskTree.class);
        } catch (JsonProcessingException ex) {
            throw new IllegalArgumentException(ex);
        }

    }
    public static List<TaskDescription> taskDescriptions(){
        var json = Json.createReader(
                            new StringReader(
                                """
                                [
                                {"name":"upload","type":"fileProvisionTask","description":"upload 3 files"},
                                {"name":"upload_other","type":"fileProvisionTask","description":"upload 3 files"},
                                {"name":"upload_sufficient","type":"fileProvisionTask","description":"upload 3 files"},
                                {"name":"upload_combination_of","type":"fileProvisionTask","description":"upload 3 files"},
                                {"name":"identify","type":"browserIdentificationTask","description":"identify yourself via browser"},
                                {"name":"vc_ident","type":"vcIdentificationTask","description":"identify yourself with vc"}
                                ]
                                """
                            )).readArray();
        return json.stream()
            .map(e -> e.asJsonObject())
            .map(j -> {
                return new TaskDescription(
                    j.getString("name"),
                    TaskType.fromString(j.getString("type")),
                    j.getString("description")
                );
            })
            .collect(Collectors.toList());

    }
    static {
        try {
            var someJwk = PublicJsonWebKey.Factory.newPublicJwk(
                    """
                    {
                    "kty": "EC",
                    "d": "ssgbzdN5aYkKEFR6L3YSB9hpKfKuiiZxjsDf4y5r8Ck",
                    "use": "enc",
                    "crv": "P-256",
                    "x": "MHOzeo5914c4RZvgTGNHoxdzjg2iziqCJ1y0D0_ONAs",
                    "y": "c0k8MWQy3sQknEjOEnR0ZjgqQFWXPJ0clVzjruRT3fQ",
                    "alg": "ECDH-ES"
                    }
                    """);
            var someOtherJwk = PublicJsonWebKey.Factory.newPublicJwk(
                    """
                    {"kty":"EC",
                     "d":"gHltlw4V2JKep2v4oRziohrYvU3cvRQ50T3-6WPf0B3d4_2yZ-AGf6sghQVoCPj-",
                     "crv":"P-384",
                     "x":"I9_0yRjMwcRLAhNBwWLPmII2BJK6pIPTqr0LRy0WkoLJiyA846iLUWmLuvM3P8-w",
                     "y":"rPaBxsFyHKPs-h5Pkp6zQ0y5ygdIjvDOc9PTPDqxZnmyBZngFlPNkcl29SSqKzAs"}
                    """);
            profile1 = new Profile(profileId1.id(),
                    AipVersion.V2_0,
                    "First name",
                    "Some first description",
                    "A256GCM",
                    List.of(new NotaryAccess("ECDH-ES+A256KW", someJwk)),
                    Period.ofYears(1),
                    false,
                    Json.createReader(
                            new StringReader("""
                                             {"first object":"other", "first field": 1}
                                             """)).readObject(),
                    """
                    { "credentialSubject": { "evidenceDocument": [ <documents:{doc|"<doc.sha256.base64>"}; separator=" ,">  ] } }
                    """,
                    neededTasks_oneOfTwoDocTasks(),
                    new ProfileTaskTree(),
                    taskDescriptions()
                    );
            profile2 = new Profile(profileId2.id(),
                    AipVersion.V2_0,
                    "Second name",
                    "Some second description",
                    "A256GCM",
                    List.of(new NotaryAccess("ECDH-ES+A256KW", someOtherJwk)),
                    Period.ofYears(1),
                    true,
                    Json.createReader(
                            new StringReader("""
                                             {"second object":"other", "second field": 1}
                                             """)).readObject(),
                    """
                    { "credentialSubject": { "evidenceDocument": [ <documents:{doc|"<doc.sha256.base64>"}; separator=" ,">  ] } }
                    """,
                    neededTasks_oneOfTwoDocTasks(),
                    precondTasks_identify(),
                    taskDescriptions()

                    );
            profile3 = new Profile(profileId3.id(),
                    AipVersion.V2_0,
                    "Third name",
                    "Some third description",
                    profile1.encryption(),
                    profile1.notaries(),
                    Period.ofMonths(7),
                    false,
                    Json.createReader(
                            new StringReader("""
                                             {"third-object":"other", "second field": 1}
                                             """)).readObject(),
                    """
                    { "evidenceDocument": "<documents:{doc|<doc.sha256.base64>}; separator=" ,">" }
                    """,
                    neededTasks_oneOfTwoDocTasks(),
                    precondTasks_identify(),
                    taskDescriptions()

                    );
            profile4 = new Profile(profileId4.id(),
                    AipVersion.V2_0,
                    "Fourth name",
                    "Some fourth description",
                    profile2.encryption(),
                    profile2.notaries(),
                    Period.ofDays(5),
                    true,
                    Json.createReader(
                            new StringReader("""
                                             {"fourth-object":"other", "second field": 1}
                                             """)).readObject(),
                    """
                    { "evidenceDocument": "<documents:{doc|<doc.sha256.base64>}; separator=" ,">" }
                    """,
                    neededTasks_oneOfTwoDocTasks(),
                    precondTasks_identify(),
                    taskDescriptions()

                    );
            profileWithOptionalPreCondition = new Profile(profileWithOptionalPreConditionId.id(),
                    AipVersion.V2_0,
                    "Fourth name",
                    "Some fourth description",
                    profile2.encryption(),
                    profile2.notaries(),
                    Period.ofDays(5),
                    false,
                    Json.createReader(
                            new StringReader("""
                                             {"fourth-object":"other", "second field": 1}
                                             """)).readObject(),
                    """
                    { "evidenceDocument": "<documents:{doc|<doc.sha256.base64>}; separator=" ,">" }
                    """,
                    neededTasks_oneOfTwoDocTasks(),
                    precondTasks_optional(),
                    taskDescriptions()

                    );

            profileWithOnlyPreCondition = new Profile(profileWithOnlyPreConditionId.id(),
                    AipVersion.V2_0,
                    "Profile-Only-Precondition",
                    "Some fourth description",
                    profile2.encryption(),
                    List.of(),
                    Period.ofDays(5),
                    false,
                    Json.createReader(
                            new StringReader("""
                                             {"an-object":"stuff", "second field": 1}
                                             """)).readObject(),
                    """
                    """,
                    new ProfileTaskTree(),
                    precondTasks_identify(),
                    taskDescriptions()

                    );
            someProfile = new Profile(someProfileId.id(),
                    AipVersion.V2_0,
                    "Some profile name",
                    "Some profile description",
                    profile2.encryption(),
                    List.of(
                        new NotaryAccess("ECDH-ES+A256KW", someJwk),
                        new NotaryAccess("ECDH-ES+A256KW", someJwk),
                        new NotaryAccess("ECDH-ES+A256KW", someJwk)
                    ),
                    Period.ofDays(155),
                    true,
                    Json.createReader(
                            new StringReader("""
                                             {"some profile field":"other", "second field": "something"}
                                             """)).readObject(),
                    null,
                    neededTasks_oneOfTwoDocTasks(),
                    precondTasks_identify(),
                    taskDescriptions()
                    );
            augmentingProfile = new Profile(augmentingProfileId.id(),
                    AipVersion.V2_0,
                    "Augmenting profile name",
                    "A profile that requires credential augmentation by the notary",
                    profile2.encryption(),
                    List.of(
                        new NotaryAccess("ECDH-ES+A256KW", someJwk),
                        new NotaryAccess("ECDH-ES+A256KW", someJwk),
                        new NotaryAccess("ECDH-ES+A256KW", someJwk)
                    ),
                    Period.ofDays(155),
                    true,
                    Json.createReader(
                            new StringReader("""
                                             {"some profile field":"other", "second field": "something"}
                                             """)).readObject(),
                    """
                    { "givenData": <credentialAugmentation> }
                    """,
                    neededTasks_oneOfTwoDocTasks(),
                    precondTasks_identify(),
                    taskDescriptions()
                    );
        } catch (JoseException ex) {
            throw new RuntimeException("Could not create JWT", ex);
        }
        profiles = Set.of(profile1,
                profile2,
                profile3,
                profile4,
                someProfile,
                profileWithOptionalPreCondition,
                profileWithOnlyPreCondition,
                augmentingProfile
                );
    }

    public static record Notary(
            UUID subject,
            String name,
            Set<ProfileId> roles,
            String token) {

        public String bearerValue() {
            return String.format("Bearer %s", this.token());
        }

        public Set<ProfileId> knownUnauthorizedRoles() {
            var unauthorizedRoles = new HashSet<>(profileIds);
            unauthorizedRoles.removeAll(this.roles);
            return unauthorizedRoles;
        }

        public String introspectionToken() {
            return String.format(
                    """
                    {
                      "sub": "%s",
                      "iss": "mocked-idp",
                      "username": "%s",
                      "roles": [
                        "default-roles-notarization-realm",
                        "offline_access",
                        "uma_authorization",
                        %s
                      ],
                      "exp": %d,
                      "typ": "Bearer",
                      "scope": "email profile %s",
                      "client_id": "portal-client",
                      "token_type": "bearer",
                      "active": true
                    }
                   """,
                    subject(),
                    name(),
                    roles.stream().map(r -> "\"" + r.id() + "\"").collect(Collectors.joining(",")),
                    Instant.now().plusSeconds(300).getEpochSecond(),
                    roles.stream().map(r -> r.id()).collect(Collectors.joining(" "))
            );
        }
    }
}
