/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.client;

import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.ListMapping;
import io.quarkus.cache.CacheResult;
import io.smallrye.mutiny.Uni;
import java.time.temporal.ChronoUnit;
import java.util.List;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import javax.ws.rs.GET;
import javax.ws.rs.DELETE;
import org.eclipse.microprofile.faulttolerance.Timeout;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;



/**
 *
 * @author Florian Otto
 */
@Path("/management")
@RegisterRestClient(configKey = "revocation-api")
public interface RevocationServiceRestClient {

    @Path("lists")
    @Timeout(value = 3, unit = ChronoUnit.SECONDS)
    @ClientHeaderParam(name = "Accept", value = MediaType.APPLICATION_JSON)
    @GET
    @CacheResult(cacheName = "listcred-profile-mapping-response")
    Uni<List<ListMapping>> getList();

    @Path("lists/{profileName}/entry/{idx}")
    @DELETE
    Uni<Void> revoke(String profileName, Long idx);
}
