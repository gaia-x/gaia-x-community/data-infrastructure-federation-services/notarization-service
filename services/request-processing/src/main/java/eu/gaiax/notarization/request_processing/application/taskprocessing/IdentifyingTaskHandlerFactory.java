/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.application.taskprocessing;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.request_processing.application.NotarizationRequestStore;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.exception.BadParameterException;
import eu.gaiax.notarization.request_processing.domain.exception.NotFoundException;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.VcTaskStart;
import java.net.URI;
import java.security.SecureRandom;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.ws.rs.Produces;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.RestClientBuilder;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class IdentifyingTaskHandlerFactory {

    private final NotarizationRequestStore notarizationRequestStore;
    private final URI internalUrl;
    private final SecureRandom secureRandom;
    private final Validator validator;

    public IdentifyingTaskHandlerFactory(
            NotarizationRequestStore notarizationRequestStore,
            @ConfigProperty(name = "notarization-processing.internal-url") URI internalUrl,
            Validator validator
    ) {
        this.notarizationRequestStore = notarizationRequestStore;
        this.internalUrl = internalUrl;
        this.validator = validator;
        this.secureRandom = new SecureRandom();
    }

    @Produces
    @ApplicationScoped
    public VCTaskHandler vcTaskHandler(
            @ConfigProperty(name = "vc-identification.url") URI identificationServiceUrl,
            ObjectMapper mapper,
            Validator validator
            ) {

        var client = RestClientBuilder.newBuilder()
                    .baseUri(identificationServiceUrl)
                    .build(BeginVCIdentificationApi.class);

        return new IdentifyingTaskHandler(
                internalUrl,
                notarizationRequestStore,
                (URI success, URI fail, JsonNode data, Session session) -> {
                    var processedData = mapper.convertValue(data, VcTaskStart.class);

                    Set<ConstraintViolation<VcTaskStart>> evaluation;
                    try {
                        evaluation = validator.validate(processedData);
                    } catch(IllegalArgumentException | ValidationException e) {
                        throw new BadParameterException("Missing or invalid data", e);
                    }

                    if (!evaluation.isEmpty()) {
                        throw new BadParameterException(evaluation.iterator().next().getMessage());
                    }

                    var request = new BeginVCIdentificationApi.BeginIdentificationRequest();
                    request.profileID = session.profileId;
                    request.holderDID = processedData.holderDID;
                    request.invitationURL = processedData.invitationURL;

                    return client.beginTask(success, fail, request);
                },
                secureRandom);
    }

    @Produces
    @ApplicationScoped
    public BrowserIdentificationHandler browserIdentificationHandler(@ConfigProperty(name = "browser-identification.url") URI identificationServiceUrl) {

        var client = RestClientBuilder.newBuilder()
                    .baseUri(identificationServiceUrl)
                    .build(BeginBrowserIdentificationApi.class);

        return new IdentifyingTaskHandler(
                internalUrl,
                notarizationRequestStore,
                (URI success, URI fail, JsonNode data, Session session) -> client.beginTask(success, fail)
                        .onItem().ifNull().failWith(() -> new NotFoundException("Identity service didn't return redirect.")),
                secureRandom);
    }
}
