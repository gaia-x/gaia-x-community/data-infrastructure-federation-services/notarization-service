/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.client;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import eu.gaiax.notarization.request_processing.domain.exception.NotFoundException;
import eu.gaiax.notarization.request_processing.domain.model.Profile;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.domain.services.ProfileService;
import io.quarkus.cache.CacheResult;
import io.smallrye.mutiny.Uni;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.temporal.ChronoUnit;
import java.util.List;
import org.eclipse.microprofile.faulttolerance.Timeout;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class ProfileServiceClient implements ProfileService {

    private final ProfileServiceRestClient profileClient;

    public ProfileServiceClient(
            @RestClient ProfileServiceRestClient profileClient) {
        this.profileClient = profileClient;
    }

    @Override
    @Timeout(value = 3, unit = ChronoUnit.SECONDS)
    @CacheResult(cacheName = "profile-by-id")
    public Uni<Profile> find(ProfileId id) {
        String idValue;
        try {
            idValue = URLEncoder.encode(id.id(), StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalArgumentException(ex);
        }
        return profileClient.findProfileAsUni(idValue)
                .onFailure(NotFoundException.class).recoverWithNull();
    }

    @Override
    @Timeout(value = 3, unit = ChronoUnit.SECONDS)
    @CacheResult(cacheName = "all-profiles")
    public Uni<List<Profile>> findAll() {
        return profileClient.findAllProfilesAsUni();
    }
}
