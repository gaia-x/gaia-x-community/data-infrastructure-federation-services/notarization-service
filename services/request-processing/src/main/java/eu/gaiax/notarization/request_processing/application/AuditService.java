/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.application;

import eu.gaiax.notarization.request_processing.domain.entity.HttpNotarizationRequestAudit;
import eu.gaiax.notarization.request_processing.domain.model.*;
import io.micrometer.core.instrument.MeterRegistry;
import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.quarkus.vertx.ConsumeEvent;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.core.eventbus.EventBus;
import java.time.OffsetDateTime;
import java.time.Period;
import javax.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class AuditService {

    private final MeterRegistry registry;
    private final Logger logger;

    @ConfigProperty(name = "notarization-processing.http.audit.logs.retention.period")
    Period auditLogsRetentionPeriod;

    public AuditService(MeterRegistry registry,
            Logger logger,
            EventBus bus) {
        this.registry = registry;
        this.logger = logger;
    }

    @ConsumeEvent(value = Channel.ON_AUDITABLE)
    @ReactiveTransactional
    public Uni<Void> process(OnAuditableHttp auditableEvent) {

        var audit = new HttpNotarizationRequestAudit();
        audit.id = auditableEvent.id();
        audit.requestUri = auditableEvent.requestUri();
        audit.sessionId = asStringId(auditableEvent.sessionId());
        audit.action = auditableEvent.action();
        audit.notarizationId = asStringId(auditableEvent.notarizationRequestId());
        audit.ipAddress = auditableEvent.ipAddress();
        audit.receivedAt = auditableEvent.receivedAt();
        audit.httpStatus = auditableEvent.httpStatus();
        audit.requestContent = auditableEvent.requestContent();
        audit.caller = auditableEvent.caller();
        audit.taskName = auditableEvent.taskName();
        return audit.<HttpNotarizationRequestAudit>persistAndFlush()
                .onItem().invoke(this::bumpAuditMetric)
                .onFailure().invoke(this::logAuditFailure).replaceWithVoid();
    }

    @ReactiveTransactional
    public Uni<Void> pruneAuditLogs() {
        return HttpNotarizationRequestAudit.delete(
            "createdAt < ?1",
            OffsetDateTime.now().minus(auditLogsRetentionPeriod)
        )
        .onItem().invoke(amountOfDeletedAuditLogs -> {
            registry.counter("http.audit.logs.prune").increment(amountOfDeletedAuditLogs);
        })
        .replaceWithVoid();
    }

    private void logAuditFailure(Throwable error) {
        logger.error("Could not create audit entry", error);
    }

    private void bumpAuditMetric(HttpNotarizationRequestAudit record) {
        registry.counter("audit.created", "action", record.action.name(), "httpStatus", Integer.toString(record.httpStatus));
    }

    private static String asStringId(SessionId id) {
        return id == null ? null : id.id;
    }
    private static String asStringId(NotarizationRequestId id) {
        return id == null ? null : id.id.toString();
    }

}
