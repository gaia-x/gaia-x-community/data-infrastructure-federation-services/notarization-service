/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.client;

import eu.gaiax.notarization.request_processing.domain.services.SignatureVerifierService;
import io.smallrye.mutiny.Uni;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.JsonValue;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

/**
 *
 * @author Michael Rauh
 */
@ApplicationScoped
public class DssSignatureVerifierClient implements SignatureVerifierService {

    private static final Logger LOG = Logger.getLogger(DssSignatureVerifierClient.class);

    private final DssRestClient dssClient;

    @Inject
    DssPolicyConfig dssPolicy;

    public DssSignatureVerifierClient(@RestClient DssRestClient dssClient) {
        this.dssClient = dssClient;
    }

    @Override
    public Uni<byte[]> verify(InputStream file) {
        var request = new DssRestClient.ValidateSignatureRequest();

        // Only apply a custom policy if present.
        if (this.dssPolicy.content().isPresent()) {
            var requestPolicy = new DssRestClient.Policy();
            requestPolicy.bytes = this.dssPolicy.content().get();
            request.policy = requestPolicy;
        }

        String fileEncoded;
        try {
            fileEncoded = Base64.getEncoder().encodeToString(file.readAllBytes());
        } catch (IOException ex) {
            LOG.warn("Cannot read bytes from InputStream", ex);
            throw new IllegalArgumentException(ex);
        }

        var signedDocument = new DssRestClient.SignedDocument();
        signedDocument.bytes = fileEncoded;
        signedDocument.name = "xades-detached.xml";
        request.signedDocument = signedDocument;

        Uni<JsonValue> dssReports = dssClient.validateSignature(request);

        return dssReports.map(givenReports -> {
            var reports = givenReports.asJsonObject();

            var etsiReportEncoded = reports.get("validationReportDataHandler");
            if (etsiReportEncoded == null) {
                LOG.error("The ETSI report was unexpectedly missing");
                return null;
            }

            var etsiReport = Base64.getMimeDecoder().decode(etsiReportEncoded.toString());
            if (LOG.isDebugEnabled()) {
                LOG.debugf("Received ETSI report: %s", new String(etsiReport));
            }

            return etsiReport;
        });
    }

}
