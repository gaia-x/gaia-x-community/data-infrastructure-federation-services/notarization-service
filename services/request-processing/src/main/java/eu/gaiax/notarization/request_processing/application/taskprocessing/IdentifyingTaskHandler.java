/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.application.taskprocessing;

import com.fasterxml.jackson.databind.JsonNode;
import eu.gaiax.notarization.request_processing.application.NotarizationRequestStore;
import eu.gaiax.notarization.request_processing.domain.entity.OngoingTask;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.entity.SessionTask;
import eu.gaiax.notarization.request_processing.domain.model.SessionId;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskId;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskInstance;
import eu.gaiax.notarization.request_processing.infrastructure.rest.Api;
import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.smallrye.mutiny.Uni;
import java.net.URI;
import java.security.SecureRandom;
import javax.ws.rs.core.UriBuilder;
import org.jboss.logging.Logger;
import java.util.Base64;
import org.eclipse.microprofile.rest.client.RestClientBuilder;

/**
 *
 * @author Florian Otto
 */
public class IdentifyingTaskHandler implements BrowserIdentificationHandler, VCTaskHandler {

    public static final Logger logger = Logger.getLogger(IdentifyingTaskHandler.class);
    private final NotarizationRequestStore notarizationRequestStore;
    private final URI internalUrl;
    private final RemoteServiceHandler remoteTaskApi;
    private final SecureRandom secureRandom;

    public IdentifyingTaskHandler(
        URI internalUrl,
        NotarizationRequestStore notarizationRequestStore,
        RemoteServiceHandler browserIdentifyService,
        SecureRandom secureRandom
    ){
        this.notarizationRequestStore = notarizationRequestStore;
        this.internalUrl = internalUrl;
        this.remoteTaskApi = browserIdentifyService;
        this.secureRandom = secureRandom;
    }

    @Override
    @ReactiveTransactional
    public Uni<TaskInstance> createTask(TaskId taskId, JsonNode data, Session session) {
        String nonce = createNonce();
        var successCBUri = UriBuilder.fromUri(internalUrl)
            .path(Api.Path.FINISH_TASK_RESOURCE)
            .path(nonce)
            .path(Api.Path.SUCCESS)
            .build();
        var failCBUri = UriBuilder.fromUri(internalUrl)
            .path(Api.Path.FINISH_TASK_RESOURCE)
            .path(nonce)
            .path(Api.Path.FAIL)
            .build();

        return
            Uni.createFrom().voidItem()
                .chain(() ->
                    begineRemoteIdentification(successCBUri, failCBUri, data, session)
                )
                .chain((uris)-> {
                    return OngoingTask.<OngoingTask>findById(taskId.id())
                        .onItem().ifNotNull()
                        //cancel ongoing if there is allready one
                        .call(o -> cancelRemote(o.cancelUri))
                        .onItem().ifNull()
                        //create new one
                        .continueWith(()->{
                            var o = new OngoingTask();
                            o.taskId = taskId.id();
                            return o;
                        })
                        .chain((o)->{
                            URI cancel;
                            URI redirect;
                            if (uris != null) {
                                cancel = uris.cancel;
                                redirect = uris.redirect;
                            } else {
                                cancel = null;
                                redirect = null;
                            }
                            o.nonce = nonce;
                            o.cancelUri = cancel;
                            return o.persistAndFlush()
                                .map((ign) -> new TaskInstance(redirect));
                        });
                });
    }

    private Uni<BeginTaskResponse> begineRemoteIdentification(URI success, URI fail, JsonNode data, Session session) {
        return this.remoteTaskApi.beginTask(success, fail, data, session);
    }

    @Override
    @ReactiveTransactional
    public Uni<Void> cancelTask(TaskId taskId) {
        return OngoingTask.<OngoingTask>find("taskId", taskId.id())
            .firstResult()
            .onItem().ifNotNull()
            .transformToUni((o) -> cancelRemote(o.cancelUri));
    }

    private Uni<Void> cancelRemote(URI cancelUri) {
        if (cancelUri != null) {
            return RestClientBuilder.newBuilder()
                    .baseUri(cancelUri)
                    .build(CancelRemoteTaskApi.class).cancel();
        } else {
            return Uni.createFrom().voidItem();
        }
    }

    @Override
    @ReactiveTransactional
    public Uni<Void> finishTaskSuccess(SessionTask sessionTask, JsonNode data) {
        return this.notarizationRequestStore.assignIdentity(new SessionId(sessionTask.session.id), data.toString())
            .onItem().invoke(()->{
                sessionTask.fulfilled = true;
            }) ;
    }

    @Override
    @ReactiveTransactional
    public Uni<Void> finishTaskFail(SessionTask sessionTask, JsonNode data) {
        return this.notarizationRequestStore.failIdentityAssignment(new SessionId(sessionTask.session.id));
    }

    public String createNonce(){
        return urlSafeString(secureRandom, new byte[64]);
    }

    public static String urlSafeString(SecureRandom secureRandom, byte[] tokenBuffer) {
        secureRandom.nextBytes(tokenBuffer);
        return Base64.getUrlEncoder().encodeToString(tokenBuffer);
    }

}
