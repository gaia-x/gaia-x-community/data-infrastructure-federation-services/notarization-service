/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.model.serialization;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.util.Map;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.lang.JoseException;

/**
 *
 * @author Neil Crossley
 */
public class PublicJsonWebKeyDeserializer extends StdDeserializer<PublicJsonWebKey> {

    public PublicJsonWebKeyDeserializer() {
        super(PublicJsonWebKey.class);
    }

    @Override
    public PublicJsonWebKey deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JacksonException {
        var rawJson = jp.readValueAs(Map.class);
        try {
            return PublicJsonWebKey.Factory.newPublicJwk(rawJson);
        } catch (JoseException ex) {
            throw new IllegalArgumentException(ex);
        }
    }
}
