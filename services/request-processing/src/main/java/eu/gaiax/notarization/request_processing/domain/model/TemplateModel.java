/*
 *
 */
package eu.gaiax.notarization.request_processing.domain.model;

/**
 *
 * @author Neil Crossley
 */
public final class TemplateModel {

    private TemplateModel() {}

    public static interface Document {
        String getId();
        String getTitle();
        String getShortDescription();
        String getMimetype();
        String getExtension();
        DocumentHash getSha3_256();
        DocumentHash getSha3();
        DocumentHash getSha();
        Document getSelf();
    }

    public static interface DocumentHash {
        String getHex();
        String getBase();
        String getBase64();
        String getBase64URLSafe();
    }
}
