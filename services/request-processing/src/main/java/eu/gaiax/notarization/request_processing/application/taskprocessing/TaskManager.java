/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.application.taskprocessing;

import com.fasterxml.jackson.databind.JsonNode;
import eu.gaiax.notarization.request_processing.domain.entity.OngoingTask;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.entity.SessionTask;
import eu.gaiax.notarization.request_processing.domain.exception.InvalidRequestStateException;
import eu.gaiax.notarization.request_processing.domain.exception.InvalidTaskStateException;
import eu.gaiax.notarization.request_processing.domain.exception.NotFoundException;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.domain.model.SessionId;
import eu.gaiax.notarization.request_processing.domain.model.TaskTreeForCalculating;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskId;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskInstance;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskType;
import eu.gaiax.notarization.request_processing.domain.services.ProfileService;
import eu.gaiax.notarization.request_processing.infrastructure.rest.feature.audit.CallbackAuditingFilter;
import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpServerRequest;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.InternalServerErrorException;

import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;

/**
 *
 * @author Florian Otto
 */
@ApplicationScoped
public class TaskManager {

    @Inject
    BrowserIdentificationHandler btHandler;
    @Inject
    FileUploadHandler fileUploadHandler;
    @Inject
    ProfileService profileService;
    @Inject
    VCTaskHandler vcHandler;
    @Inject
    HttpServerRequest request;

    public static final Logger logger = Logger.getLogger(TaskManager.class);

    @ReactiveTransactional
    public Uni<TaskInstance> startTask(TaskId taskId, JsonNode data){

        return SessionTask.<SessionTask>findById(taskId.id())
            .onItem().ifNull().failWith(() -> new NotFoundException("Task"))
            .chain(sessionTask -> {
                var handler = getHandlerByType(sessionTask.type);
                return handler.createTask(taskId, data, sessionTask.session)
                    .call(()->{
                        sessionTask.running = true;
                        return sessionTask.persistAndFlush();
                    });
        });
    }

    @ReactiveTransactional
    public Uni<Void> cancelTask(TaskId taskId){

        return SessionTask.<SessionTask>findById(taskId.id())
            .onItem().ifNull().failWith(() -> new NotFoundException("Task"))
            .chain(sessionTask -> {
                return getHandlerByType(sessionTask.type).cancelTask(taskId)
                    .chain(()->{
                        sessionTask.running = false;
                        sessionTask.fulfilled = false;
                        return sessionTask.persistAndFlush().replaceWithVoid();
                    });
        });
    }

    @ReactiveTransactional
    public Uni<Void> cancelTasks(Session session){
        return Mutiny.fetch(session.tasks)
            .chain((dbSession)->{
                return Uni.combine().all().unis(
                    Stream.concat(
                        Stream.of(Uni.createFrom().voidItem()),
                        session.tasks.stream()
                            .filter(t -> t.running)
                            .map((sessionTask)->{
                                return getHandlerByType(sessionTask.type)
                                    .cancelTask(new TaskId(sessionTask.taskId))
                                    .chain(()-> {
                                        sessionTask.running = false;
                                        sessionTask.fulfilled = false;
                                        return sessionTask.persist();
                                    });
                            })
                    )
                    .collect(Collectors.toList())
                ).usingConcurrencyOf(1).discardItems();
            });
    }

    @ReactiveTransactional
    public Uni<Void> finishTaskSuccess(TaskId taskId, JsonNode data) {
        return SessionTask.<SessionTask>findById(taskId.id())
            .onItem().ifNull().failWith(()-> {
                return new NotFoundException("TaskId");
            })
            .onItem().ifNotNull().invoke((sessTask)->{
                CallbackAuditingFilter.storeTaskName(request, sessTask.name);
                CallbackAuditingFilter.storeSessionId(request, sessTask.session.id);
            })
            .chain((sessTask)-> {
                if(!sessTask.running){
                    throw new InvalidTaskStateException("Cannot finish not running task.");
                }
                sessTask.running = false;

                if(!sessTask.session.state.isTaskProcessingAllowed()){
                    throw new InvalidRequestStateException(new SessionId(sessTask.session.id), sessTask.session.state.taskProcessingStates, sessTask.session.state);
                }

                return getHandlerByType(sessTask.type)
                    .finishTaskSuccess(sessTask, data)
                    .chain(() -> {
                        final Session session = sessTask.session;
                        if (!sessTask.fulfilled || session.state != NotarizationRequestState.CREATED) {
                            return Uni.createFrom().voidItem();
                        } else {
                            return Uni.combine().all().unis(
                                    this.profileService.find(new ProfileId(session.profileId)),
                                    Mutiny.fetch(session.tasks)
                                ).asTuple()
                                .chain(vals -> {
                                    var profile = vals.getItem1();
                                    var tasks = vals.getItem2();
                                    if (profile == null) {
                                        throw new InternalServerErrorException(String.format("Could not complete a task because the profile with id %s is missing", session.profileId));
                                    }
                                    var calcTree = TaskTreeForCalculating.buildTree(
                                        profile.preconditionTasks(),
                                        tasks
                                    );
                                    if (calcTree.fulfilled()) {
                                        session.state = NotarizationRequestState.SUBMITTABLE;
                                        return session.persist().replaceWithVoid();
                                    } else {
                                        return Uni.createFrom().voidItem();
                                    }
                                });
                        }
                    })
                    ;

            }).replaceWithVoid();
    }

    @ReactiveTransactional
    public Uni<Void> finishTaskSuccess(String nonce, JsonNode data) {
        return OngoingTask.<OngoingTask>find("nonce", nonce).firstResult()
            .onItem().ifNull().failWith(()->new NotFoundException("Task"))
            .chain(t -> {
                t.nonce = null;
                return t.persist()
                    .chain(()-> finishTaskSuccess(new TaskId(t.taskId), data));
            });
    }

    @ReactiveTransactional
    public Uni<Void> finishTaskFail(TaskId taskId, JsonNode data) {
        return SessionTask.<SessionTask>findById(taskId.id())
            .onItem().ifNull().failWith(()->new NotFoundException("TaskId"))
            .chain((sessTask)-> {
                if(!sessTask.running){
                    throw new InvalidTaskStateException("Cannot finish not running task.");
                }
                sessTask.running = false;
                CallbackAuditingFilter.storeTaskName(request, sessTask.name);
                CallbackAuditingFilter.storeSessionId(request, sessTask.session.id);

                return getHandlerByType(sessTask.type)
                    .finishTaskFail(sessTask, data)
                    ;

            }).replaceWithVoid();
    }

    @ReactiveTransactional
    public Uni<Void> finishTaskFail(String nonce, JsonNode data) {

        return OngoingTask.<OngoingTask>find("nonce", nonce).firstResult()
            .onItem().ifNull().failWith(()->new NotFoundException("Task"))
            .chain(t -> {
                t.nonce = null;
                return t.persist().chain(() ->
                    finishTaskFail(new TaskId(t.taskId), data)
                );
            });
    }

    private TaskHandler getHandlerByType(TaskType type) throws IllegalArgumentException {
        switch (type) {
            case BROWSER_IDENTIFICATION_TASK : return btHandler;
            case VC_IDENTIFICATION_TASK : return vcHandler;
            case FILEPROVISION_TASK : return fileUploadHandler;
            default: throw new IllegalArgumentException();
        }
    }
}
