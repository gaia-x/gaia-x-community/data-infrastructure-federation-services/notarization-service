
  dss:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/dss-demo-webapp:5.10.1
    container_name: dss
    ports:
      - "8080"

  request-processing-db:
    image: postgres:14
    container_name: request-processing-db
    ports:
      - "5434:5432"
    environment:
      POSTGRES_USER: request
      POSTGRES_PASSWORD: request
      POSTGRES_DB: requests_database
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U request -d requests_database"]
      interval: 5s
      timeout: 5s
      retries: 5

  request-processing-flyway:
    image: flyway/flyway:8.5
    container_name: request-processing-flyway
    command: "-locations=filesystem:/sql-migrations -url=jdbc:postgresql://request-processing-db:5432/requests_database -schemas=public -user=request -password=request -connectRetries=60 migrate"
    volumes:
      - ../../../services/request-processing/src/main/jib/db-flyway/:/sql-migrations
    depends_on:
      request-processing-db:
        condition: service_healthy

  rabbitmq:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/rabbitmq-dev:3.9-management
    container_name: 'rabbitmq'
    ports:
        - 5671:5671   # amqp/tls
        - 5672:5672   # amqp
        - 15672:15672 # http
        - 15692:15692 # prometheus
    environment:
      RABBITMQ_DEFAULT_USER: request-rabbit
      RABBITMQ_DEFAULT_PASS: request-rabbit-password
    volumes:
      - ../../../services/request-processing/deploy/docker-compose/infra/rabbitmq.conf:/etc/rabbitmq/rabbitmq.conf
      - ../../../services/request-processing/deploy/docker-compose/infra/rabbitmq_definitions.json:/etc/rabbitmq/definitions.json
      - ../../../services/request-processing/deploy/docker-compose/infra/ca_certificate.pem:/notarization/ca_certificate.pem
      - ../../../services/request-processing/deploy/docker-compose/infra/server_notarization_certificate.pem:/notarization/server_certificate.pem
      - ../../../services/request-processing/deploy/docker-compose/infra/server_notarization_key.pem:/notarization/server_key.pem

  keycloak:
    image: quay.io/keycloak/keycloak:${KEYCLOAK_VERSION:-18.0.0}
    ports:
      - "${KEYCLOAK_PORT:-9194}:${KEYCLOAK_PORT:-9194}"
    command: "start-dev --http-port=${KEYCLOAK_PORT:-9194} --log-level=INFO"
    environment:
      - KEYCLOAK_ADMIN=${KEYCLOAK_ADMIN:-keycloak}
      - KEYCLOAK_PORT=${KEYCLOAK_PORT:-9194}
      - KEYCLOAK_ADMIN_PASSWORD=${KEYCLOAK_ADMIN_PASSWORD:-keycloakcd}
      - KC_HOSTNAME=localhost
      - DB_DATABASE=${KEYCLOAK_DATABASE_NAME:-keycloakdb}
      - DB_USER=${KEYCLOAK_DATABASE_USER:-keycloakdb}
      - DB_PASSWORD=${KEYCLOAK_DATABASE_PASSWORD:-keycloakdb}
      - DB_ADDR=keycloakdb
      - DB_VENDOR=postgres
    networks:
      default:
        aliases:
          - keycloak
    depends_on:
      - keycloakdb
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:${KEYCLOAK_PORT:-9194}"]
      start_period: 50s
      interval: 5s
      timeout: 5s
      retries: 150

  keycloakdb:
    image: postgres:${POSTGRES_VERSION:-14}
    container_name: keycloak-db
    ports:
      - "5433:5432"
    environment:
      - POSTGRES_USER=${KEYCLOAK_DATABASE_USER:-keycloakdb}
      - POSTGRES_PASSWORD=${KEYCLOAK_DATABASE_PASSWORD:-keycloakdb}
      - POSTGRES_DB=${KEYCLOAK_DATABASE_NAME:-keycloakdb}
    networks:
      default:

  keycloak-initial-config-importer:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/keycloak-jq:18.0.0
    container_name: keycloak-config
    depends_on:
      keycloak:
        condition: service_healthy
    environment:
      - KEYCLOAK_ADMIN=${KEYCLOAK_ADMIN:-keycloak}
      - KEYCLOAK_ADMIN_PASSWORD=${KEYCLOAK_ADMIN_PASSWORD:-keycloakcd}
      - KEYCLOAK_PORT=9194
      - KC_HOSTNAME=keycloak
    volumes:
      - ../../../services/request-processing/deploy/docker-compose/infra/keycloak_init.sh:/tmp/keycloak_init.sh
    restart: on-failure
    entrypoint: "/tmp/keycloak_init.sh"
    networks:
      default:
