/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.management;

import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import static eu.gaiax.notarization.request_processing.Helper.withTransactionAsync;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import io.quarkus.test.common.QuarkusTestResource;
import javax.inject.Inject;
import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import java.util.HashSet;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@Tag("security")
@QuarkusTestResource(MockServicesLifecycleManager.class)
public class FetchAvailableRequestPermissionsTest {

    public static final String REQUESTS_PATH = "/api/v1/requests";
    public static final String REQUEST_PATH = "/api/v1/profiles/{profileId}/requests/{notarizationRequestId}";

    private int totalRecordsInserted = 100;

    @Inject
    Mutiny.SessionFactory sessionFactory;

    @BeforeEach
    public void setup() {
        pruneDB();
        totalRecordsInserted = 0;

        IntStream.range(0, 3).forEach((c) -> {
            for (ProfileId currentProfile : MockState.profileIds) {
                totalRecordsInserted++;
                createRequestWithStateInDB(NotarizationRequestState.READY_FOR_REVIEW, currentProfile);
            }
        });
    }

    private void pruneDB() {
        withTransactionAsync(sessionFactory, (session, tx) -> {
            return session.createQuery("delete NotarizationRequest").executeUpdate();
        }).await().indefinitely();
    }

    private String createRequestWithStateInDB(NotarizationRequestState state, ProfileId profile) {

        var id = UUID.randomUUID();
        withTransactionAsync(sessionFactory, (session, tx) -> {

            var sess = new Session();
            sess.id = UUID.randomUUID().toString();
            sess.state = state;
            sess.profileId = profile.id();

            var nr = new NotarizationRequest();
            nr.id = id;
            nr.session = sess;

            return session.persist(sess).chain(() -> session.persist(nr));
        }).await().indefinitely();

        return id.toString();
    }

    @ParameterizedTest(name = "{index} Notary {0} accesses correct requests")
    @ArgumentsSource(AllMockNotariesArgumentsProvider.class)
    public void canOnlyFetchRequestsByProfileRoles(UUID subject) {
        var givenNotary = MockState.notariesBySubject.get(subject);
        var expectedRoles = givenNotary.roles().stream().map(r -> r.id()).collect(Collectors.toList());
        var unexpectedRoles = givenNotary.knownUnauthorizedRoles().stream().map(r -> r.id()).collect(Collectors.toList());

        var response = given()
                .header("Authorization", givenNotary.bearerValue())
                .queryParam("offset", 0)
                .queryParam("limit", totalRecordsInserted)
                .when()
                .get(REQUESTS_PATH)
                .then()
                .statusCode(200)
                .extract();

        var resultProfileIds = new HashSet<>(response.jsonPath().getList("notarizationRequests.profileId"));
        assertThat(resultProfileIds,
                describedAs("Notary %0 to access to profiles %1", hasItems(expectedRoles.toArray()), givenNotary.name(), expectedRoles));
        assertThat(resultProfileIds,
                describedAs("Notary %0 not to have access to profiles %1", not(hasItems(unexpectedRoles.toArray())), givenNotary.name(), unexpectedRoles));
    }

    public static class AllMockNotariesArgumentsProvider implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
            /*
             * The use of subject is a workaround to avoid the de-/serialization of
                types currently not supported by x-stream.

                Waiting for version 1.5
                https://github.com/x-stream/xstream/issues/168
             */
            return MockState.notaries.stream().map(n -> Arguments.of(n.subject()));
        }
    }
}
