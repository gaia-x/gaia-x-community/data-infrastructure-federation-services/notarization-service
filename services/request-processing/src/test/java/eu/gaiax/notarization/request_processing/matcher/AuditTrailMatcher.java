/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.matcher;

import eu.gaiax.notarization.request_processing.domain.entity.HttpNotarizationRequestAudit;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.SessionId;
import static eu.gaiax.notarization.request_processing.Helper.withTransactionAsync;
import io.smallrye.mutiny.TimeoutException;
import io.smallrye.mutiny.Uni;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import org.hamcrest.Matcher;
import static org.hamcrest.Matchers.*;
import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
public class AuditTrailMatcher {

    private final static Logger logger = Logger.getLogger(AuditTrailMatcher.class);

    public static List<HttpNotarizationRequestAudit> auditTrailFor(String sessionId,
            NotarizationRequestAction action,
            Mutiny.SessionFactory sessionFactory) {
        return auditTrailFor(sessionId, action, 1, sessionFactory);
    }

    public static List<HttpNotarizationRequestAudit> auditTrailForNotarizationRequestID(
        String notReqId,
        NotarizationRequestAction action,
        int total,
        Mutiny.SessionFactory sessionFactory
    ){
        var queryString = "from HttpNotarizationRequestAudit audit where notarizationId = :id";
        try {
            return selectAuditLogs(queryString, notReqId, action, total, sessionFactory)
                .await().atMost(Duration.ofMillis(400));
        }
        catch (TimeoutException ex){
            return List.of();
        }
    }

    public static List<HttpNotarizationRequestAudit> auditTrailFor(String sessionId,
            NotarizationRequestAction action,
            int total,
            Mutiny.SessionFactory sessionFactory) {

        var queryString = "from HttpNotarizationRequestAudit audit where sessionId = :id";
        try {
            return selectAuditLogs(queryString, sessionId, action, total, sessionFactory)
                .await().atMost(Duration.ofMillis(200));
        }
        catch (TimeoutException ex){
            return List.of();
        }
    }

    private static Uni<List<HttpNotarizationRequestAudit>> selectAuditLogs(
            String queryString,
            String id,
            NotarizationRequestAction action,
            int total,
            Mutiny.SessionFactory sessionFactory) {
        return withTransactionAsync(sessionFactory, (dbSession, transaction) -> {
            logger.debug("Selecting audit logs");

            var query = dbSession.<HttpNotarizationRequestAudit>createQuery(queryString);
            query.setParameter("id", id);

            return query.getResultList();
        }).onItem().transformToUni(entries -> {
            int count = 0;
            for (HttpNotarizationRequestAudit entry : entries) {
                if (entry.action == action) {
                    count++;
                }
                if (count >= total) {
                    logger.debug("Found expected audit logs");
                    return Uni.createFrom().item(entries);
                }
            }

            logger.debug("Retry - select the audit logs again");
            return selectAuditLogs(queryString, id, action, total, sessionFactory);
        });
    }

    public static List<HttpNotarizationRequestAudit> auditTrailFor(SessionId sessionId,
            NotarizationRequestAction action,
            int count,
            Mutiny.SessionFactory sessionFactory) {
        return auditTrailFor(sessionId.id, action, count, sessionFactory);
    }

    public static Matcher<Iterable<HttpNotarizationRequestAudit>> hasAuditEntries(AuditLogMatcherBuilder... builders) {

        List<Matcher<HttpNotarizationRequestAudit>> result = new ArrayList<>();
        for (AuditLogMatcherBuilder builder : builders) {
            result.add(builder.matcher());
        }

        return hasItems(result.stream()
                .toArray(Matcher[]::new));
    }

    public static AuditLogMatcherBuilder auditEntry() {
        return new AuditLogMatcherBuilder();
    }
    public static AuditLogMatcherBuilder auditLogMarkUnready() {
        return auditEntry().action(NotarizationRequestAction.MARK_UNREADY)
                    .httpStatus(204);
    }
    public static AuditLogMatcherBuilder auditFetchDocument() {
        return auditEntry().action(NotarizationRequestAction.FETCH_DOCUMENT)
                    .httpStatus(200);
    }
    public static AuditLogMatcherBuilder auditUploadDocument() {
        return auditEntry().action(NotarizationRequestAction.UPLOAD_DOCUMENT)
                    .httpStatus(204);
    }
    public static AuditLogMatcherBuilder auditDeleteDocument() {
        return auditEntry().action(NotarizationRequestAction.DELETE_DOCUMENT)
                    .httpStatus(204);
    }
    public static AuditLogMatcherBuilder auditAssignDid() {
        return auditEntry().action(NotarizationRequestAction.ASSIGN_DID)
                    .httpStatus(204);
    }
    public static AuditLogMatcherBuilder auditLogFetch() {
        return auditEntry().action(NotarizationRequestAction.FETCH)
                    .httpStatus(200);
    }
    public static AuditLogMatcherBuilder auditLogMarkReady() {
        return auditEntry().action(NotarizationRequestAction.MARK_READY)
                    .httpStatus(200);
    }
    public static AuditLogMatcherBuilder auditLogCreateSession() {
        return auditEntry().action(NotarizationRequestAction.CREATE_SESSION)
                    .httpStatus(201);
    }
    public static AuditLogMatcherBuilder auditLogSubmit() {
        return auditEntry().action(NotarizationRequestAction.SUBMIT)
                    .httpStatus(201);
    }
    public static AuditLogMatcherBuilder auditLogUpdate() {
        return auditEntry().action(NotarizationRequestAction.UPDATE)
                    .httpStatus(204);
    }
    public static AuditLogMatcherBuilder auditLogRevoke() {
        return auditEntry().action(NotarizationRequestAction.REVOKE)
                    .httpStatus(204);
    }
    public static AuditLogMatcherBuilder auditLogClaim() {
        return auditEntry().action(NotarizationRequestAction.CLAIM)
                    .httpStatus(204);
    }
    public static AuditLogMatcherBuilder auditLogAccept() {
        return auditEntry().action(NotarizationRequestAction.ACCEPT)
                    .httpStatus(204)
                    .hasCaller();
    }
    public static AuditLogMatcherBuilder auditLogReject() {
        return auditEntry().action(NotarizationRequestAction.REJECT)
                    .httpStatus(204);
    }
    public static AuditLogMatcherBuilder auditLogNotaryDelete() {
        return auditEntry().action(NotarizationRequestAction.NOTARY_DELETE)
                    .httpStatus(204);
    }
    public static AuditLogMatcherBuilder auditLogFetchIdentity() {
        return auditEntry().action(NotarizationRequestAction.FETCH_IDENTITY)
                    .httpStatus(200);
    }
    public static AuditLogMatcherBuilder auditLogFinSuccess() {
        return auditEntry().action(NotarizationRequestAction.ISSUANCE_FINISH_SUCCESS)
                    .httpStatus(204);
    }
    public static AuditLogMatcherBuilder auditLogFinFail() {
        return auditEntry().action(NotarizationRequestAction.ISSUANCE_FINISH_FAIL)
                    .httpStatus(204);
    }
    public static AuditLogMatcherBuilder auditLogManualRel() {
        return auditEntry().action(NotarizationRequestAction.MANUAL_RELEASE)
                    .httpStatus(204);
    }
    public static AuditLogMatcherBuilder auditLogTaskStart() {
        return auditEntry().action(NotarizationRequestAction.TASK_START)
                    .httpStatus(201)
                    .taskNameNotEmpty();
    }
    public static AuditLogMatcherBuilder auditLogTaskCancel() {
        return auditEntry().action(NotarizationRequestAction.TASK_CANCEL)
                    .httpStatus(204)
                    .taskNameNotEmpty();
    }
    public static AuditLogMatcherBuilder auditLogTaskFinish() {
        return auditEntry().action(NotarizationRequestAction.TASK_FINISH_SUCCESS)
                    .httpStatus(204)
                    .taskNameNotEmpty();
    }
    public static AuditLogMatcherBuilder auditLogTaskFail() {
        return auditEntry().action(NotarizationRequestAction.TASK_FINISH_FAIL)
                    .httpStatus(204)
                    .taskNameNotEmpty();
    }
    public static AuditLogMatcherBuilder auditLogAssignCredentialOverride() {
        return auditEntry().action(NotarizationRequestAction.CREDENTIAL_AUGMENTATION_PUT)
                    .httpStatus(204)
                    .hasCaller();
    }
}
