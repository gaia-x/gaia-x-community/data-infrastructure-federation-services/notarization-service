/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.matcher;

import eu.gaiax.notarization.request_processing.domain.entity.HttpNotarizationRequestAudit;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestId;
import static eu.gaiax.notarization.request_processing.matcher.FieldMatcher.hasField;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.IntFunction;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

/**
 *
 * @author Neil Crossley
 */
public class AuditLogMatcherBuilder {

        private Matcher<HttpNotarizationRequestAudit> action;
        private Matcher<HttpNotarizationRequestAudit> requestId;
        private List<Matcher<HttpNotarizationRequestAudit>> matchers = new ArrayList<>();
        private Matcher<HttpNotarizationRequestAudit> ipAddress;
        private Matcher<HttpNotarizationRequestAudit> httpStatus;
        private Matcher<HttpNotarizationRequestAudit> taskNameNotEmpty;
        private Matcher<HttpNotarizationRequestAudit> requestContext;
        private Matcher<HttpNotarizationRequestAudit> callerNotEmpty;

        public AuditLogMatcherBuilder action(NotarizationRequestAction action) {
            this.action = hasField("action", equalTo(action));
            return this;
        }

        public AuditLogMatcherBuilder requestId(UUID id) {
            this.requestId = hasField("notarizationId", equalTo(id));
            return this;
        }

        public AuditLogMatcherBuilder requestId(NotarizationRequestId id) {
            return this.requestId(id.id);
        }

        public AuditLogMatcherBuilder with(Matcher<HttpNotarizationRequestAudit> matcher) {
            this.matchers.add(matcher);
            return this;
        }

        public AuditLogMatcherBuilder ipAddress(String ipAddress) {
            this.ipAddress = hasField("ipAddress", equalTo(ipAddress));
            return this;
        }

        public AuditLogMatcherBuilder httpStatus(int httpStatus) {
            this.httpStatus = hasField("httpStatus", equalTo(httpStatus));
            return this;
        }

        public AuditLogMatcherBuilder taskNameNotEmpty() {
            this.taskNameNotEmpty = hasField("taskName", notNullValue());
            return this;
        }

        public AuditLogMatcherBuilder hasCaller() {
            this.callerNotEmpty = hasField("caller", notNullValue());
            return this;
        }

        public AuditLogMatcherBuilder requestContent(String requestContent) {
            this.requestContext = hasField("requestContent", equalTo(requestContent));
            return this;
        }

        public static <T> T[] newArray(Class<T[]> type, int size) {
            return type.cast(Array.newInstance(type.getComponentType(), size));
        }

        @SuppressWarnings("unchecked")
        static <T, R extends T> IntFunction<R[]> genericArray(IntFunction<T[]> arrayCreator) {
            return size -> (R[]) arrayCreator.apply(size);
        }

        public Matcher<HttpNotarizationRequestAudit> matcher() {
            List<Matcher<HttpNotarizationRequestAudit>> result = new ArrayList<>();
            result.addAll(this.matchers);
            if (this.requestId != null) {
                result.add(requestId);
            }
            if (this.action != null) {
                result.add(action);
            }
            if (this.ipAddress != null) {
                result.add(ipAddress);
            }
            if (this.httpStatus != null) {
                result.add(httpStatus);
            }
            if (this.taskNameNotEmpty != null){
                result.add(taskNameNotEmpty);
            }
            if (this.requestContext != null) {
                result.add(requestContext);
            }
            if (this.callerNotEmpty != null) {
                result.add(callerNotEmpty);
            }

            return Matchers.allOf(result.stream()
                    .toArray(Matcher[]::new));
        }
}
