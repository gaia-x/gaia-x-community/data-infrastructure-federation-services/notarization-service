/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.submission;

import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import static eu.gaiax.notarization.request_processing.Helper.*;
import static io.restassured.RestAssured.given;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.Test;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@QuarkusTestResource(MockServicesLifecycleManager.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
public class UpdateContactTest {

    private static final String MARK_READY_PATH = SUBMISSION_PATH + "/ready";
    public static final String PATH = "/api/v1/session/{sessionId}/updateContact";

    @Inject
    Mutiny.SessionFactory sessionFactory;

    static Stream<NotarizationRequestState> allowedStates() {
        return Stream.of(
            NotarizationRequestState.EDITABLE
        );
    }

    static Stream<NotarizationRequestState> notAllowedStates() {
        var updateable = allowedStates().collect(Collectors.toSet());
        return Stream.of(NotarizationRequestState.values()).filter((s) -> !updateable.contains(s));
    }

    @Test
    public void canMarkReadyOnlyWithFulfilledTasks() {

        var session = prepareSessionWithSubmittedNotarizationRequest(sessionFactory).session();

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken())
            .when()
            .post(MARK_READY_PATH)
            .then()
            .statusCode(400);

        //simulate ready tasks
        withTransaction(sessionFactory, (sess, tx) -> {
            return sess.createQuery("update SessionTask st set st.fulfilled = true").executeUpdate();

        });

        given()
            //.contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken())
            .body("emil@email.de")
            .when()
            .put(PATH)
            .then()
            .statusCode(204);

    }
}
