/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.matcher;

import java.lang.reflect.Field;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 *
 * @author Neil Crossley
 */
public class FieldMatcher<T, V> extends TypeSafeMatcher<T> {

    private final String fieldName;
    private final Matcher<V> matcher;

    public FieldMatcher(String fieldName, Matcher<V> matcher) {
        this.fieldName = fieldName;
        this.matcher = matcher;
    }

    public static <T,V> FieldMatcher<T,V> hasField(String name, Matcher<V> matcher) {
        return new FieldMatcher<>(name, matcher);
    }

    @Override
    protected boolean matchesSafely(T item) {
        Field foundField;
        try {
            foundField = item.getClass().getField(fieldName);
        } catch (NoSuchFieldException ex) {
            try {
                foundField = item.getClass().getDeclaredField(fieldName);
            } catch (NoSuchFieldException | SecurityException ex1) {
                return false;
            }
        } catch (SecurityException ex) {
            return false;
        }
        foundField.setAccessible(true);
        V foundValue;
        try {
            foundValue = (V)foundField.get(item);
        } catch (IllegalArgumentException | IllegalAccessException | ClassCastException ex) {
            return false;
        }
        return matcher.matches(foundValue);
    }

    @Override
    protected void describeMismatchSafely(T item, Description mismatchDescription) {
        Field foundField;

        try {
            foundField = item.getClass().getField(fieldName);
        } catch (NoSuchFieldException ex) {
            try {
                foundField = item.getClass().getDeclaredField(fieldName);
            } catch (NoSuchFieldException ex1) {
                mismatchDescription.appendText("was missing field named ")
                        .appendValue(this.fieldName)
                        .appendText(" on ")
                        .appendValue(item);
                return;
            } catch (SecurityException ex1) {
                mismatchDescription.appendText("could not access field name ")
                        .appendValue(this.fieldName)
                        .appendText(" on ")
                        .appendValue(item);
                return;
            }
        } catch (SecurityException ex) {
            mismatchDescription.appendText("could not access field name ")
                    .appendValue(this.fieldName)
                    .appendText(" on ")
                    .appendValue(item);
            return;
        }
        foundField.setAccessible(true);
        Object foundValue;
        try {
            foundValue = foundField.get(item);
        } catch (IllegalArgumentException ex) {
            mismatchDescription.appendText("could not access field named ")
                    .appendValue(this.fieldName)
                    .appendText(" on ")
                    .appendValue(item);
            return;
        } catch (IllegalAccessException ex) {
            mismatchDescription.appendText("could not access value of field named ")
                    .appendValue(this.fieldName)
                    .appendText(" on ")
                    .appendValue(item);
            return;
        }
        matcher.describeMismatch(foundValue, mismatchDescription);
    }

    @Override
    public void describeTo(Description d) {
        d.appendText("a field ")
                .appendValue(this.fieldName)
                .appendText(" [")
                .appendDescriptionOf(this.matcher)
                .appendValue("]");
    }

}
