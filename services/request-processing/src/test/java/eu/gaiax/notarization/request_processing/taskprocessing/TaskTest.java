/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.taskprocessing;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.delete;
import static com.github.tomakehurst.wiremock.client.WireMock.noContent;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static eu.gaiax.notarization.request_processing.Helper.SESSION_VARIABLE;
import static eu.gaiax.notarization.request_processing.Helper.SUBMISSION_PATH;
import static eu.gaiax.notarization.request_processing.Helper.prepareSessionWithSubmittedNotarizationRequest;
import static eu.gaiax.notarization.request_processing.Helper.withTransaction;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskType;
import io.quarkus.test.common.QuarkusTestResource;
import javax.inject.Inject;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import static io.restassured.RestAssured.given;
import java.util.Random;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.Test;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;
import eu.gaiax.notarization.IdentityWireMock;
import eu.gaiax.notarization.MockIdentityServiceResource;
import eu.gaiax.notarization.request_processing.Helper;
import static eu.gaiax.notarization.request_processing.Helper.prepareSessionWithState;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.SessionTaskSummary;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditTrailFor;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.hasAuditEntries;
import java.net.URISyntaxException;
import java.util.Arrays;
import javax.ws.rs.core.MediaType;
/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@QuarkusTestResource(MockIdentityServiceResource.class)
public class TaskTest {

    public static final Logger logger = Logger.getLogger(TaskTest.class);

    public static final String TASK_PATH = "/api/v1/session/{sessionId}/task";

    private static final Random random = new Random();
    public static ObjectMapper mapper = new ObjectMapper();

    @Inject
    Mutiny.SessionFactory sessionFactory;

    @IdentityWireMock
    WireMockServer wiremock;

    public void stubConfig(){
        var sb = new StringBuffer();
        sb
            .append("{")
            .append("\"redirect\":\"http://localhost:")
            .append(wiremock.port())
            .append("/login/NONCE\"")
            .append(",")
            .append("\"cancel\":\"http://localhost:")
            .append(wiremock.port())
            .append("/session/NONCE\"")
            .append("}")
        ;

        var redirectFromIdentityService = sb.toString();

        wiremock.resetAll();
        wiremock.addMockServiceRequestListener(
                (in, out) -> requestReceived(in, out));
        wiremock.stubFor(
            post(urlMatching("/session.+success.+failure.+"))
                .willReturn(
                   aResponse()
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                        .withBody(redirectFromIdentityService)
                        .withStatus(200)
                )
        );

        var urlPatCancel= "/session.+";
        wiremock.stubFor(delete(urlMatching(urlPatCancel))
            .willReturn(
                noContent()
            ));

    }

    protected void requestReceived(Request inRequest, Response inResponse) {
        logger.debugv(" WireMock stub identity service request at URL: {0}", inRequest.getAbsoluteUrl());
        logger.debugv(" WireMock stub identity service request headers: \n{0}", inRequest.getHeaders());
        logger.debugv(" WireMock stub identity service request body: \n{0}", inRequest.getBodyAsString());
        logger.debugv(" WireMock stub identity service response body: \n{0}", inResponse.getBodyAsString());
        logger.debugv(" WireMock stub identity service response headers: \n{0}", inResponse.getHeaders());
    }

    @Test
    public void startTask(){
        stubConfig();

        //use profile without preconTasks
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.profileId1);

        var sessionInfo = sessionWithNotarizationRequest.session();
        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");

        var tasklist = Arrays.asList(mapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .findAny().orElseThrow();

        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .queryParam("taskId", task.taskId)
            .when()
            .post(TASK_PATH)
            .then()
            .statusCode(201)
            .extract().body().path("uri");

//        assertStateInStoredSession(sessionWithNotarizationRequest.session().id(), sessionFactory, NotarizationRequestState.ACCEPTED);

        withTransaction(sessionFactory, (dbSession, transaction) -> {
            return dbSession.find(Session.class, sessionWithNotarizationRequest.session().id())
            .call(s -> Mutiny.fetch(s.tasks))
            .invoke(foundSession -> {
                var sessTask = foundSession.tasks.stream().filter((t)-> t.taskId.equals(task.taskId)).findFirst().orElseThrow();
                assertThat(sessTask.taskId, is(task.taskId));
                assertThat("Expected state started of SessionTask was not correct", sessTask.running, is(true));
            });
        });

        assertThat(auditTrailFor(sessionInfo.id(), NotarizationRequestAction.TASK_START, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogTaskStart())
        );
    }

    @Test
    public void cancelTaskWithSubmitableRequest(){

        //use profile without preconTasks
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.profileId1);

        cancelTask(sessionWithNotarizationRequest.session());
    }

    @Test
    public void cancelTaskWithCreatedRequest(){

        //use profile without preconTasks
        var sessionInfo = prepareSessionWithState(NotarizationRequestState.CREATED, sessionFactory, MockState.profileWithOnlyPreConditionId);

        cancelTask(sessionInfo);
    }

    private void cancelTask(Helper.SessionInfo sessionInfo) throws IllegalArgumentException {
        //var sessionInfo =sessionWithNotarizationRequest.session();
        //call fetch session to get taskids
        var tasks = given()
                .accept(ContentType.JSON)
                .header("token", sessionInfo.accessToken())
                .when().get(sessionInfo.location())
                .then()
                .statusCode(200)
                .extract()
                .path("tasks");

        var tasklist = Arrays.asList(mapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
                .findAny().orElseThrow();

        given()
                .contentType(ContentType.JSON)
                .pathParam(SESSION_VARIABLE, sessionInfo.id())
                .header("token", sessionInfo.accessToken())
                .queryParam("taskId", task.taskId)
                .when()
                .post(TASK_PATH)
                .then()
                .statusCode(201)
                ;

        withTransaction(sessionFactory, (dbSession, transaction) -> {
            return dbSession.find(Session.class, sessionInfo.id())
                    .call(s -> Mutiny.fetch(s.tasks))
                    .invoke(foundSession -> {
                        var sessTask = foundSession.tasks.stream().filter((t)-> t.taskId.equals(task.taskId)).findFirst().orElseThrow();
                        assertThat(sessTask.taskId, is(task.taskId));
                        assertThat("Expected state of SessionTask was not correct", sessTask.running, is(true));
                    });
        });

        given()
                .pathParam(SESSION_VARIABLE, sessionInfo.id())
                .header("token", sessionInfo.accessToken())
                .param("taskId", task.taskId.toString())
                .when()
                .delete(TASK_PATH)
                .then()
                .statusCode(204)
                ;

        withTransaction(sessionFactory, (dbSession, transaction) -> {
            return dbSession.find(Session.class, sessionInfo.id())
                    .call(s -> Mutiny.fetch(s.tasks))
                    .invoke(foundSession -> {
                        var sessTask = foundSession.tasks.stream().filter((t)-> t.taskId.equals(task.taskId)).findFirst().orElseThrow();
                        assertThat(sessTask.taskId, is(task.taskId));
                        assertThat("Expected state of SessionTask was not correct", sessTask.running, is(false));
                    });
        });

        assertThat(auditTrailFor(sessionInfo.id(), NotarizationRequestAction.TASK_CANCEL, sessionFactory),
                hasAuditEntries(AuditTrailMatcher.auditLogTaskCancel())
        );
    }

    @Test
    public void onlyPreconditionTasksStartableWhenNotYetFinished() throws URISyntaxException {

        //starts with "some profile" -> which has preconditiontask identifiy and normal tasks for download
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.someProfileId);
        var sessionInfo =sessionWithNotarizationRequest.session();
        stubConfig();

        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .body()
            .path("tasks");
            ;

        var tasklist = Arrays.asList(mapper.convertValue(tasks, SessionTaskSummary[].class ));
        var uploadtask = tasklist.stream().filter((t)->t.type == TaskType.FILEPROVISION_TASK)
            .findAny().orElseThrow();

        //wrong task leads to 400
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .queryParam("taskId", uploadtask.taskId.toString())
            .when()
            .post(TASK_PATH)
            .then()
            .statusCode(400)
            ;


        var identTask = tasklist.stream().filter((t)->t.type == TaskType.BROWSER_IDENTIFICATION_TASK)
            .findAny().orElseThrow();

        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .queryParam("taskId", identTask.taskId)
            .when()
            .post(TASK_PATH)
            .then()
            .statusCode(201)
            ;

        //hijack success url
        var successUri = wiremock.getAllServeEvents().stream()
            .map(e -> e.getRequest().getQueryParams())
            .map(e -> e.get("success"))
            .filter(v -> v.isPresent())
            .findFirst().get().values().get(0);

        given()
            .header("Content-Type", "application/json")
            .body("{}")
            .when()
            .post(successUri)
            .then()
            .statusCode(204)
            ;

        assertThat(auditTrailFor(sessionInfo.id(), NotarizationRequestAction.TASK_FINISH_SUCCESS, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogTaskFinish())
        );
        //now starting the upload task should be fine
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .queryParam("taskId", uploadtask.taskId)
            .when()
            .post(TASK_PATH)
            .then()
            .statusCode(201)
            ;

    }

    @Test
    public void failedTaskGetsAudit() throws URISyntaxException {

        //starts with "some profile" -> which has preconditiontask identifiy and normal tasks for download
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.someProfileId);
        var sessionInfo =sessionWithNotarizationRequest.session();
        stubConfig();

        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .body()
            .path("tasks");
            ;

        var tasklist = Arrays.asList(mapper.convertValue(tasks, SessionTaskSummary[].class ));

        var identTask = tasklist.stream().filter((t)->t.type == TaskType.BROWSER_IDENTIFICATION_TASK)
            .findAny().orElseThrow();

        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .queryParam("taskId", identTask.taskId)
            .when()
            .post(TASK_PATH)
            .then()
            .statusCode(201)
            ;

        //hijack fail url
        var failUri = wiremock.getAllServeEvents().stream()
            .map(e -> e.getRequest().getQueryParams())
            .map(e -> e.get("failure"))
            .filter(v -> v.isPresent())
            .findFirst().get().values().get(0);

        given()
            .header("Content-Type", "application/json")
            .body("{}")
            .when()
            .post(failUri)
            .then()
            .statusCode(204)
            ;

        assertThat(auditTrailFor(sessionInfo.id(), NotarizationRequestAction.TASK_FINISH_FAIL, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogTaskFail())
        );

    }

    public void finishTaskOnlyWorksInCorrectState(){

        //starts with "some profile" -> which has preconditiontask identifiy and normal tasks for download
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.someProfileId);
        var sessionInfo =sessionWithNotarizationRequest.session();
        stubConfig();

        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .body()
            .path("tasks")
            ;

        var tasklist = Arrays.asList(mapper.convertValue(tasks, SessionTaskSummary[].class ));

        var identTask = tasklist.stream().filter((t)->t.type == TaskType.BROWSER_IDENTIFICATION_TASK)
            .findAny().orElseThrow();

        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .queryParam("taskId", identTask.taskId)
            .when()
            .post(TASK_PATH)
            .then()
            .statusCode(201)
            ;

        //hijack success url
        var successUri = wiremock.getAllServeEvents().stream()
            .map(e -> e.getRequest().getQueryParams())
            .map(e -> e.get("success"))
            .filter(v -> v.isPresent())
            .findFirst().get().values().get(0);


        //change session state to non valid taskfinish state
        Helper.setSessionState(sessionInfo, NotarizationRequestState.READY_FOR_REVIEW, sessionFactory);

        given()
            .header("Content-Type", "application/json")
            .body("{}")
            .when()
            .post(successUri)
            .then()
            .statusCode(400)
            ;
        assertThat(auditTrailFor(sessionInfo.id(), NotarizationRequestAction.TASK_FINISH_SUCCESS, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogTaskCancel())
        );
    }

    @Test
    public void deleteSessionCancelsOngoingTasks(){

        stubConfig();
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.profileId1);


        var session = sessionWithNotarizationRequest.session();
        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", session.accessToken())
            .when().get(session.location())
            .then()
            .statusCode(200)
            .extract()
            .body()
            .path("tasks");
            ;

        //startall
        var tasklist = Arrays.asList(mapper.convertValue(tasks, SessionTaskSummary[].class ));
        tasklist.stream().filter((t)->t.type == TaskType.FILEPROVISION_TASK)
            .forEach((st)-> {
                given()
                    .contentType(ContentType.JSON)
                    .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
                    .header("token", sessionWithNotarizationRequest.session().accessToken())
                    .queryParam("taskId", st.taskId)
                    .when()
                    .post(TASK_PATH)
                    .then()
                    .statusCode(201)
                    ;
            });

        //call fetch session to get them again
        tasks = given()
            .accept(ContentType.JSON)
            .header("token", session.accessToken())
            .when().get(session.location())
            .then()
            .statusCode(200)
            .extract()
            .body()
            .path("tasks");
            ;

        Arrays.asList(mapper.convertValue(tasks, SessionTaskSummary[].class ))
            .stream().forEach((st) -> assertThat(st.running, is(true)));

        //cancel session
        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken())
            .when().delete(SUBMISSION_PATH)
            .then()
            .statusCode(204);

        //call fetch session to get them again
        tasks = given()
            .accept(ContentType.JSON)
            .header("token", session.accessToken())
            .when().get(session.location())
            .then()
            .statusCode(200)
            .extract()
            .body()
            .path("tasks");
            ;

        Arrays.asList(mapper.convertValue(tasks, SessionTaskSummary[].class ))
            .stream().forEach((st) -> assertThat(st.running, is(false)));


    }

}
