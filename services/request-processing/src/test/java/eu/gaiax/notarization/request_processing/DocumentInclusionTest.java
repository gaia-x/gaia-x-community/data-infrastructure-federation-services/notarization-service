/*
 *
 */
package eu.gaiax.notarization.request_processing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.matchingJsonPath;
import eu.gaiax.notarization.MockSsiIssuanceLifecycleManager;
import eu.gaiax.notarization.SsiIssuanceWireMock;
import eu.gaiax.notarization.request_processing.domain.entity.Document;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.AipVersion;
import eu.gaiax.notarization.request_processing.domain.model.Profile;
import eu.gaiax.notarization.request_processing.infrastructure.rest.client.SsiIssuanceClient;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.vertx.RunOnVertxContext;
import io.quarkus.test.vertx.UniAsserter;
import java.time.OffsetDateTime;
import java.time.Period;
import java.util.Base64;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.inject.Inject;
import javax.json.JsonValue;
import org.apache.commons.codec.binary.Hex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Neil Crossley
 */
@QuarkusTest
@QuarkusTestResource(MockSsiIssuanceLifecycleManager.class)
public class DocumentInclusionTest {

    @SsiIssuanceWireMock
    WireMockServer issuanceWireMock;

    @Inject
    SsiIssuanceClient ssiIssuanceClient;

    @Inject
    ObjectMapper mapper;

    @BeforeEach
    public void setup() {

        issuanceWireMock.resetRequests();
    }

    @Test
    @RunOnVertxContext
    public void givenAbsentTemplateThenDataIsUnchanged(UniAsserter asserter) {
        var inputRequest = someRequest();
        String givenNullTemplate = null;

        asserter.assertThat(
                () -> ssiIssuanceClient.issue(
                        inputRequest,
                        someProfileWithDocumentTemplate(givenNullTemplate)),
                result -> {
                    issuanceWireMock.verify(
                            WireMock.postRequestedFor(WireMock.urlMatching("/credential/start-issuance/"))
                                    .withRequestBody(matchingJsonPath("$.credentialData", equalToJson(inputRequest.data)))
                    );
                });
    }

    @Test
    @RunOnVertxContext
    public void givenStaticTemplateThenDataIsCorrectMergedResult(UniAsserter asserter) {
        var inputRequest = someRequest();
        var givenStaticTemplate = String.format(
                """
                { "%s": "%s" }
                """,
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        asserter.assertThat(
                () -> ssiIssuanceClient.issue(
                        inputRequest,
                        someProfileWithDocumentTemplate(givenStaticTemplate)),
                result -> {
                    String expectedData = mergeJson(inputRequest.data, givenStaticTemplate);
                    issuanceWireMock.verify(
                            WireMock.postRequestedFor(WireMock.urlMatching("/credential/start-issuance/"))
                                    .withRequestBody(matchingJsonPath("$.credentialData", equalToJson(expectedData)))
                    );
                });
    }

    @Test
    @RunOnVertxContext
    public void givenExampleAip1TemplateThenRequestIsCorrect(UniAsserter asserter) {
        var inputRequest = someRequest();
        var givenHash1 = new byte[]{1, 2, 3, 4};
        var givenHash2 = new byte[]{3, 4, 5, 6};
        var givenHash3 = new byte[]{6, 7, 8, 9};
        inputRequest.session.documents = Set.of(
                // These UUIDs are sorted to ensure document order is kept
                someDocument(UUID.fromString("25169b2b-a657-46d8-b3ce-429f1e895656"), givenHash1),
                someDocument(UUID.fromString("97fa3276-8e00-4814-8d16-13998798a8a8"), givenHash2),
                someDocument(UUID.fromString("d8925a3c-5e60-4cc0-bcfd-1cebe7d44dc9"), givenHash3)
        );
        var givenTemplate
                = """
                { "evidenceDocument": "<documents:{doc|<doc.sha3_256.base64>}; separator=", ">" }
                """;
        asserter.assertThat(
                () -> ssiIssuanceClient.issue(
                        inputRequest,
                        someProfileWithDocumentTemplate(givenTemplate)),
                result -> {
                    String expectedData = mergeJson(
                            inputRequest.data,
                            String.format(
                                    """
                                    { "evidenceDocument": "%s, %s, %s" }
                                    """,
                                    Base64.getEncoder().encodeToString(givenHash1),
                                    Base64.getEncoder().encodeToString(givenHash2),
                                    Base64.getEncoder().encodeToString(givenHash3)));
                    issuanceWireMock.verify(
                            WireMock.postRequestedFor(WireMock.urlMatching("/credential/start-issuance/"))
                                    .withRequestBody(matchingJsonPath("$.credentialData", equalToJson(expectedData)))
                    );
                });
    }

    @Test
    @RunOnVertxContext
    public void givenExampleAip2TemplateThenRequestIsCorrect(UniAsserter asserter) {
        var inputRequest = someRequest();
        var givenHash1 = new byte[]{1, 2, 3, 4};
        var givenHash2 = new byte[]{3, 4, 5, 6};
        var givenHash3 = new byte[]{6, 7, 8, 9};
        inputRequest.session.documents = Set.of(
                // These UUIDs are sorted to ensure document order is kept
                someDocument(UUID.fromString("25169b2b-a657-46d8-b3ce-429f1e895656"), givenHash1),
                someDocument(UUID.fromString("97fa3276-8e00-4814-8d16-13998798a8a8"), givenHash2),
                someDocument(UUID.fromString("d8925a3c-5e60-4cc0-bcfd-1cebe7d44dc9"), givenHash3)
        );
        var givenTemplate =
                """
                { "credentialSubject": { "evidenceDocument": [ <documents:{doc|"<doc.sha3_256.base64>"}; separator=" ,">  ] } }
                """;
        asserter.assertThat(
                () -> ssiIssuanceClient.issue(
                        inputRequest,
                        someProfileWithDocumentTemplate(AipVersion.V2_0, givenTemplate)),
                result -> {
                    String expectedSubject = mergeJson(
                            inputRequest.data,
                            String.format(
                                    """
                                    { "id": "%s", "evidenceDocument": ["%s", "%s", "%s"] }
                                    """,
                                    inputRequest.did,
                                    Base64.getEncoder().encodeToString(givenHash1),
                                    Base64.getEncoder().encodeToString(givenHash2),
                                    Base64.getEncoder().encodeToString(givenHash3)));
                    String expectedData = String.format(
                            """
                            { "credentialSubject": %s }
                            """,
                            expectedSubject);
                    issuanceWireMock.verify(
                            WireMock.postRequestedFor(WireMock.urlMatching("/credential/start-issuance/"))
                                    .withRequestBody(matchingJsonPath("$.credentialData", equalToJson(expectedData)))
                    );
                });
    }

    private String mergeJson(String inputRequest, String givenStaticTemplate) throws IllegalArgumentException {
        String expectedData;
        try {
            expectedData = mapper.readerForUpdating(mapper.readTree(inputRequest))
                    .readTree(givenStaticTemplate)
                    .toPrettyString();
        } catch (JsonProcessingException ex) {
            throw new IllegalArgumentException(ex);
        }
        return expectedData;
    }

    public Profile someProfileWithDocumentTemplate(String documentTemplate) {
        return someProfileWithDocumentTemplate(AipVersion.V1_0, documentTemplate);
    }
    public Profile someProfileWithDocumentTemplate(AipVersion aip, String documentTemplate) {

        var result = new Profile(
                UUID.randomUUID().toString(),
                aip,
                "name+" + UUID.randomUUID(),
                "description" + UUID.randomUUID(),
                "SomeValue",
                List.of(),
                Period.ofWeeks(3),
                true,
                JsonValue.TRUE,
                documentTemplate,
                null,
                null,
                List.of());
        return result;
    }

    public Document someDocument(byte[] hash) {
        return someDocument(UUID.randomUUID(), hash);
    }

    public Document someDocument(UUID id, byte[] hash) {
        var result = new Document();
        result.id = id;
        result.content = DataGen.genBytes();
        result.hash = Hex.encodeHexString(hash);
        result.title = "Title:" + UUID.randomUUID();
        result.shortDescription = "ShortDesc:" + UUID.randomUUID();
        result.longDescription = "LongDesc:" + UUID.randomUUID();
        return result;
    }

    public NotarizationRequest someRequest() {

        Session resultSession = new Session();
        resultSession.id = UUID.randomUUID().toString();
        resultSession.accessToken = UUID.randomUUID().toString();
        resultSession.documents = Set.of();
        resultSession.createdAt = OffsetDateTime.now().minusHours(1);
        resultSession.successCBUri = "http://localhost/" + UUID.randomUUID();
        resultSession.failCBUri = "http://localhost/" + UUID.randomUUID();

        NotarizationRequest resultRequest = new NotarizationRequest();
        resultRequest.createdAt = resultSession.createdAt.plusMinutes(1);
        resultRequest.claimedBy = "Some Notary";
        resultRequest.data = String.format(
                """
                { "name": "%s" }
                """, UUID.randomUUID());
        resultRequest.did = "did:dad:" + UUID.randomUUID();
        resultRequest.id = UUID.randomUUID();
        resultRequest.lastModified = resultRequest.createdAt.plusMinutes(20);
        resultRequest.rejectComment = null;
        resultRequest.requestorInvitationUrl = null;
        resultRequest.ssiInvitationUrl = null;
        resultRequest.session = resultSession;
        resultSession.request = resultRequest;

        return resultRequest;
    }
}
