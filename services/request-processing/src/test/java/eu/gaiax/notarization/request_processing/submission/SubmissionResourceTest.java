/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.submission;

import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.UUID;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import static eu.gaiax.notarization.request_processing.Helper.*;

import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.Test;

import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.SubmitNotarizationRequest;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

@QuarkusTest
@QuarkusTestResource(MockServicesLifecycleManager.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
public class SubmissionResourceTest {

    @Inject
    Mutiny.SessionFactory sessionFactory;

    @Test
    public void canCreateSession() {
        var response = given()
            .accept(ContentType.JSON)
            .contentType(ContentType.JSON)
            .body(String.format("""
                                {"profileId":"%s"}
                                """, MockState.someProfileId.id()))
            .when().post("/api/v1/session")
            .then()
            .statusCode(201)
            .header("Location", containsString("/api/v1/session/"))
            .body("token", is(instanceOf(String.class)))
            .body("sessionId", is(instanceOf(String.class)))
            .extract();

        var actualLocation = response.header("Location");
        var sessionId = response.path("sessionId").toString();
        assertThat(actualLocation, endsWith(sessionId));

        assertThat(auditTrailFor(sessionId, NotarizationRequestAction.CREATE_SESSION, 1, sessionFactory), hasAuditEntries(
            auditLogCreateSession()
        ));
    }

    @Test
    public void createSessionUsingProfileWithOptionalPreconditionTasksEndsInSubmittableSession(){

        var session = prepareSession(MockState.profileWithOptionalPreConditionId);
        given()
            .accept(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken())
            .when().get(SESSION_PATH)
            .then()
            .body("state", is("submittable"))
            ;

    }

    @Test
    public void createSessionUsingProfileWithPreconditionLeadsToSessionInCreatedState(){

        var session = prepareSession(MockState.profileId4);
        given()
            .accept(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken())
            .when().get(SESSION_PATH)
            .then()
            .body("state", is("created"))
            ;
    }

    @Test
    public void canNotCreateSessionWithoutProfile() {
        given()
            .accept(ContentType.JSON)
            .contentType(ContentType.JSON)
            .when().post("/api/v1/session")
            .then()
            .statusCode(400)
            .extract();
    }

    @Test
    public void canSubmitRequestGivenNewSessionInfo() {
        var session = prepareSessionWithState(NotarizationRequestState.SUBMITTABLE, sessionFactory);
        var inputRequest = createSubmitNotarizationRequest();

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .body(inputRequest)
            .header("token", session.accessToken())
            .when().post(SUBMISSION_PATH)
            .then()
            .statusCode(201)
            .header("Location", is(notNullValue()));

        assertDataExistsInDB(session, inputRequest);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.SUBMIT, 1, sessionFactory), hasAuditEntries(
            auditLogCreateSession(),
            auditLogSubmit()
        ));
    }

    private void assertDataExistsInDB(SessionInfo session, SubmitNotarizationRequest inputRequest) {
        var actual = getSession(session);
        assertThat(actual.request.data, is(inputRequest.data.toString()));
    }

    private Session getSession(SessionInfo sessionInfo) {
        return withTransaction(sessionFactory, (dbSession, transaction) -> {
            return dbSession.find(Session.class, sessionInfo.id());
        });
    }

    @Test
    public void givenSubmissionThenHasCorrectState() {
        var inputState = NotarizationRequestState.SUBMITTABLE;
        var session = prepareSessionWithState(inputState, sessionFactory);
        var inputRequest = createSubmitNotarizationRequest();

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .body(inputRequest)
            .header("token", session.accessToken())
            .when().post(SUBMISSION_PATH)
            .then()
            .statusCode(201);

        var actual = getSession(session);
        assertThat(actual, not(nullValue()));
        assertThat(actual.state, equalTo(NotarizationRequestState.EDITABLE));
    }

    @Test
    public void givenMissingTokenThenCannotSubmitRequest() {

        var session = prepareSession();
        var notarizationRequest = createSubmitNotarizationRequest();
        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .body(notarizationRequest)
            .when().post(SUBMISSION_PATH)
            .then()
            .statusCode(400);
        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.SUBMIT, 1, sessionFactory), hasAuditEntries(
            auditLogCreateSession(),
            auditLogSubmit().httpStatus(400)
        ));
    }

    @Test
    public void givenInvalidTokenThenCannotSubmitRequest() {
        String inputInvalidToken = UUID.randomUUID().toString();

        var session = prepareSession();
        var notarizationRequest = createSubmitNotarizationRequest();

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .body(notarizationRequest)
            .header("token", inputInvalidToken)
            .when().post(SUBMISSION_PATH)
            .then()
            .statusCode(401);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.SUBMIT, 1, sessionFactory),
            hasAuditEntries(auditLogCreateSession(),
                auditLogSubmit().httpStatus(401)));
    }

    @Test
    public void givenInvalidSessionThenCannotSubmitRequest() {

        var invalidSessionID = UUID.randomUUID().toString();

        var session = prepareSession();
        var notarizationRequest = createSubmitNotarizationRequest();

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, invalidSessionID)
            .body(notarizationRequest)
            .header("token", session.accessToken())
            .when().post(SUBMISSION_PATH)
            .then()
            .statusCode(404);

        assertThat(auditTrailFor(invalidSessionID, NotarizationRequestAction.SUBMIT, 1, sessionFactory),
            allOf(hasAuditEntries(auditLogSubmit().httpStatus(404)),
                not(hasAuditEntries(auditLogCreateSession()))));
    }

    @Test
    public void givenInvalidJsonLdThenCannotSubmitRequest() {
        var session = prepareSessionWithState(NotarizationRequestState.SUBMITTABLE, sessionFactory);
        var notarizationRequest = createSubmitNotarizationRequest(
                asJson("""
                       {
                         "@context": "https://schema.org/ContextDoesNotExistHere",
                         "@type": "Person",
                         "name": "Jane Doe",
                         "jobTitle": "Professor",
                         "telephone": "(425) 123-4567",
                         "url": "http://www.janedoe.com"
                       }
                       """)
        );

        given().contentType(ContentType.JSON)
                .pathParam(SESSION_VARIABLE, session.id())
                .body(notarizationRequest)
                .header("token", session.accessToken())
                .when().post(SUBMISSION_PATH)
                .then()
                .statusCode(400);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.SUBMIT, 1, sessionFactory),
                hasAuditEntries(
                        auditLogCreateSession(),
                        auditLogSubmit().httpStatus(400)
                ));
    }

    @Test
    public void givenValidJsonLdThenCanSubmitRequest() {
        var session = prepareSessionWithState(NotarizationRequestState.SUBMITTABLE, sessionFactory);
        var notarizationRequest = createSubmitNotarizationRequest(
                asJson("""
                       {
                         "@context": "https://schema.org/",
                         "@type": "Person",
                         "name": "Jane Doe",
                         "jobTitle": "Professor",
                         "telephone": "(425) 123-4567",
                         "url": "http://www.janedoe.com"
                       }
                       """)
        );

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .body(notarizationRequest)
            .header("token", session.accessToken())
            .when().post(SUBMISSION_PATH)
            .then()
            .statusCode(201);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.SUBMIT, 1, sessionFactory), hasAuditEntries(
            auditLogCreateSession(),
            auditLogSubmit()
        ));
    }

    @Test
    public void givenUsedSessionThenCannotSubmitRequest() {
        var session = prepareSession(MockState.profileId1);
        var notarizationRequest = createSubmitNotarizationRequest();

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .body(notarizationRequest)
            .header("token", session.accessToken())
            .when().post(SUBMISSION_PATH)
            .then()
            .statusCode(201);

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .body(notarizationRequest)
            .header("token", session.accessToken())
            .when().post(SUBMISSION_PATH)
            .then()
            .statusCode(400);
    }

    private static JsonNode asJson(String value) {
        try {
            return new ObjectMapper().readTree(value);
        } catch (JsonProcessingException ex) {
            throw new RuntimeException(ex);
        }
    }
}
