/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import static com.github.tomakehurst.wiremock.client.WireMock.matchingJsonPath;
import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.MockSsiIssuanceLifecycleManager;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import eu.gaiax.notarization.SsiIssuanceWireMock;
import static eu.gaiax.notarization.request_processing.Helper.withTransaction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import static eu.gaiax.notarization.request_processing.Helper.withTransactionAsync;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.entity.RequestorIdentity;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditLogFinFail;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditLogFinSuccess;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditTrailFor;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.hasAuditEntries;
import io.quarkus.test.common.QuarkusTestResource;
import javax.inject.Inject;
import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import java.math.BigInteger;
import java.net.MalformedURLException;

import java.util.UUID;
import javax.ws.rs.core.UriBuilder;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@QuarkusTestResource(MockServicesLifecycleManager.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
@QuarkusTestResource(MockSsiIssuanceLifecycleManager.class)
public class SessionTerminationAfterIssuanceTest {

    public static final String REQUESTS_PATH = "/api/v1/requests";
    public static final String REQUEST_PATH = "/api/v1/profiles/{profileId}/requests/{notarizationRequestId}/accept";

    @Inject
    ObjectMapper objectMapper;

    @Inject
    Mutiny.SessionFactory sessionFactory;

    @SsiIssuanceWireMock
    WireMockServer issuanceWireMock;

    @ConfigProperty(name = "quarkus.http.port")
    Integer assignedPort;

    public static record NotAndSessId(
        String notReqId,
        String sessId
    ){};

    private NotAndSessId createRequestWithStateInDB(NotarizationRequestState state, boolean didSet, ProfileId profile) {

        var notReqId = UUID.randomUUID();
        var sessId = UUID.randomUUID();
        withTransactionAsync(sessionFactory, (session, tx) -> {

            var sess =new Session();
            sess.id = sessId.toString();
            sess.state = state;
            sess.profileId = profile.id();

            var nr = new NotarizationRequest();
            nr.id = notReqId;
            nr.session = sess;

            if(didSet){
                nr.did = "did.action:value";
                nr.requestorInvitationUrl = "some-invitation";
            }

            return session.persist(sess).chain(() -> session.persist(nr));
        }).await().indefinitely();

        return new NotAndSessId(
            notReqId.toString(),
            sessId.toString()
        );

    }

    private void assertStateInDB(String id, NotarizationRequestState state){
        withTransactionAsync(sessionFactory, (dbSession, tx) -> {
            return dbSession.find(Session.class, id).invoke((session -> {
                assertThat(session.state, is(state));
            }));
        }).await().indefinitely();
    }

    @Test
    public void successCB_finishesSessionToIssued() throws MalformedURLException {

        var profile = MockState.someProfileId;
        var ids = createRequestWithStateInDB(NotarizationRequestState.WORK_IN_PROGRESS, true, profile);

        addIdentities(ids.sessId);

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", ids.notReqId())
            .pathParam("profileId", profile.id())
            .when()
            .post(REQUEST_PATH)
            .then()
            .statusCode(204);

        issuanceWireMock.verify(
            WireMock.postRequestedFor(WireMock.urlMatching("/credential/start-issuance/"))
            .withRequestBody(matchingJsonPath("$.successURL", WireMock.containing("finishNotarizationRequest")))
        );

        var successUri = issuanceWireMock.getAllServeEvents().stream()
            .map(e -> e.getRequest().getBodyAsString())
            .filter(s -> s != null && !s.isEmpty())
            .map(str -> {
                try {
                    var js = objectMapper.readTree(str);
                    return js.get("successURL").asText();
                } catch (JsonProcessingException ex) {
                    throw new RuntimeException(ex);
                }
            })
            .findFirst().orElseThrow();

        successUri = fixUrl(successUri);

        given().when()
            .post(successUri)
            .then()
            ;

        assertThat(UUID.fromString(ids.sessId), sessionCleaned);
        assertStateInDB(ids.sessId, NotarizationRequestState.ISSUED);

        assertThat(auditTrailFor(ids.sessId(), NotarizationRequestAction.ISSUANCE_FINISH_SUCCESS, sessionFactory),
            hasAuditEntries(auditLogFinSuccess())
        );

    }

    @Test
    public void failCB_finishesSessionToTerminated() throws MalformedURLException {

        var profile = MockState.someProfileId;
        var ids = createRequestWithStateInDB(NotarizationRequestState.WORK_IN_PROGRESS, true, profile);

        addIdentities(ids.sessId);

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", ids.notReqId())
            .pathParam("profileId", profile.id())
            .when()
            .post(REQUEST_PATH)
            .then()
            .statusCode(204);

        issuanceWireMock.verify(
            WireMock.postRequestedFor(WireMock.urlMatching("/credential/start-issuance/"))
            .withRequestBody(matchingJsonPath("$.successURL", WireMock.containing("finishNotarizationRequest")))
        );

        var failureURL = issuanceWireMock.getAllServeEvents().stream()
            .map(e -> e.getRequest().getBodyAsString())
            .filter(s -> s != null && !s.isEmpty())
            .map(str -> {
                try {
                    var js = objectMapper.readTree(str);
                    return js.get("failureURL").asText();
                } catch (JsonProcessingException ex) {
                    throw new RuntimeException(ex);
                }
            })
            .findFirst().orElseThrow();

        failureURL = fixUrl(failureURL);

        given().when()
            .post(failureURL)
            .then()
            ;

        assertStateInDB(ids.sessId, NotarizationRequestState.TERMINATED);
        assertThat(UUID.fromString(ids.sessId), sessionCleaned);

        assertThat(auditTrailFor(ids.sessId(), NotarizationRequestAction.ISSUANCE_FINISH_FAIL, sessionFactory),
            hasAuditEntries(auditLogFinFail())
        );

    }

    private String fixUrl(String url) throws MalformedURLException {
        return UriBuilder.fromUri(url).port(assignedPort).build().toURL().toString();
    }

    private UUID addIdentities(String sessionId){
        var identiyId = UUID.randomUUID();
        withTransaction(sessionFactory, (session, tx) -> {
            var q = session.<Session>createQuery("from Session s left join fetch s.identities where s.id = :id");
            q.setParameter("id", sessionId);
            return q.getSingleResult()
                .call(s -> {
                    var identiy = new RequestorIdentity();
                    identiy.id = identiyId;
                    identiy.session = s;
                    s.identities.add(identiy);
                    return session.persist(s);
                });
            });
        return identiyId;
    }

    public BaseMatcher<UUID> sessionCleaned = new BaseMatcher<UUID>() {
        private String reason="";
        @Override
        public boolean matches(Object id) {
            reason = "";
            var s = withTransaction(sessionFactory, (session, tx) -> {
                return session.<Session>find(Session.class, id.toString());
            });
            //we also check if identities are deleted just to be sure
            var identities_count = withTransaction(sessionFactory, (session, tx) -> {
                var q = session.<BigInteger>createNativeQuery("select count(id) from requestor_identity r where r.session_id = :sid");
                q.setParameter("sid", id.toString());
                return q.getSingleResult();
            });

            var documents_count = withTransaction(sessionFactory, (session, tx) -> {
                var q = session.<BigInteger>createNativeQuery(" select count(id) from document d where d.session_id = :sid");
                q.setParameter("sid", id.toString());
                return q.getSingleResult();
            });

            if(s != null && (s.state == NotarizationRequestState.TERMINATED || s.state == NotarizationRequestState.ISSUED)){
                if(identities_count.intValue() != 0){
                    reason += "found identity, count was: " + identities_count.toString();
                    return false;
                }
                if(documents_count.intValue() != 0){
                    reason += "found documents with no association, count was: " + documents_count.toString();
                    return false;
                }
                return true;
            }else{
                if(s == null){
                    reason += "found no session";
                } else {
                    reason += "found session with state: '"+ s.state + "' instead of TERMINATED or ISSUED";
                }
                return false;
            }
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("a session with terminated state in database");
        }

        @Override
        public void describeMismatch(Object item, Description description) {
            description.appendText("<" + item.toString() + "> was errornous, reason is: " + reason);
        }

    };
}
