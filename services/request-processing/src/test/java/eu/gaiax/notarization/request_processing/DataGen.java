/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.request_processing.domain.model.DocumentId;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.DocumentUploadByLink;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 *
 * @author Florian Otto
 */
public class DataGen {

    private static final int MAX_DEPTH = 5;
    private static final int MAX_FIELDNUMBER = 25;
    private static final int MAX_STR_LEN = 512;
    private static final int MAX_BYTEA_LEN = 512;
    private static final int MAX_LST_LEN = 50;
    private static final Random random = new Random();

    public static byte[] genBytes() {
        return genBytes(MAX_BYTEA_LEN);
    }
    public static String genString() {
        return genString(MAX_STR_LEN);
    }

    public static int genInt(int bound) {
        return random.nextInt(bound);
    }
    public static int genInt(int origin, int bound) {
        return random.nextInt(origin, bound);
    }

    public static byte[] genBytes(int len) {
        var randlen = random.nextInt(len);
        var buf = new byte[randlen];
        random.nextBytes(buf);
        if(randlen>0){
            buf[0]=0;
        }
        return buf;
    }
    public static String genString(int len) {
        var randlen = random.nextInt(len);
        var buf = new byte[randlen];
        random.nextBytes(buf);
        if(randlen>0){
            buf[0]=0;
        }
        return new String(buf);
    }

    public static List<Integer> genList() {
        return genList(MAX_LST_LEN);
    }

    public static List<Integer> genList(int len) {
        var lst = new ArrayList<Integer>();
        var nbr = random.nextInt(len);
        for (var i = 0; i < nbr; i++) {
            lst.add(random.nextInt());
        }
        return lst;
    }

    public static DocumentUploadByLink createRandomUploadDocumentData() {
        try {
            var d = new DocumentUploadByLink();
            d.id = new DocumentId(UUID.randomUUID());
            d.location = new URI("http://localhost/~florian/link");

            d.title = DataGen.genString(25);
            d.shortDescription = DataGen.genString(128);
            d.longDescription = DataGen.genString(512);

            return d;
        } catch (URISyntaxException ex) {
            throw new RuntimeException();
        }
    }

    interface Generator<T> {
        T gen();
    }

    public static final Generator<?>[] generators = {
        () -> genString(),
        () -> genBytes(),
        () -> random.nextInt(),
        () -> random.nextBoolean(),
        () -> random.nextDouble(),
        () -> genList()
    };

    public static JsonNode createRandomJsonData() {
        return createRandomJsonData((int) (Math.random() * MAX_DEPTH));
    }

    private static JsonNode createRandomJsonData(int depth) {
        var data = new ObjectMapper().createObjectNode();

        var nbr = random.nextInt(MAX_FIELDNUMBER);
        for (var i = 0; i < nbr; i++) {
            var generator = generators[random.nextInt(generators.length)];
            if (depth > 0) {
                data.putPOJO("key_" + i + "_" + depth, createRandomJsonData(--depth));
            } else {
                data.putPOJO("key_" + i, generator.gen());
            }
        }

        return data;
    }

}
