/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.resource;

import com.fasterxml.jackson.databind.JsonNode;
import static org.hamcrest.CoreMatchers.*;
import static io.restassured.RestAssured.given;

import java.net.URI;

import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.delete;
import static com.github.tomakehurst.wiremock.client.WireMock.noContent;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Test;

import eu.gaiax.notarization.MockIdentityServiceResource;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.DataGen;
import static eu.gaiax.notarization.request_processing.Helper.*;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import java.net.MalformedURLException;
import java.net.URL;
import javax.inject.Inject;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import org.hibernate.reactive.mutiny.Mutiny;
import eu.gaiax.notarization.IdentityWireMock;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import eu.gaiax.notarization.request_processing.Helper;
import eu.gaiax.notarization.request_processing.domain.model.NotaryAccess;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskType;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.SessionTaskSummary;
import static eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState.mapper;
import java.util.Arrays;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import org.jboss.logging.Logger;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.lang.JoseException;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.http.Response;
import java.net.URISyntaxException;

/**
 *
 * @author Neil Crossley
 */
@QuarkusTest
@QuarkusTestResource(MockIdentityServiceResource.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
public class IdentityResourceTest {

    private static final String SESSION_VARIABLE = "sessionId";

    @IdentityWireMock
    WireMockServer wireMockServer;

    public static final Logger logger = Logger.getLogger(IdentityResourceTest.class);


    public static final String TASK_PATH = "/api/v1/session/{sessionId}/task";

    @Inject
    Mutiny.SessionFactory sessionFactory;


    @ConfigProperty(name = "notarization-processing.internal-url")
    URI internalUrl;

    @ConfigProperty(name = "quarkus.http.port")
    Integer assignedPort;

    public void configMock(){
        var sb = new StringBuffer();
        sb
            .append("{")
            .append("\"redirect\":\"http://localhost:")
            .append(wireMockServer.port())
            .append("/login/NONCE\"")
            .append(",")
            .append("\"cancel\":\"http://localhost:")
            .append(wireMockServer.port())
            .append("/session/NONCE\"")
            .append("}")
        ;

        var redirectFromIdentityService = sb.toString();

        wireMockServer.resetAll();
        wireMockServer.addMockServiceRequestListener(
                (in, out) -> requestReceived(in, out));
        wireMockServer.stubFor(
            post(urlMatching("/session.+success.+failure.+"))
                .willReturn(
                   aResponse()
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                        .withBody(redirectFromIdentityService)
                        .withStatus(200)
                )
        );

        var urlPatCancel= "/session.+";
        wireMockServer.stubFor(delete(urlMatching(urlPatCancel))
            .willReturn(
                noContent()
            ));

    }

    protected void requestReceived(Request inRequest, Response inResponse) {
        logger.debugv(" WireMock stub identity service request at URL: {0}", inRequest.getAbsoluteUrl());
        logger.debugv(" WireMock stub identity service request headers: \n{0}", inRequest.getHeaders());
        logger.debugv(" WireMock stub identity service request body: \n{0}", inRequest.getBodyAsString());
        logger.debugv(" WireMock stub identity service response body: \n{0}", inResponse.getBodyAsString());
        logger.debugv(" WireMock stub identity service response headers: \n{0}", inResponse.getHeaders());
    }

    @Test
    public void identificationCanBeCancelled() throws MalformedURLException, JoseException {

        configMock();

        var someSession = Helper.prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.someProfileId).session();

        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", someSession.accessToken())
            .when().get(someSession.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");

        var tasklist = Arrays.asList(mapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .filter(t -> t.type == TaskType.BROWSER_IDENTIFICATION_TASK)
            .findAny().orElseThrow();

        //startTask
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, someSession.id())
            .header("token", someSession.accessToken())
            .queryParam("taskId", task.taskId)
            .when()
            .post(TASK_PATH)
            .then()
            .statusCode(201)
            ;

        //cancel
        given()
            .pathParam(SESSION_VARIABLE, someSession.id())
            .header("token", someSession.accessToken())
            .param("taskId", task.taskId.toString())
            .when()
            .delete(TASK_PATH)
            .then()
            .statusCode(204)
            ;

        assertThat(
                wireMockServer.getAllServeEvents().stream()
                    .filter((evt)-> evt.getRequest().getMethod().isOneOf(RequestMethod.DELETE)).findAny().isPresent(),
                is(true)
        );
    }

    @Test
    public void identificationGetsCancelledIfNewOneIsStarted() throws MalformedURLException, JoseException {

        configMock();

        var someSession = Helper.prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.someProfileId).session();

        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", someSession.accessToken())
            .when().get(someSession.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");

        var tasklist = Arrays.asList(mapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .filter(t -> t.type == TaskType.BROWSER_IDENTIFICATION_TASK)
            .findAny().orElseThrow();

        //startTask
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, someSession.id())
            .header("token", someSession.accessToken())
            .queryParam("taskId", task.taskId)
            .when()
            .post(TASK_PATH)
            .then()
            .statusCode(201)
            ;

        //startTask again
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, someSession.id())
            .header("token", someSession.accessToken())
            .queryParam("taskId", task.taskId)
            .when()
            .post(TASK_PATH)
            .then()
            .statusCode(201)
            ;

        assertThat(
                wireMockServer.getAllServeEvents().stream()
                    .filter((evt)-> evt.getRequest().getMethod().isOneOf(RequestMethod.DELETE)).findAny().isPresent(),
                is(true)
        );
    }


    @Test
    public void identificationIsStoredAfterIdentification() throws MalformedURLException, JoseException, URISyntaxException {
        final JsonNode inputIdentity = DataGen.createRandomJsonData();
        var inputProfile = MockState.someProfile;

        configMock();

        var someSession = Helper.prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.someProfileId).session();

        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", someSession.accessToken())
            .when().get(someSession.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");
            ;

        var tasklist = Arrays.asList(mapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .filter(t -> t.type == TaskType.BROWSER_IDENTIFICATION_TASK)
            .findAny().orElseThrow();

        //startTask
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, someSession.id())
            .header("token", someSession.accessToken())
            .queryParam("taskId", task.taskId)
            .when()
            .post(TASK_PATH)
            .then()
            .statusCode(201)
            ;

        //hijack success url
        var successUri = wireMockServer.getAllServeEvents().stream()
            .map(e -> e.getRequest().getQueryParams())
            .map(e -> e.get("success"))
            .filter(v -> v.isPresent())
            .findFirst().get().values().get(0);
        //reencode nonce?

        given()
            .header("Content-Type", "application/json")
            .body(inputIdentity)
            .when()
            .post(successUri)
            .then()
            .statusCode(204);

        var res = withTransaction(sessionFactory, (session, tx) -> session.find(Session.class, someSession.id())
            .call(foundSession -> Mutiny.fetch(foundSession.identities)));

        assertThat("expected nbr of identities in session does not match", res.identities, hasSize(inputProfile.notaries().size()));

        var requestorIdentity = res.identities.iterator().next();
        var notaryAccess = inputProfile.notaries().iterator().next();
        assertThat(requestorIdentity.algorithm, equalTo(notaryAccess.algorithm()));
        assertThat(requestorIdentity.encryption, equalTo(inputProfile.encryption()));

        var ecnryptedReqData = requestorIdentity.data;
        var plain = decryptedIdentity(notaryAccess, ecnryptedReqData);
        assertThat(plain, is(inputIdentity.toString()));

    }

    private String decryptedIdentity(NotaryAccess notary, String payload) throws JoseException{
        JsonWebEncryption jwe = new JsonWebEncryption();
        var priv = notary.key().getPrivateKey();
        jwe.setKey(priv);

        jwe.setCompactSerialization(payload);
        var decrypted = jwe.getPayload();
        return decrypted;
    }
    public URL asCallbackUrl(String rawUrl) {

        try {
            return UriBuilder.fromUri(rawUrl).port(assignedPort).build().toURL();
        } catch (MalformedURLException ex) {
            throw new RuntimeException(ex);
        }
    }
}
