/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import java.util.Map;

import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;
import eu.gaiax.notarization.request_processing.domain.model.IssuanceResponse;
import static eu.gaiax.notarization.resource.IdentityResourceTest.logger;
import javax.ws.rs.core.MediaType;
import eu.gaiax.notarization.request_processing.domain.model.serialization.EnhanceSerializationCustomizer;
import java.net.URI;


/**
 *
 * @author Neil Crossley
 */
public class MockSsiIssuanceLifecycleManager implements QuarkusTestResourceLifecycleManager {

    WireMockServer wireMockServer;

    private static IssuanceResponse resp = new IssuanceResponse();

    private IssuanceResponse getResp(){
        var resp = new IssuanceResponse();
    //    resp.invitationURL = URI.create("http://a.url");
        resp.invitationURL = URI.create("");
        return resp;
    }

    @Override
    public Map<String, String> start() {
        try {
            wireMockServer = new WireMockServer(wireMockConfig().dynamicPort());
            wireMockServer.start();
            var objectMapper = new ObjectMapper();
            new EnhanceSerializationCustomizer().customize(objectMapper);
            objectMapper.registerModule(new Jdk8Module());
            objectMapper.registerModule(new JavaTimeModule());

            wireMockServer.stubFor(
                post(urlMatching("/credential/start-issuance/"))
                    .willReturn(
                        aResponse()
                            .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                            .withStatus(200)
                            .withBody(objectMapper.writeValueAsString(getResp())))
            );

            wireMockServer.addMockServiceRequestListener(
                (in, out) -> requestReceived(in, out));

            // create some stubs
            return Map.of("quarkus.rest-client.ssi-issuance-api.url", wireMockServer.baseUrl());
        } catch (JsonProcessingException ex) {
            return null;
        }
    }

    protected void requestReceived(Request inRequest, Response inResponse) {
        logger.debugv(" WireMock stub SSI issuance request at URL: {0}", inRequest.getAbsoluteUrl());
        logger.debugv(" WireMock stub SSI issuance request headers: \n{0}", inRequest.getHeaders());
        logger.debugv(" WireMock stub SSI issuance request body: \n{0}", inRequest.getBodyAsString());
        logger.debugv(" WireMock stub SSI issuance response body: \n{0}", inResponse.getBodyAsString());
        logger.debugv(" WireMock stub SSI issuance response headers: \n{0}", inResponse.getHeaders());
    }

    @Override
    public void stop() {
        if (wireMockServer != null) {
            wireMockServer.stop();
            wireMockServer = null;
        }
    }

    @Override
    public void inject(TestInjector testInjector) {
        testInjector.injectIntoFields(
                wireMockServer,
                new TestInjector.AnnotatedAndMatchesType(
                        SsiIssuanceWireMock.class, WireMockServer.class));
    }
}
