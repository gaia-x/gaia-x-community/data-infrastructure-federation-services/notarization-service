/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.feature.audit;

import eu.gaiax.notarization.MockServicesLifecycleManager;
import static eu.gaiax.notarization.request_processing.matcher.IsAnnotationPresent.isAnnotationPresent;
import static eu.gaiax.notarization.request_processing.matcher.IsAssignableTo.isAssignableTo;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.ext.Provider;

import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;
/**
 *
 * @author Neil Crossley
 */
@QuarkusTest
@QuarkusTestResource(MockServicesLifecycleManager.class)
public class DynamicAuditingFeatureTest {

    @Test
    public void sutIsDynamicFeature() {
        assertThat(DynamicAuditingFeature.class, isAssignableTo(DynamicFeature.class));
    }

    @Test
    public void sutIsProvider() {
        assertThat(DynamicAuditingFeature.class, isAnnotationPresent(Provider.class));
    }

}
