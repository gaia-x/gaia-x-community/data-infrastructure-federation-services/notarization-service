/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.submission;

import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import static eu.gaiax.notarization.request_processing.Helper.*;
import eu.gaiax.notarization.request_processing.Helper.SessionInfo;
import static eu.gaiax.notarization.request_processing.Helper.prepareSessionWithState;
import static eu.gaiax.notarization.request_processing.Helper.withTransaction;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditLogMarkUnready;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditTrailFor;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.hasAuditEntries;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@QuarkusTestResource(MockServicesLifecycleManager.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
public class MarkUnreadySubmissionTest {

    private static final String MARK_UNREADY_PATH = SUBMISSION_PATH + "/unready";

    @Inject
    Mutiny.SessionFactory sessionFactory;

    static Stream<NotarizationRequestState> allowedStates() {
        return Stream.of(
            NotarizationRequestState.READY_FOR_REVIEW
        );
    }

    static Stream<NotarizationRequestState> notAllowedStates() {
        var updateable = allowedStates().collect(Collectors.toSet());
        return Stream.of(NotarizationRequestState.values()).filter((s) -> !updateable.contains(s));
    }

    @ParameterizedTest
    @MethodSource("allowedStates")
    public void canMarkunready(NotarizationRequestState state) {

        var session = prepareSessionWithState(state, sessionFactory);

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken())
            .when()
            .post(MARK_UNREADY_PATH)
            .then()
            .statusCode(204);

        assertStateInStoredSession(session.id(), sessionFactory, NotarizationRequestState.EDITABLE);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.MARK_UNREADY, 1, sessionFactory),
            hasAuditEntries(auditLogMarkUnready())
        );

    }

    @ParameterizedTest
    @MethodSource("notAllowedStates")
    public void cannotMarkunready(NotarizationRequestState state) {
        var session = prepareSessionWithState(state, sessionFactory);

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken())
            .when()
            .post(MARK_UNREADY_PATH)
            .then()
            .statusCode(400);

        assertStateInStoredSession(session.id(), sessionFactory,state);
        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.MARK_UNREADY, 1, sessionFactory),
            hasAuditEntries(auditLogMarkUnready().httpStatus(400))
        );
    }

    @Test
    public void invalidTokenDetectedInMarkunreadyRequest() {

        var session = prepareSessionWithState(NotarizationRequestState.EDITABLE, sessionFactory);

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken() + "INVALID")
            .when()
            .post(MARK_UNREADY_PATH)
            .then()
            .statusCode(401);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.MARK_UNREADY, 1, sessionFactory),
            hasAuditEntries(auditLogMarkUnready().httpStatus(401))
        );

    }

    @Test
    public void noTokenDetectedInMarkunreadyRequest() {

        var session = prepareSessionWithState(NotarizationRequestState.EDITABLE, sessionFactory);

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .when()
            .post(MARK_UNREADY_PATH)
            .then()
            .statusCode(400);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.MARK_UNREADY, 1, sessionFactory),
            hasAuditEntries(auditLogMarkUnready().httpStatus(400))
        );

    }

    @Test
    public void emptyTokenDetectedInMarkunreadyRequest() {

        var session = prepareSessionWithState(NotarizationRequestState.EDITABLE, sessionFactory);

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", "")
            .when()
            .post(MARK_UNREADY_PATH)
            .then()
            .statusCode(401);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.MARK_UNREADY, 1, sessionFactory),
            hasAuditEntries(auditLogMarkUnready().httpStatus(401))
        );

    }

    public void assertSessionState(SessionInfo sess, NotarizationRequestState state) {
        var res = withTransaction(sessionFactory, (session, tx) -> session.find(Session.class, sess.id()));

        assertThat(res.state, is(state));
    }

}
