/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.management;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.MockServicesLifecycleManager;
import static eu.gaiax.notarization.request_processing.Helper.withTransaction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.domain.model.RequestFilter;
import static eu.gaiax.notarization.request_processing.Helper.withTransactionAsync;
import eu.gaiax.notarization.request_processing.domain.entity.Document;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.DocumentFull;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.DocumentView;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState.Notary;
import io.quarkus.test.common.QuarkusTestResource;
import javax.inject.Inject;
import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import io.restassured.response.ValidatableResponse;
import io.smallrye.mutiny.Uni;
import java.util.Arrays;
import java.util.Base64;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;
import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@Tag("security")
@QuarkusTestResource(MockServicesLifecycleManager.class)
public class FetchAvailableRequestsTest {

    public static final String REQUESTS_PATH = "/api/v1/requests";
    public static final String REQUEST_PATH = "/api/v1/profiles/{profileId}/requests/{notarizationRequestId}";
    public static final String DOC_REQUEST_PATH = "/api/v1/profiles/{profileId}/requests/{notarizationRequestId}/document/{documentId}";

    ObjectMapper objectMapper = new ObjectMapper();
    @Inject
    Mutiny.SessionFactory sessionFactory;

    public static final Random random = new Random();

    static Stream<NotarizationRequestState> allowedStates() {
        return Stream.of(
            NotarizationRequestState.READY_FOR_REVIEW,
            NotarizationRequestState.WORK_IN_PROGRESS,
            NotarizationRequestState.ACCEPTED,
            NotarizationRequestState.PENDING_DID
        );
    }

    static Stream<NotarizationRequestState> notAllowedStates() {
        var updateable = allowedStates().collect(Collectors.toSet());
        return Stream.of(NotarizationRequestState.values()).filter((s) -> !updateable.contains(s));
    }

    @Test
    public void invalidLimitPaginateFetchAvailableRequests(){

        var filterVal = RequestFilter.available;
        var limit = 0;

        pruneDB();
        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .queryParam("offset", 0)
            .queryParam("limit", limit)
            .queryParam("filter", filterVal)
            .when()
            .get(REQUESTS_PATH)
            .then()
            .statusCode(400);
    }

    public static class TotalRec_Limit {
        public final int totalRequests;
        public final int limitPerPage;
        public TotalRec_Limit(int totalRequests, int limitPerPage){
            this.totalRequests = totalRequests;
            this.limitPerPage = limitPerPage;
        }
    };
    public static Stream<TotalRec_Limit> testLimitValues(){
        return Stream.of(
            new TotalRec_Limit(101, 1),
            new TotalRec_Limit(101, 100),
            new TotalRec_Limit(101, 101),
            new TotalRec_Limit(101, 102),
            new TotalRec_Limit(101, 500)
        );
    }

    private NotarizationRequest createClaimedRequest(ProfileId profile, String notary) {

        var nr = new NotarizationRequest();
        var id = UUID.randomUUID();
        withTransactionAsync(sessionFactory, (session, tx) -> {

            var sess =new Session();
            sess.id = UUID.randomUUID().toString();
            sess.state = NotarizationRequestState.WORK_IN_PROGRESS;
            sess.profileId = profile.id();

            nr.id = id;
            nr.session = sess;
            nr.claimedBy = notary;

            return session.persist(sess).chain(() -> session.persist(nr));
        }).await().indefinitely();

        return nr;

    }

    private void pruneDB(){
        withTransactionAsync(sessionFactory, (session, tx) -> {
            return session.createQuery("delete Document").executeUpdate().chain(()->{
                return session.createQuery("delete NotarizationRequest").executeUpdate();
            });
        }).await().indefinitely();
    }

    private NotarizationRequest createRequestWithStateInDB(NotarizationRequestState state, ProfileId profile) {

        var nr = new NotarizationRequest();
        var id = UUID.randomUUID();
        withTransactionAsync(sessionFactory, (session, tx) -> {

            var sess =new Session();
            sess.id = UUID.randomUUID().toString();
            sess.state = state;
            sess.profileId = profile.id();

            nr.id = id;
            nr.session = sess;

            return session.persist(sess).chain(() -> session.persist(nr));
        }).await().indefinitely();

        return nr;

    }

    public static record ExpectedResultsByFilter(
        String[] available,
        String[] claimed,
        String[] ownClaimed
    ){}

    private Notary getRandomNotary(){
        return getRandomNotary(null);
    }
    private Notary getRandomNotary(Notary except){
        if(except != null){
            return MockState.notariesWithSomeProfile.stream()
                .filter(n -> !n.name().equals(except.name()))
                .skip(random.nextInt(0, MockState.notariesWithSomeProfile.size()-1))
                .findFirst().orElseThrow();
        }
        else {
            return MockState.notariesWithSomeProfile.stream()
                .skip(random.nextInt(0, MockState.notariesWithSomeProfile.size()))
                .findFirst().orElseThrow();
        }
    }

    public static class Totals {
        int totalClaimedBy, totalClaimedByOther, totalAvailable;
        public Totals(int totalClaimedBy, int totalClaimedByOther, int totalAvailable){
            this.totalClaimedBy = totalClaimedBy;
            this.totalClaimedByOther = totalClaimedByOther;
            this.totalAvailable = totalAvailable;
        }
    }

    private ExpectedResultsByFilter prepareFilterTest(Notary claimingNotary, Totals totals){

        pruneDB();

        var profile = MockState.someProfileId;

        var claimedByNotary = Stream.generate(()->createClaimedRequest(profile, claimingNotary.name()))
            .limit(totals.totalClaimedBy)
            .map(r -> r.id.toString())
            .collect(Collectors.toList())
            ;

        var allClaimed = Stream.generate(()->createClaimedRequest(profile, getRandomNotary(claimingNotary).name()))
            .limit(totals.totalClaimedByOther)
            .map(r -> r.id.toString())
            .collect(Collectors.toList())
            ;
        allClaimed.addAll(claimedByNotary);


        var available = Stream.generate(()->createRequestWithStateInDB(NotarizationRequestState.READY_FOR_REVIEW, profile))
            .limit(totals.totalAvailable)
            .map(r -> r.id.toString())
            .collect(Collectors.toList())
            ;

        return new ExpectedResultsByFilter(
            available.toArray(String[]::new),
            allClaimed.toArray(String[]::new),
            claimedByNotary.toArray(String[]::new)
        );

    }

    private ValidatableResponse givenFiltered(RequestFilter filterVal, Notary claimingNotary){

        return given()
            .header("Authorization", claimingNotary.bearerValue())
            .queryParam("offset", 0)
            .queryParam("limit", Integer.MAX_VALUE)
            .queryParam("filter" , filterVal)
            .when()
            .get(REQUESTS_PATH)
            .then()
            .statusCode(200)
            ;

    }

    private static Stream<Totals> totals(){
        return Stream.of(
            new Totals(0,0,0)
            ,new Totals(0,0,1)
            ,new Totals(0,1,0)
            ,new Totals(0,1,1)
            ,new Totals(1 ,0,0)
            ,new Totals(1 ,0,1)
            ,new Totals(1 ,1,0)
            ,new Totals(1 ,1,1)
            ,new Totals(1,1,10)
            ,new Totals(0,1,10)
            ,new Totals(11,13,0)
            ,new Totals(11,13,23)
            ,new Totals(random.nextInt(0,10),random.nextInt(0,10),random.nextInt(0,10))
            ,new Totals(random.nextInt(0,10),random.nextInt(0,10),random.nextInt(0,10))
            ,new Totals(random.nextInt(0,10),random.nextInt(0,10),random.nextInt(0,10))
            ,new Totals(random.nextInt(0,10),random.nextInt(0,10),random.nextInt(0,10))
        );
    }


    @ParameterizedTest
    @MethodSource("totals")
    public void testFilterAllClaimed(Totals totals){

        var claimingNotary = getRandomNotary();
        var expectedNotarizationRequests = prepareFilterTest(claimingNotary, totals);

        givenFiltered(RequestFilter.allClaimed, claimingNotary)
            .body("notarizationRequests.id" , containsInAnyOrder(expectedNotarizationRequests.claimed))
            ;

    }

    @ParameterizedTest
    @MethodSource("totals")
    public void testFilterOwnClaimed(Totals totals){

        var claimingNotary = getRandomNotary();
        var expectedNotarizationRequests = prepareFilterTest(claimingNotary, totals);

        givenFiltered(RequestFilter.ownClaimed, claimingNotary)
            .body("notarizationRequests.id" , containsInAnyOrder(expectedNotarizationRequests.ownClaimed))
            ;

    }

    @ParameterizedTest
    @MethodSource("totals")
    public void testFilterAvailable(Totals totals){

        var claimingNotary = getRandomNotary();
        var expectedNotarizationRequests = prepareFilterTest(claimingNotary, totals);

        givenFiltered(RequestFilter.available, claimingNotary)
            .body("notarizationRequests.id" , containsInAnyOrder(expectedNotarizationRequests.available))
            ;

    }

    @ParameterizedTest
    @MethodSource("testLimitValues")
    public void canPaginateFetchAvailableRequests(TotalRec_Limit testValues){

        var profile = MockState.someProfileId;

        var nbrRequests = testValues.totalRequests;
        var limit = testValues.limitPerPage;

        var expectedPageCount = (int)Math.ceil((double)nbrRequests/limit);

        //last page
        var offset = expectedPageCount-1;
        var expectedRequestsOnPage = nbrRequests - offset*limit;

        pruneDB();
        for(var i=0;i<nbrRequests; i++){
            createRequestWithStateInDB(NotarizationRequestState.READY_FOR_REVIEW, profile);
        }

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .queryParam("offset", offset)
            .queryParam("limit", limit)
            .when()
            .get(REQUESTS_PATH)
            .then()
            .statusCode(200)
            .body("requestCount", is(nbrRequests))
            .body("pageCount", is(expectedPageCount))
            .body("notarizationRequests.size()", is(expectedRequestsOnPage))
            ;
    }


    @ParameterizedTest
    @MethodSource("allowedStates")
    public void canFetchNotarizationRequestSummaryById(NotarizationRequestState state){
        var profile = MockState.someProfileId;
        var notRequestId = createRequestWithStateInDB(state, profile).id.toString();

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", notRequestId)
            .pathParam("profileId", profile.id())
            .when()
            .get(REQUEST_PATH)
            .then()
            .body("id", is(notRequestId))
            .statusCode(200);
    }

    @ParameterizedTest
    @MethodSource("notAllowedStates")
    public void canNotFetchNotarizationRequestSummaryById(NotarizationRequestState state){
        var profile = MockState.someProfileId;
        var notRequestId = createRequestWithStateInDB(state, profile).id;

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", notRequestId)
            .pathParam("profileId", profile.id())
            .when()
            .get(REQUEST_PATH)
            .then()
            .statusCode(400);
    }

    @Test
    public void canNotFetchNotarizationRequestSummaryByUnknownId(){
        var profile = MockState.someProfileId;
        createRequestWithStateInDB(NotarizationRequestState.WORK_IN_PROGRESS, profile);

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", UUID.randomUUID().toString())
            .pathParam("profileId", profile.id())
            .when()
            .get(REQUEST_PATH)
            .then()
            .statusCode(404);
    }

    @ParameterizedTest
    @MethodSource("allowedStates")
    public void canFetchNotarizationRequestDocumentById(NotarizationRequestState state){
//    public void canFetchNotarizationRequestDocumentById(){
        var profile = MockState.someProfileId;
        var docNRequest = createRequestWithStateAndDocumentInDB(NotarizationRequestState.READY_FOR_REVIEW, profile);
        var notRequestId = docNRequest.notReq.id.toString();

        var docSums = given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", notRequestId)
            .pathParam("profileId", profile.id())
            .when()
            .get(REQUEST_PATH)
            .then()
            .body("id", is(notRequestId))
            .statusCode(200)
            .extract().body().path("documents")
            ;

        var docViews= Arrays.asList(objectMapper.convertValue(docSums, DocumentView[].class ));
        var docId = docViews.stream()
            .map((v)-> v.id.id)
            .findAny().orElseThrow();

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", notRequestId)
            .pathParam("profileId", profile.id())
            .pathParam("documentId", docId)
            .when()
            .get(DOC_REQUEST_PATH)
            .then()
            .statusCode(200)
            .body("content", is(Base64.getEncoder().encodeToString(docNRequest.doc.content)))
            .body("verificationReport", is(docNRequest.doc.verificationReport.value));
    }

    private Document aDocument(Session sess) {

        var doc = new Document();
            doc.id = UUID.randomUUID();
            doc.content = "conntent".getBytes();

            doc.title = "title";
            doc.shortDescription = "sdesc";
            doc.longDescription = "ldesc";
            doc.mimetype = "mime";
            doc.extension = "ext";
            doc.verificationReport = "<report>evaluation</report>";
            doc.hash = "hash";

            doc.session = sess;

        return doc;

    }

    public static record DocNRequest(DocumentFull doc,NotarizationRequest notReq){}

    private DocNRequest createRequestWithStateAndDocumentInDB(NotarizationRequestState state, ProfileId profile) {

        return withTransaction(sessionFactory, (session, tx) -> {

            var sess =new Session();
            var nr = new NotarizationRequest();
            var doc = aDocument(sess);
            var id = UUID.randomUUID();

            sess.id = UUID.randomUUID().toString();
            sess.state = state;
            sess.profileId = profile.id();
            sess.documents = Set.of(doc);

            nr.id = id;
            nr.session = sess;

            doc.session = sess;

            return
                Uni.createFrom().voidItem()
                .chain(() -> session.persist(sess))
                .chain(() -> session.persist(doc))
                .chain(() -> session.persist(nr))
                .map(d -> {
                    return new DocNRequest(DocumentFull.fromDocument(doc), nr);
                });
        });

    }


}
