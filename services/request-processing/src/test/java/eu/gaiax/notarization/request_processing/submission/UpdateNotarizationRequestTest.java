/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.submission;

import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.time.Duration;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import static eu.gaiax.notarization.request_processing.DataGen.createRandomJsonData;
import static eu.gaiax.notarization.request_processing.Helper.*;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@QuarkusTestResource(MockServicesLifecycleManager.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
public class UpdateNotarizationRequestTest {

    @Inject
    Mutiny.SessionFactory sessionFactory;

    static Stream<NotarizationRequestState> updateableStates() {
        return Stream.of(
            NotarizationRequestState.EDITABLE
        );
    }

    static Stream<NotarizationRequestState> nonUpdateableStates() {
        var updateable = updateableStates().collect(Collectors.toSet());
        return Stream.of(NotarizationRequestState.values()).filter((s) -> !updateable.contains(s));
    }

    @ParameterizedTest
    @MethodSource("updateableStates")
    public void canUpdateRequest(NotarizationRequestState state) {

        var sessionWithSubmittedNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);
        setSessionState(sessionWithSubmittedNotarizationRequest.session(), state, sessionFactory);

        var dataToUpdate = createRandomJsonData();

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithSubmittedNotarizationRequest.session().id())
            .header("token", sessionWithSubmittedNotarizationRequest.session().accessToken())
            .body(dataToUpdate)
            .when()
            .put(SUBMISSION_PATH)
            .then()
            .statusCode(204)
            .extract();

        assertDataOfNotRequestInDB(sessionWithSubmittedNotarizationRequest.session(), dataToUpdate);

        assertThat(auditTrailFor(sessionWithSubmittedNotarizationRequest.session().id(), NotarizationRequestAction.UPDATE, sessionFactory),
            hasAuditEntries(auditLogUpdate())
        );
    }

    @ParameterizedTest
    @MethodSource("nonUpdateableStates")
    public void cannotUpdateRequest(NotarizationRequestState state) {

        var sessionWithSubmittedNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);
        setSessionState(sessionWithSubmittedNotarizationRequest.session(), state, sessionFactory);

        var dataToUpdate = createRandomJsonData();
        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithSubmittedNotarizationRequest.session().id())
            .header("token", sessionWithSubmittedNotarizationRequest.session().accessToken())
            .body(dataToUpdate)
            .when()
            .put(SUBMISSION_PATH)
            .then()
            .statusCode(400)
            .extract();

        //data not changed assertion proofs data generated in prepareSession
        assertDataOfNotRequestInDB(sessionWithSubmittedNotarizationRequest.session(), sessionWithSubmittedNotarizationRequest.notarizationRequest().data);

        assertThat(auditTrailFor(sessionWithSubmittedNotarizationRequest.session().id(), NotarizationRequestAction.UPDATE, sessionFactory),
            hasAuditEntries(auditLogUpdate().httpStatus(400))
        );
    }

    @Test
    public void invalidTokenDetectedInUpdateRequest() {

        var sessionWithSubmittedNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithSubmittedNotarizationRequest.session().id())
            .header("token", sessionWithSubmittedNotarizationRequest.session().accessToken() + "INVALID")
            .body(createRandomJsonData())
            .when()
            .put(SUBMISSION_PATH)
            .then()
            .statusCode(401)
            .extract();

        //data not changed assertion proofs data generated in prepareSession
        assertDataOfNotRequestInDB(sessionWithSubmittedNotarizationRequest.session(), sessionWithSubmittedNotarizationRequest.notarizationRequest().data);

        assertThat(auditTrailFor(sessionWithSubmittedNotarizationRequest.session().id(), NotarizationRequestAction.UPDATE, sessionFactory),
            hasAuditEntries(auditLogUpdate().httpStatus(401))
        );
    }

    @Test
    public void noTokenDetectedInUpdateRequest() {

        var sessionWithSubmittedNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithSubmittedNotarizationRequest.session().id())
            .body(createRandomJsonData())
            .when()
            .put(SUBMISSION_PATH)
            .then()
            .statusCode(400)
            .extract();

        //data not changed assertion proofs data generated in prepareSession
        assertDataOfNotRequestInDB(sessionWithSubmittedNotarizationRequest.session(), sessionWithSubmittedNotarizationRequest.notarizationRequest().data);

        assertThat(auditTrailFor(sessionWithSubmittedNotarizationRequest.session().id(), NotarizationRequestAction.UPDATE, sessionFactory),
            hasAuditEntries(auditLogUpdate().httpStatus(400))
        );
    }

    @Test
    public void emptyTokenDetectedInUpdateRequest() {

        var sessionWithSubmittedNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithSubmittedNotarizationRequest.session().id())
            .header("token", "")
            .body(createRandomJsonData())
            .when().put(SUBMISSION_PATH)
            .then()
            .statusCode(401)
            .extract();

        //data not changed assertion proofs data generated in prepareSession
        assertDataOfNotRequestInDB(sessionWithSubmittedNotarizationRequest.session(), sessionWithSubmittedNotarizationRequest.notarizationRequest().data);

        assertThat(auditTrailFor(sessionWithSubmittedNotarizationRequest.session().id(), NotarizationRequestAction.UPDATE, sessionFactory),
            hasAuditEntries(auditLogUpdate().httpStatus(401))
        );
    }

    @Test
    public void givenInvalidJsonLdThenCannotUpdateRequest() {
        var givenSession = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);
        var inputInvalidJsonld = asJson("""
                       {
                         "@context": "https://schema.org/ContextDoesNotExistHere",
                         "@type": "Person",
                         "name": "Jane Doe",
                         "jobTitle": "Professor",
                         "telephone": "(425) 123-4567",
                         "url": "http://www.janedoe.com"
                       }
                       """);

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, givenSession.session().id())
            .header("token", givenSession.session().accessToken())
            .body(inputInvalidJsonld)
            .when()
            .put(SUBMISSION_PATH)
            .then()
            .statusCode(400);

        assertThat(auditTrailFor(givenSession.session().id(), NotarizationRequestAction.UPDATE, sessionFactory),
                hasAuditEntries(
                        auditLogCreateSession(),
                        auditLogUpdate().httpStatus(400)
                ));
    }

    @Test
    public void givenValidJsonLdThenCanUpdateRequest() {
        var givenSession = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);
        var inputValidJsonld = asJson("""
                       {
                         "@context": "https://schema.org/",
                         "@type": "Person",
                         "name": "Jane Doe",
                         "jobTitle": "Professor",
                         "telephone": "(425) 123-4567",
                         "url": "http://www.janedoe.com"
                       }
                       """);

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, givenSession.session().id())
            .header("token", givenSession.session().accessToken())
            .body(inputValidJsonld)
            .when()
            .put(SUBMISSION_PATH)
            .then()
            .statusCode(204);

        assertThat(auditTrailFor(givenSession.session().id(), NotarizationRequestAction.UPDATE, sessionFactory),
                hasAuditEntries(
                        auditLogCreateSession(),
                        auditLogSubmit(),
                        auditLogUpdate()
                ));
    }

    private void assertDataOfNotRequestInDB(SessionInfo sess, JsonNode expected) {

        withTransactionAsync(sessionFactory, (session, tx) -> {
            return session.find(Session.class, sess.id());
        }).map((session) -> session.request.data)
                .invoke(data -> assertThat(data, is(expected.toString())))
                .await().atMost(Duration.ofSeconds(1));
    }

    private static JsonNode asJson(String value) {
        try {
            return new ObjectMapper().readTree(value);
        } catch (JsonProcessingException ex) {
            throw new RuntimeException(ex);
        }
    }
}
