/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing;

import static io.restassured.RestAssured.given;

import java.time.Duration;
import java.util.List;
import java.util.function.BiFunction;

import com.fasterxml.jackson.databind.JsonNode;

import org.hibernate.reactive.mutiny.Mutiny;
import org.hibernate.reactive.mutiny.Mutiny.SessionFactory;

import eu.gaiax.notarization.request_processing.domain.entity.HttpNotarizationRequestAudit;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.DistributedIdentity;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.SubmitNotarizationRequest;
import eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher;
import static eu.gaiax.notarization.request_processing.DataGen.createRandomJsonData;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import io.restassured.http.ContentType;
import io.smallrye.mutiny.Uni;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 * @author Florian Otto
 */
public class Helper {

    public static final String SESSION_VARIABLE = "sessionId";
    public static final String SUBMISSION_PATH = "/api/v1/session/{sessionId}/submission";
    public static final String SESSION_PATH = "/api/v1/session/{sessionId}";

    public List<HttpNotarizationRequestAudit> auditTrailFor(String sessionId,
        NotarizationRequestAction action,
        int count,
        Mutiny.SessionFactory sessionFactory) {
        return AuditTrailMatcher.auditTrailFor(sessionId, action, count, sessionFactory);
    }

    public static SessionInfo prepareSession() {
        return prepareSession(MockState.someProfileId);
    }
    public static SessionInfo prepareSession(ProfileId profile) {
        var response = given()
                .accept(ContentType.JSON)
                .contentType(ContentType.JSON)
                .body(String.format("""
                                    {"profileId":"%s"}
                                    """, profile.id()))
                .when().post("/api/v1/session")
                .then()
                .extract();

        return new SessionInfo(response.path("sessionId"),
            response.header("Location"),
            response.path("token"),
            profile.id());
    }

    public static SessionWithSubmittedNotarizationRequest prepareSessionWithSubmittedNotarizationRequest(SessionFactory factory, ProfileId profileId) {
        var notarizationRequest = createSubmitNotarizationRequest();
        return prepareSessionWithSubmittedNotarizationRequest(notarizationRequest, factory, profileId);
    }

    public static SessionWithSubmittedNotarizationRequest prepareSessionWithSubmittedNotarizationRequest(SessionFactory factory) {
        var notarizationRequest = createSubmitNotarizationRequest();
        return prepareSessionWithSubmittedNotarizationRequest(notarizationRequest, factory);
    }

    public static SessionWithSubmittedNotarizationRequest prepareSessionWithSubmittedNotarizationRequest(
            NotarizationRequestState state, SessionFactory factory) {
        var notarizationRequest = createSubmitNotarizationRequest();
        return prepareSessionWithSubmittedNotarizationRequest(notarizationRequest, state, factory);
    }

    public static SessionWithSubmittedNotarizationRequest prepareSessionWithSubmittedNotarizationRequest(
            SubmitNotarizationRequest notarizationRequest,
            SessionFactory factory) {
        return prepareSessionWithSubmittedNotarizationRequest(notarizationRequest, NotarizationRequestState.EDITABLE, factory);
    }
    public static SessionWithSubmittedNotarizationRequest prepareSessionWithSubmittedNotarizationRequest(
            SubmitNotarizationRequest notarizationRequest,
            SessionFactory factory,
            ProfileId profileId) {
        return prepareSessionWithSubmittedNotarizationRequest(notarizationRequest, NotarizationRequestState.EDITABLE, factory, profileId);
    }

    public static SessionWithSubmittedNotarizationRequest prepareSessionWithSubmittedNotarizationRequest(
            SubmitNotarizationRequest notarizationRequest,
            NotarizationRequestState state,
            SessionFactory factory,
            ProfileId profileId
    ) {
        var sessionInfo = prepareSessionWithState(NotarizationRequestState.SUBMITTABLE, factory, profileId);

        submitNotarizationRequest(notarizationRequest, sessionInfo);

        if (state != NotarizationRequestState.EDITABLE) {
            setSessionState(sessionInfo, state, factory);
        }

        return new SessionWithSubmittedNotarizationRequest(sessionInfo, notarizationRequest);
    }
    private static SessionWithSubmittedNotarizationRequest prepareSessionWithSubmittedNotarizationRequest(
            SubmitNotarizationRequest notarizationRequest,
            NotarizationRequestState state,
            SessionFactory factory
    ) {
        var sessionInfo = prepareSessionWithState(NotarizationRequestState.SUBMITTABLE, factory);

        submitNotarizationRequest(notarizationRequest, sessionInfo);

        if (state != NotarizationRequestState.EDITABLE) {
            setSessionState(sessionInfo, state, factory);
        }

        return new SessionWithSubmittedNotarizationRequest(sessionInfo, notarizationRequest);
    }
    public static SessionInfo prepareSessionWithState(NotarizationRequestState state, SessionFactory factory, ProfileId profileId) {

        var sessionInfo = prepareSession(profileId);
        setSessionState(sessionInfo, state, factory);
        return sessionInfo;
    }
    public static SessionInfo prepareSessionWithState(NotarizationRequestState state, SessionFactory factory) {

        var sessionInfo = prepareSession();
        setSessionState(sessionInfo, state, factory);
        return sessionInfo;
    }

    public static void setSessionState(SessionInfo sessionInfo,
            NotarizationRequestState state,
            SessionFactory factory) {

        withTransaction(factory, (dbSession, transaction) -> {
            return dbSession.find(Session.class, sessionInfo.id)
            .chain(foundSession -> {
                foundSession.state = state;
                return dbSession.persist(foundSession);
            });
        });
    }

    public static <T> T withTransaction(SessionFactory factory, BiFunction<Mutiny.Session, Mutiny.Transaction, Uni<T>> work) {
        return factory.openSession()
                .chain(dbSession -> dbSession.withTransaction(transaction -> work.apply(dbSession, transaction)).eventually(dbSession::close))
                .await().atMost(Duration.ofSeconds(1));
    }

    public static <T> Uni<T> withTransactionAsync(SessionFactory factory, BiFunction<Mutiny.Session, Mutiny.Transaction, Uni<T>> work) {
        return factory.openSession()
                .chain(dbSession -> dbSession.withTransaction(transaction -> work.apply(dbSession, transaction)).eventually(dbSession::close));
    }

    public static SubmitNotarizationRequest createSubmitNotarizationRequest() {
        return createSubmitNotarizationRequest(createRandomJsonData());
    }

    public static void assertStateInStoredSession(String sessId, SessionFactory factory, NotarizationRequestState expected){
        var dbSess = withTransaction(factory, (session, transaction) -> {
            return session.find(Session.class, sessId);
        });
        assertThat(dbSess.state , is(expected));
    }

    public static SubmitNotarizationRequest createSubmitNotarizationRequest(JsonNode data) {
        var submission = new SubmitNotarizationRequest();
        submission.data = data;
        submission.holder = new DistributedIdentity("did:action:value");
        submission.invitation = "did:invitation:some-thing";
        return submission;
    }

    public static record SessionInfo(
        String id, String location, String accessToken, String profileId) {
    }

    public static record SessionWithSubmittedNotarizationRequest(
        SessionInfo session,
        SubmitNotarizationRequest notarizationRequest) {
    }

    private static void submitNotarizationRequest(SubmitNotarizationRequest inputRequest, SessionInfo session) {
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken())
            .body(inputRequest)
            .when()
            .post(SUBMISSION_PATH)
            .then().statusCode(201);
    }
}
