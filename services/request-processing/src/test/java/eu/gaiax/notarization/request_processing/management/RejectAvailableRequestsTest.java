/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.management;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import static eu.gaiax.notarization.request_processing.Helper.withTransactionAsync;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditLogReject;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditTrailForNotarizationRequestID;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.hasAuditEntries;
import io.quarkus.test.common.QuarkusTestResource;
import javax.inject.Inject;
import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@Tag("security")
@QuarkusTestResource(MockServicesLifecycleManager.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
public class RejectAvailableRequestsTest {

    public static final String REQUESTS_PATH = "/api/v1/requests";
    public static final String REQUEST_PATH = "/api/v1/profiles/{profileId}/requests/{notarizationRequestId}/reject";

    ObjectMapper objectMapper = new ObjectMapper();
    @Inject
    Mutiny.SessionFactory sessionFactory;

    static Stream<NotarizationRequestState> allowedStates() {
        return Stream.of(
            NotarizationRequestState.WORK_IN_PROGRESS
        );
    }

    static Stream<NotarizationRequestState> notAllowedStates() {
        var updateable = allowedStates().collect(Collectors.toSet());
        return Stream.of(NotarizationRequestState.values()).filter((s) -> !updateable.contains(s));
    }

    private String createRequestWithStateInDB(ProfileId profile, NotarizationRequestState state) {

        var id = UUID.randomUUID();
        withTransactionAsync(sessionFactory, (session, tx) -> {

            var sess =new Session();
            sess.id = UUID.randomUUID().toString();
            sess.state = state;
            sess.profileId = profile.id();

            var nr = new NotarizationRequest();
            nr.id = id;
            nr.session = sess;

            return session.persist(sess).chain(() -> session.persist(nr));
        }).await().indefinitely();

        return id.toString();

    }

    private void assertStateAndCommentInDB(String id, NotarizationRequestState state, String rejectComment){
        withTransactionAsync(sessionFactory, (dbSession, tx) -> {
            return dbSession.find(NotarizationRequest.class, UUID.fromString(id)).invoke((notReq -> {
                assertThat(notReq.session.state, is(state));
                assertThat(notReq.rejectComment, is(rejectComment));
            }));
        }).await().indefinitely();
    }

    @Test
    public void canRejectNotarizationRequestWithoutComment(){
        var state  = NotarizationRequestState.WORK_IN_PROGRESS;
        var profile = MockState.someProfileId;
        var notRequestId = createRequestWithStateInDB(profile, state);

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .contentType(ContentType.JSON)
            .pathParam("profileId", profile.id())
            .pathParam("notarizationRequestId", notRequestId)
            .when()
            .post(REQUEST_PATH)
            .then()
            .statusCode(204);

        assertStateAndCommentInDB(notRequestId, NotarizationRequestState.EDITABLE, null);

        assertThat(auditTrailForNotarizationRequestID(notRequestId, NotarizationRequestAction.REJECT, 1, sessionFactory),
            hasAuditEntries(auditLogReject())
        );
    }

    @ParameterizedTest
    @MethodSource("allowedStates")
    public void canRejectNotarizationRequest(NotarizationRequestState state){
        var profile = MockState.someProfileId;
        var notRequestId = createRequestWithStateInDB(profile, state);
        var rejectComment = "rejectComment";

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .contentType(ContentType.JSON)
            .pathParam("profileId", profile.id())
            .pathParam("notarizationRequestId", notRequestId)
            .body(prepareRejectComment(rejectComment))
            .when()
            .post(REQUEST_PATH)
            .then()
            .statusCode(204);

        assertStateAndCommentInDB(notRequestId, NotarizationRequestState.EDITABLE, rejectComment);

        assertThat(auditTrailForNotarizationRequestID(notRequestId, NotarizationRequestAction.REJECT, 1, sessionFactory),
            hasAuditEntries(auditLogReject())
        );

    }

    @ParameterizedTest
    @MethodSource("notAllowedStates")
    public void canNotRejectNotarizationRequest(NotarizationRequestState state){
        var profile = MockState.someProfileId;
        var notRequestId = createRequestWithStateInDB(profile, state);

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .contentType(ContentType.JSON)
            .pathParam("profileId", profile.id())
            .pathParam("notarizationRequestId", notRequestId)
            .when()
            .post(REQUEST_PATH)
            .then()
            .statusCode(400);

        assertStateAndCommentInDB(notRequestId, state, null);

        assertThat(auditTrailForNotarizationRequestID(notRequestId, NotarizationRequestAction.REJECT, 1, sessionFactory),
            hasAuditEntries(auditLogReject().httpStatus(400))
        );
    }

    @Test
    public void canNotRejectNotarizationRequest(){
        var profile = MockState.someProfileId;
        createRequestWithStateInDB(profile, NotarizationRequestState.WORK_IN_PROGRESS);

        var unkownId = UUID.randomUUID().toString();
        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .contentType(ContentType.JSON)
            .pathParam("profileId", profile.id())
            .pathParam("notarizationRequestId", unkownId)
            .when()
            .post(REQUEST_PATH)
            .then()
            .statusCode(404);

        assertThat(auditTrailForNotarizationRequestID(unkownId, NotarizationRequestAction.REJECT, 1, sessionFactory),
            hasAuditEntries(auditLogReject().httpStatus(404))
        );
    }

    private static String prepareRejectComment(String comment) {
        return String.format("""
                             { "reason": "%s" }
                             """, comment);
    }
}
