/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.model;

import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import java.util.HashSet;
import java.util.Set;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Neil Crossley
 */
public class NotarizationRequestActionTest {

    private Set<NotarizationRequestAction> allActions = Set.of(NotarizationRequestAction.values());

    @Test
    public void allActionsBelongToAnActor() {
        var sumOfAllActors = new HashSet<NotarizationRequestAction>();
        sumOfAllActors.addAll(NotarizationRequestAction.RequestorActions);
        sumOfAllActors.addAll(NotarizationRequestAction.NotaryActions);
        sumOfAllActors.addAll(NotarizationRequestAction.SystemActions);
        sumOfAllActors.addAll(NotarizationRequestAction.CallbackActions);

        assertThat(sumOfAllActors, equalTo(allActions));
    }

    @Test
    public void requestorActionsAndNotaryActionsAreDisjunct() {
        var intersection = new HashSet<>(NotarizationRequestAction.RequestorActions);
        intersection.retainAll(NotarizationRequestAction.NotaryActions);

        assertThat(intersection, equalTo(Set.of()));
    }

    @Test
    public void requestorActionsAndSystemActionsAreDisjunct() {
        var intersection = new HashSet<>(NotarizationRequestAction.RequestorActions);
        intersection.retainAll(NotarizationRequestAction.SystemActions);

        assertThat(intersection, equalTo(Set.of()));
    }

    @Test
    public void notaryActionsAndSystemActionsAreDisjunct() {
        var intersection = new HashSet<>(NotarizationRequestAction.NotaryActions);
        intersection.retainAll(NotarizationRequestAction.SystemActions);

        assertThat(intersection, equalTo(Set.of()));
    }
    
    @Test
    public void testFailure() {
        var intersection = new HashSet<>(NotarizationRequestAction.NotaryActions);
        intersection.retainAll(NotarizationRequestAction.SystemActions);

        assertThat(intersection, equalTo(Set.of()));
    }
}
