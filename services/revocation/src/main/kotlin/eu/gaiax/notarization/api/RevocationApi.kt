/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.api

import com.fasterxml.jackson.annotation.JsonProperty
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.DELETE
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.QueryParam
import eu.gaiax.notarization.db.Lists
import eu.gaiax.notarization.db.ListEntry
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import javax.ws.rs.Consumes
import javax.ws.rs.DefaultValue


@Path("status")
interface ListStatus {

    @Operation(
        summary = "Retrieves the current ListCredential",
        description = "The URL pointing to this list is the statusListCredential value in the StatusListEntry.",
    )
    @APIResponse(
        responseCode = "200",
        description = "Returns the currently issued ListCredential for the requested list.",
    )
    @APIResponse(
        responseCode = "404",
        description = "Returned if either the list does not exists, or no ListCredential has been issued yet.",
    )

    @Path("{listName}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun getList(@PathParam("listName") listName: String): String

}

data class ListMapping(
    @JsonProperty("profile-name")
    var profileName: String,
    @JsonProperty("list-name")
    var listName: String,
)

@Path("management")
interface ListManagement {

    @Operation(
        summary = "Listing of the managed lists",
        description = "Retrieves a list of pairs containing the profile identifier and the associated list name.",
    )

    @Path("lists")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun getLists(): List<ListMapping>


    @Operation(
        summary = "Registers a new list",
        description = "Register a new list for the given profile. This call initiates the database for the new list. A ListCredential is not created.",
    )
    @APIResponse(
        responseCode = "200",
        description = "Returns the list name of the newly created list.",
    )
    @APIResponse(
        responseCode = "409",
        description = "Returned in case the profile already exists.",
    )
    @APIResponse(
        responseCode = "500",
        description = "Returned in case the profile could not be created.",
    )

    @Path("lists")
    @POST
    fun registerList(
        @QueryParam("profile") profileName: String,
        @QueryParam("issue-list-credential") @DefaultValue("true") issueCredential: Boolean,
    ): String


    @Operation(
        summary = "Start issuance of all ListCredentials",
        description = "Given the right conditions (e.i. renewal interval reached and changes in the revocation data present), a new ListCredential is requested from the issuance service. This call checks the conditions for all managed lists.",
    )
    @APIResponse(
        responseCode = "204",
        description = "Returned when the issuance process is finished.",
    )

    @Path("lists/issue-credentials")
    @POST
    fun issueCredentials()


    @Operation(
        summary = "Start issuance of one specific ListCredential",
        description = """
            Given the right conditions (e.i. renewal interval reached and changes in the revocation data present), a new
            ListCredential is requested from the issuance service. This call checks the conditions for one managed list.
        """,
    )
    @APIResponse(
        responseCode = "204",
        description = "Returned when the issuance process is finished.",
    )
    @APIResponse(
        responseCode = "404",
        description = "Returned if the profile does not exist.",
    )

    @Path("lists/issue-credential/{profileName}")
    @POST
    fun issueCredential(
        profileName: String,
        @QueryParam("force") @DefaultValue("false") force: Boolean
    )


    @Path("lists/{profileName}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun getListDefinition(
        @PathParam("profileName") profileName: String
    ): Lists


    @Operation(
        summary = "Register a new credential with the list",
        description = "When this method is called, a new list entry is created and the CredentialStatus element described in the W3C specification is returned, so that it can be included in the new credential.",
    )
    @APIResponse(
        responseCode = "200",
        description = "Returns the CredentialStatus object of the added entry.",
        content = [
            Content(
                mediaType = "application/json",
                schema = Schema(implementation = CredentialStatus::class),
                example = """
                    {
                        "type": "StatusList2021Entry",
                        "statusPurpose": "revocation",
                        "statusListIndex": "94567",
                        "statusListCredential": "https://revocation.example.com/status/somelistname"
                    }
                """,
            )
        ]
    )
    @APIResponse(
        responseCode = "404",
        description = "Returned if the list does not exists.",
    )

    @Path("lists/{profileName}/entry")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    fun addStatusEntry(
        @PathParam("profileName") profileName: String,
    ): CredentialStatus


    @Path("lists/{profileName}/encoded")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun getEncodedList(
        @PathParam("profileName") profileName: String,
    ): String


    @Path("lists/{profileName}/entry/{idx}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun getStatusEntry(
        @PathParam("profileName") profileName: String,
        @PathParam("idx") idx: Long,
    ): ListEntry


    @Operation(
        summary = "Revokes the referenced Credential",
        description = "The URL pointing to this list is the statusListCredential value in the StatusListEntry.",
    )
    @APIResponse(
        responseCode = "204",
        description = "Returned if the entry has been set to revoked.",
    )
    @APIResponse(
        responseCode = "404",
        description = "Returned if neither the list, nor the index in this list exists.",
    )

    @Path("lists/{profileName}/entry/{idx}")
    @DELETE
    fun revoke(
        @PathParam("profileName") profileName: String,
        @PathParam("idx") idx: Long,
    )

}

class CredentialStatus {
    @JsonProperty("id")
    var statusId: String? = null

    @JsonProperty("type")
    var credentialType: String = "StatusList2021Entry"

    @JsonProperty("statusPurpose")
    var statusPurpose: String = "revocation"

    @JsonProperty("statusListIndex")
    var index: String? = null

    @JsonProperty("statusListCredential")
    var listUrl: String? = null
}

class ListCredentialRequestData {
    @JsonProperty("id")
    var listId: String? = null
    var subject: ListCredentialSubject? = null
}

class ListCredentialSubject {
    @JsonProperty("id")
    var credentialId: String? = null

    @JsonProperty("type")
    var credentialType: String = "StatusList2021"

    @JsonProperty("statusPurpose")
    var statusPurpose: String = "revocation"

    @JsonProperty("encodedList")
    var encodedList: String? = null
}

@RegisterRestClient(configKey = "ssi-issuance-api")
interface IssuanceController {

    @Path("list-credential/{profileName}/issue")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun issueListCredential(
        @PathParam("profileName") profileName: String,
        credentialRequest: ListCredentialRequestData
    ): String

}
