/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization

import eu.gaiax.notarization.api.IssuanceController
import eu.gaiax.notarization.api.ListCredentialRequestData
import eu.gaiax.notarization.api.ListCredentialSubject
import eu.gaiax.notarization.db.*
import io.quarkus.logging.Log
import org.eclipse.microprofile.rest.client.inject.RestClient
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.time.Instant
import java.util.*
import java.util.stream.Stream
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.transaction.Transactional


@ApplicationScoped
class ListCredentialIssueService {

    @Inject
    lateinit var listsRepo: ListsRepo

    @Inject
    lateinit var listEntryRepo: ListEntryRepo

    @Inject
    lateinit var listService: ListService

    @Inject
    @RestClient
    lateinit var issuanceController: IssuanceController

    fun issueCredential(list: Lists) {
        var list = list
        val updateStart = Instant.now()

        if (list.listCredential != null) {
            // check if we really need an update and abort if it is unneeded
            if (! listEntryRepo.hasUnprocessedEntries(list)) {
                Log.debugv("No ListCredential update needed for profile {0}, due to lack of new entries.", list.profileName)
                return
            }
        }

        Log.info("Starting to issue new ListCredential for profile ${list.profileName}.")

        val encoded = calculateBitSet(list)
        // save encoded list, in case issuance fails
        list = listService.updateEncodedList(list, encoded)

        val listUrl = listService.createStatusUrl(list.listName)
        val requestData = ListCredentialRequestData().apply {
            listId = listUrl
            subject = ListCredentialSubject().apply {
                credentialId = "$listUrl#list"
                encodedList = encoded
            }
        }

        val credential = issuanceController.issueListCredential(list.profileName, requestData)

        list = listService.updateListCredential(list, credential, updateStart)
        Log.info("Finished issuing new ListCredential for profile ${list.profileName}.")
    }

    fun calculateBitSet(list: Lists, updateEntries: Boolean = true): String {
        val listBytes = calculateBitSetBytes(list, updateEntries)
        return encodeBitset(listBytes)
    }

    @Transactional
    protected fun calculateBitSetBytes(list: Lists, updateEntries: Boolean): ByteArray {
        val oldBitSet = getOldBitset(list)
        val changedEntries = listEntryRepo.findUnprocessedEntries(list)
        val numBytes = listService.determineBitstringNumBytes(list)
        return buildBitSet(oldBitSet, changedEntries, numBytes, updateEntries)
    }

    fun getOldBitset(list: Lists): BitSet {
        with (list.encodedList) {
            if (this == null) {
                return BitSet()
            } else {
                return decodeBitset(this)
            }
        }
    }

    private fun buildBitSet(bitset: BitSet, changedEntries: Stream<ListEntry>, bitStringNumBytes: Int, updateEntries: Boolean): ByteArray {
        // update bitset
        for (nextEntry in changedEntries) {
            bitset.set(nextEntry.index!!.toInt())
            if (updateEntries) {
                nextEntry.processed = true
            }
        }

        val bitsetBytes = bitset.toByteArray()


        // safeguard in case we have more entries that anticipated (ORM cache fuckups?!?)
        return if (bitsetBytes.size > bitStringNumBytes) {
            bitsetBytes
        } else {
            val targetBytes = ByteArray(bitStringNumBytes)
            bitsetBytes.copyInto(targetBytes)
            targetBytes
        }
    }

}

fun encodeBitset(bitArray: ByteArray): String {
    // gzip data
    val zipped = with(ByteArrayOutputStream()) {
        GZIPOutputStream(this).use {
            it.write(bitArray)
        }
        this.toByteArray()
    }

    return Base64.getEncoder().encodeToString(zipped)
}

fun decodeBytes(encoded: String): ByteArray {
    val rawBytes = Base64.getDecoder().decode(encoded)

    return ByteArrayInputStream(rawBytes).run {
        GZIPInputStream(this).use {
            it.readBytes()
        }
    }
}

fun decodeBitset(encoded: String): BitSet {
    val unzipped = decodeBytes(encoded)
    return BitSet.valueOf(unzipped)
}
