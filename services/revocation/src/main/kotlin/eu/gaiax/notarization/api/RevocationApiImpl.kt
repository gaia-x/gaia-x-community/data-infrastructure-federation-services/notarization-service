/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.api

import eu.gaiax.notarization.ListCredentialIssueService
import eu.gaiax.notarization.RevocationConfig
import eu.gaiax.notarization.db.*
import io.quarkus.logging.Log
import java.time.Instant
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.Response


class ListStatusImpl : ListStatus {

    @Inject
    lateinit var listRepo: ListsRepo

    override fun getList(listName: String): String {
        val list = resolveListByName(listRepo, listName)
        return list.listCredential ?: throw WebApplicationException(
            "ListCredential has not been created yet.", Response.Status.NOT_FOUND
        )
    }

}


class ListManagementImpl : ListManagement {

    @Inject
    lateinit var config: RevocationConfig

    @Inject
    lateinit var listRepo: ListsRepo

    @Inject
    lateinit var entryRepo: ListEntryRepo

    @Inject
    lateinit var listService: ListService

    @Inject
    lateinit var issuerService: ListCredentialIssueService

    override fun getLists(): List<ListMapping> {
        return listRepo.listAll().map {
            ListMapping(it.profileName, it.listName)
        }
    }

    override fun registerList(profileName: String, issueCredential: Boolean): String {
        // get list for requested profile, if it exists we return with conflict
        listRepo.findByProfileName(profileName)?.let {
            // at this point we know, that the profile is registered, answer with 409 Conflict
            Log.warn("Profile $profileName already exists, skipping its creation.")
            throw WebApplicationException("Profile $profileName already exists.", Response.Status.CONFLICT)
        }

        try {
            Log.info("Registering status list for profile '$profileName'.")
            val newList = listService.createList(profileName)

            if (issueCredential) {
                try {
                    Log.info("Trying to issue new list credential for profile '$profileName'.")
                    issuerService.issueCredential(newList)
                } catch (ex: Exception) {
                    Log.error("Failed to issue new ListCredential for profile ${newList.profileName}.", ex)
                }
            }

            Log.info("Successfully registered profile '$profileName'.")

            return newList.listName
        } catch (ex: Exception) {
            Log.error("Failed to register status list for profile '$profileName'.", ex)
            throw WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR)
        }
    }

    override fun issueCredentials() {
        for (newList in listRepo.listAll()) {
            issueCredentialInternal(newList, false)
        }
    }

    override fun issueCredential(profileName: String, force: Boolean) {
        val newList = resolveListByProfile(listRepo, profileName)
        issueCredentialInternal(newList, force)
    }

    private fun issueCredentialInternal(newList: Lists, force: Boolean) {
        Log.debug("Checking if ListCredential issuance is needed for ${newList.profileName} ...")
        if (force || newList.lastUpdate.isBefore(Instant.now().minus(config.minIssueInterval()))) {
            try {
                issuerService.issueCredential(newList)
            } catch (ex: Exception) {
                Log.error("Failed to issue new ListCredential for profile ${newList.profileName}.", ex)
            }
        } else {
            Log.debugv("No ListCredential update needed for profile {0}, due to renewal interval.", newList.profileName)
        }
    }

    override fun getListDefinition(profileName: String): Lists {
        return resolveListByProfile(listRepo, profileName)
    }

    override fun addStatusEntry(profileName: String): CredentialStatus {
        Log.debugv("Adding status entry to profile {0}.", profileName)
        val listRef = resolveListByProfile(listRepo, profileName)
        val entryIndex = listService.addEntry(listRef)

        return CredentialStatus().apply {
            index = entryIndex.toString()
            listUrl = listService.createStatusUrl(listRef.listName)
            statusId = "$listUrl#$index"
        }.also {
            Log.debugv("Successfully added entry to status list with profile {0}.", profileName)
        }
    }

    override fun getStatusEntry(profileName: String, idx: Long): ListEntry {
        val listRef = resolveListByProfile(listRepo, profileName)
        return entryRepo.findByIndex(listRef, idx)
            ?: throw WebApplicationException("Requested status entry does not exist.", Response.Status.NOT_FOUND)
    }

    override fun revoke(profileName: String, idx: Long) {
        Log.debugv("Revoking status entry with index {0} of profile {1}.", idx, profileName)
        val listRef = resolveListByProfile(listRepo, profileName)
        listService.revokeEntry(listRef, idx)
            .also {
                Log.debugv("Successfully revoked entry in status list with index {0} of profile {1}.", idx, profileName)
            }
    }

    override fun getEncodedList(
        profileName: String,
    ): String {
        val listRef = resolveListByProfile(listRepo, profileName)
        return issuerService.calculateBitSet(listRef, updateEntries = false)
    }

}


private fun resolveListByName(listRepo: ListsRepo, listName: String): Lists {
    return listRepo.findByListName(listName)
        ?: throw WebApplicationException("Requested status list does not exist.", Response.Status.NOT_FOUND)
}

private fun resolveListByProfile(listRepo: ListsRepo, profileName: String): Lists {
    return listRepo.findByProfileName(profileName)
        ?: throw WebApplicationException("Requested status list does not exist.", Response.Status.NOT_FOUND)
}
