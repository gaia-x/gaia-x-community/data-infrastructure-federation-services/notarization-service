version: "3"
services:

  # Jaeger
  jaeger-all-in-one:
    image: jaegertracing/all-in-one:1.34.1
    container_name: jaegertracing
    ports:
      - "16686:16686"
      - "14268"
      - "14250"

  # OpenTelemetry Collector
  otel-collector:
    image: otel/opentelemetry-collector:0.50.0
    container_name: opentelemetry-collector
    command: ["--config=/etc/otel-collector-config.yaml"]
    volumes:
      - ../../../deploy/resources/config/otel-collector-config.yaml:/etc/otel-collector-config.yaml
    ports:
      - "13133:13133"  # Health_check extension
      - "4317:4317"    # OTLP gRPC receiver
      - "55680:55680"  # OTLP gRPC receiver alternative port
    depends_on:
      - jaeger-all-in-one

  revocation-db:
    image: postgres:14
    container_name: revocation-db
    ports:
      - "5432"
    environment:
      POSTGRES_USER: revocation
      POSTGRES_PASSWORD: revocation
      POSTGRES_DB: revocation
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U revocation -d revocation"]
      interval: 5s
      timeout: 5s
      retries: 5

  revocation-flyway:
    image: flyway/flyway:8.5
    container_name: revocation-flyway
    command: "-locations=filesystem:/sql-migrations -url=jdbc:postgresql://revocation-db:5432/revocation -schemas=public -user=revocation -password=revocation -connectRetries=60 migrate"
    volumes:
      - ../../../services/revocation/src/main/jib/db-flyway/:/sql-migrations
    depends_on:
      revocation-db:
        condition: service_healthy

  revocation-java17:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/revocation:java17-latest
    container_name: revocation-java17
    ports:
      - "8086:8086"
    depends_on:
      - revocation-flyway
    environment:
      QUARKUS_HTTP_PORT: 8086
      QUARKUS_HTTP_ACCESS_LOG_ENABLED: "true"
      QUARKUS_DATASOURCE_USERNAME: revocation
      QUARKUS_DATASOURCE_PASSWORD: revocation
      QUARKUS_DATASOURCE_JDBC_URL: jdbc:postgresql://revocation-db:5432/revocation
      QUARKUS_REST_CLIENT_SSI_ISSUANCE_API_URL: "${SSI_SERVICE_URL:-http://ssi-issuance:8080}"
      REVOCATION_BASE_URL: http://localhost:8086
      REVOCATION_MIN_ISSUE_INTERVAL: PT0S
      QUARKUS_OPENTELEMETRY_ENABLED: "true"
      QUARKUS_OPENTELEMETRY_TRACER_EXPORTER_OTLP_ENDPOINT: http://otel-collector:4317
    restart: on-failure
    networks:
      default:
        aliases:
          - revocation
    deploy:
      resources:
        limits:
          memory: 1G
          cpus: '1'
        reservations:
          memory: 256M
          cpus: '0.25'
