#!/bin/bash -ex

# Create the deploy/docker-compose files for each version of each of the Quarkus services
# Then add on the ui-super-heroes


SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

PROJECT_ROOT="${SCRIPT_DIR}/.."

INPUT_DIR=src/main/docker-compose
LOCAL_OUTPUT_DIR=deploy/local/docker-compose
E2E_OUTPUT_DIR=deploy/e2e/docker-compose
OUTPUT_DIRS=("${LOCAL_OUTPUT_DIR}" "${E2E_OUTPUT_DIR}")

COMMON_INFRA_FILE="deploy/resources/docker-compose/infra.yaml"

create_output_file() {
  local output_file=$1

  echo "Creating output file: $output_file"

  touch $output_file
  echo 'version: "3"' >> $output_file
  echo 'services:' >> $output_file
  cat "${COMMON_INFRA_FILE}" >> $output_file
}

# create_project_output(project, javaVersion, kind)
# 
# Produce files for the given project
create_project_output() {
  local project=$1
  local javaVersion=$2
  local kind=$3
  local filename="${kind}java${javaVersion}"
  local infra_input_file="services/$project/$INPUT_DIR/infra.yml"
  local input_file_name="$filename.yml"
  local version_file_name="${kind}java${javaVersion}.yml"
  local project_input_file="services/$project/$INPUT_DIR/$input_file_name"
  local project_output_file="services/$project/deploy/docker-compose/$input_file_name"

  echo ""
  echo "-----------------------------------------"
  echo "Creating output for project $project"

  if [[ -f "$project_output_file" ]]; then
    rm -rf $project_output_file
  fi

  create_output_file $project_output_file

  if [[ -f "$infra_input_file" ]]; then
    cat $infra_input_file >> $project_output_file

    for output_dir in "${OUTPUT_DIRS[@]}"
    do
      local all_apps_output_file="$output_dir/$version_file_name"
      if [[ ! -f "$all_apps_output_file" ]]; then
        create_output_file $all_apps_output_file
      fi
      cat $infra_input_file >> $all_apps_output_file
    done
  fi

  if [[ -f "$project_input_file" ]]; then
    cat $project_input_file >> $project_output_file

    for output_dir in "${OUTPUT_DIRS[@]}"
    do
      local all_apps_output_file="$output_dir/$version_file_name"
      if [[ ! -f "$all_apps_output_file" ]]; then
        create_output_file $all_apps_output_file
      fi

      cat $project_input_file >> $all_apps_output_file
    done
  fi
}

cd "${PROJECT_ROOT}"

for output_dir in "${OUTPUT_DIRS[@]}"
do
  find "${output_dir}" \( -iregex ".*ya?ml" -not -iregex ".*acapy-holder.ya?ml" -not -iregex ".*acapy.ya?ml" -not -iregex ".*ledger.ya?ml" -not -iregex ".*ssi-issuance.ya?ml" \) -exec rm {} \;
done

for project in "profile" "request-processing" "oidc-identity-resolver" "scheduler" "revocation"
do
  for javaVersion in 17
  do
    for kind in "" "native-"
    do
      create_project_output $project $javaVersion $kind
    done
  done
done
