version: "3"
services:

  # Jaeger
  jaeger-all-in-one:
    image: jaegertracing/all-in-one:1.34.1
    container_name: jaegertracing
    ports:
      - "16686:16686"
      - "14268"
      - "14250"

  # OpenTelemetry Collector
  otel-collector:
    image: otel/opentelemetry-collector:0.50.0
    container_name: opentelemetry-collector
    command: ["--config=/etc/otel-collector-config.yaml"]
    volumes:
      - ../../../deploy/resources/config/otel-collector-config.yaml:/etc/otel-collector-config.yaml
    ports:
      - "13133:13133"  # Health_check extension
      - "4317:4317"    # OTLP gRPC receiver
      - "55680:55680"  # OTLP gRPC receiver alternative port
    depends_on:
      - jaeger-all-in-one

  profile-db:
    image: postgres:14
    container_name: profile-db
    ports:
      - "35433:5432"
    environment:
      POSTGRES_USER: profile
      POSTGRES_PASSWORD: profile
      POSTGRES_DB: profile_database
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U profile -d profile_database"]
      interval: 5s
      timeout: 5s
      retries: 5

  profile-flyway:
    image: flyway/flyway:8.5
    container_name: profile-flyway
    command: "-locations=filesystem:/sql-migrations -url=jdbc:postgresql://profile-db:5432/profile_database -schemas=public -user=profile -password=profile -connectRetries=60 migrate"
    volumes:
      - ../../../services/profile/src/main/jib/db-flyway/:/sql-migrations
    depends_on:
      profile-db:
        condition: service_healthy

  profile-native-java17:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/profile:native-java17-latest
    container_name: profile-native-java17
    depends_on:
      - profile-flyway
    ports:
      - "8083:8083"
    restart: on-failure
    environment:
      "QUARKUS_HTTP_PORT": 8083
      "QUARKUS_HTTP_ACCESS_LOG_ENABLED": "true"
      QUARKUS_DATASOURCE_USERNAME: profile
      QUARKUS_DATASOURCE_PASSWORD: profile
      QUARKUS_DATASOURCE_REACTIVE_URL: postgresql://profile-db:5432/profile_database
      QUARKUS_REST_CLIENT_SSI_ISSUANCE_API_URL: "${SSI_SERVICE_URL:-http://ssi-issuance:8080}"
      QUARKUS_REST_CLIENT_REVOCATION_API_URL: http://revocation:8086
      QUARKUS_OPENTELEMETRY_TRACER_EXPORTER_OTLP_ENDPOINT: http://otel-collector:4317
    volumes:
      - ../../../services/profile/deploy/config/application-example-docker-compose.yaml:/home/jboss/config/application.yaml
    networks:
      default:
        aliases:
          - profile
    deploy:
      resources:
        limits:
          memory: 256M
          cpus: '0.25'
        reservations:
          memory: 128M
          cpus: '0.10'
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:8083/q/health/ready"]
      interval: 20s
      timeout: 20s
      retries: 5

  dss:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/dss-demo-webapp:5.10.1
    container_name: dss
    ports:
      - "8080"

  request-processing-db:
    image: postgres:14
    container_name: request-processing-db
    ports:
      - "5434:5432"
    environment:
      POSTGRES_USER: request
      POSTGRES_PASSWORD: request
      POSTGRES_DB: requests_database
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U request -d requests_database"]
      interval: 5s
      timeout: 5s
      retries: 5

  request-processing-flyway:
    image: flyway/flyway:8.5
    container_name: request-processing-flyway
    command: "-locations=filesystem:/sql-migrations -url=jdbc:postgresql://request-processing-db:5432/requests_database -schemas=public -user=request -password=request -connectRetries=60 migrate"
    volumes:
      - ../../../services/request-processing/src/main/jib/db-flyway/:/sql-migrations
    depends_on:
      request-processing-db:
        condition: service_healthy

  rabbitmq:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/rabbitmq-dev:3.9-management
    container_name: 'rabbitmq'
    ports:
        - 5671:5671   # amqp/tls
        - 5672:5672   # amqp
        - 15672:15672 # http
        - 15692:15692 # prometheus
    environment:
      RABBITMQ_DEFAULT_USER: request-rabbit
      RABBITMQ_DEFAULT_PASS: request-rabbit-password
    volumes:
      - ../../../services/request-processing/deploy/docker-compose/infra/rabbitmq.conf:/etc/rabbitmq/rabbitmq.conf
      - ../../../services/request-processing/deploy/docker-compose/infra/rabbitmq_definitions.json:/etc/rabbitmq/definitions.json
      - ../../../services/request-processing/deploy/docker-compose/infra/ca_certificate.pem:/notarization/ca_certificate.pem
      - ../../../services/request-processing/deploy/docker-compose/infra/server_notarization_certificate.pem:/notarization/server_certificate.pem
      - ../../../services/request-processing/deploy/docker-compose/infra/server_notarization_key.pem:/notarization/server_key.pem

  keycloak:
    image: quay.io/keycloak/keycloak:${KEYCLOAK_VERSION:-18.0.0}
    ports:
      - "${KEYCLOAK_PORT:-9194}:${KEYCLOAK_PORT:-9194}"
    command: "start-dev --http-port=${KEYCLOAK_PORT:-9194} --log-level=INFO"
    environment:
      - KEYCLOAK_ADMIN=${KEYCLOAK_ADMIN:-keycloak}
      - KEYCLOAK_PORT=${KEYCLOAK_PORT:-9194}
      - KEYCLOAK_ADMIN_PASSWORD=${KEYCLOAK_ADMIN_PASSWORD:-keycloakcd}
      - KC_HOSTNAME=localhost
      - DB_DATABASE=${KEYCLOAK_DATABASE_NAME:-keycloakdb}
      - DB_USER=${KEYCLOAK_DATABASE_USER:-keycloakdb}
      - DB_PASSWORD=${KEYCLOAK_DATABASE_PASSWORD:-keycloakdb}
      - DB_ADDR=keycloakdb
      - DB_VENDOR=postgres
    networks:
      default:
        aliases:
          - keycloak
    depends_on:
      - keycloakdb
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:${KEYCLOAK_PORT:-9194}"]
      start_period: 50s
      interval: 5s
      timeout: 5s
      retries: 150

  keycloakdb:
    image: postgres:${POSTGRES_VERSION:-14}
    container_name: keycloak-db
    ports:
      - "5433:5432"
    environment:
      - POSTGRES_USER=${KEYCLOAK_DATABASE_USER:-keycloakdb}
      - POSTGRES_PASSWORD=${KEYCLOAK_DATABASE_PASSWORD:-keycloakdb}
      - POSTGRES_DB=${KEYCLOAK_DATABASE_NAME:-keycloakdb}
    networks:
      default:

  keycloak-initial-config-importer:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/keycloak-jq:18.0.0
    container_name: keycloak-config
    depends_on:
      keycloak:
        condition: service_healthy
    environment:
      - KEYCLOAK_ADMIN=${KEYCLOAK_ADMIN:-keycloak}
      - KEYCLOAK_ADMIN_PASSWORD=${KEYCLOAK_ADMIN_PASSWORD:-keycloakcd}
      - KEYCLOAK_PORT=9194
      - KC_HOSTNAME=keycloak
    volumes:
      - ../../../services/request-processing/deploy/docker-compose/infra/keycloak_init.sh:/tmp/keycloak_init.sh
    restart: on-failure
    entrypoint: "/tmp/keycloak_init.sh"
    networks:
      default:

  request-processing-native-java17:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/request-processing:native-java17-latest
    container_name: request-processing-java17
    depends_on:
      - request-processing-flyway
      - keycloak-initial-config-importer
    ports:
      - "8084:8084"
    environment:
      QUARKUS_HTTP_PORT: 8084
      QUARKUS_HTTP_ACCESS_LOG_ENABLED: "true"
      QUARKUS_REST_CLIENT_PROFILE_API_URL: http://profile:8083/
      BROWSER_IDENTIFICATION_URL: http://oidc-identity-resolver:8085/session/
      VC_IDENTIFICATION_URL: "${SSI_SERVICE_URL:-http://ssi-issuance:8080}/credential/verify"
      QUARKUS_REST_CLIENT_LOGGING_SCOPE: request-response
      QUARKUS_REST_CLIENT_LOGGING_BODY_LIMIT: 500
      QUARKUS_HTTP_ACCESS_LOG_EXCLUDE_PATTERN: (\/api\/v1\/routines\/deleteTimeout|\/api\/v1\/routines\/deleteSubmitTimeout|\/api\/v1\/routines\/deleteTerminated)
      QUARKUS_LOG_CATEGORY__ORG_JBOSS_RESTEASY_REACTIVE_CLIENT_LOGGING__LEVEL: DEBUG
      QUARKUS_REST_CLIENT_SSI_ISSUANCE_API_URL: "${SSI_SERVICE_URL:-http://ssi-issuance:8080}"
      QUARKUS_REST_CLIENT_REVOCATION_API_URL: http://revocation:8086
      QUARKUS_DATASOURCE_REACTIVE_URL: postgresql://request-processing-db:5432/requests_database
      QUARKUS_HIBERNATE_ORM_DATABASE_GENERATION: none
      QUARKUS_DATASOURCE_USERNAME: request
      QUARKUS_DATASOURCE_PASSWORD: request
      QUARKUS_OIDC_AUTH_SERVER_URL: http://keycloak:9194/realms/notarization-realm
      QUARKUS_OIDC_CLIENT_ID: notarization-client
      QUARKUS_OIDC_CREDENTIALS_SECRET: notarization-api-secret-12345
      QUARKUS_OIDC_DISCOVERY_ENABLED: false
      QUARKUS_OIDC_INTROSPECTION_PATH: http://keycloak:9194/realms/notarization-realm/protocol/openid-connect/token/introspect
      RABBITMQ_HOST: rabbitmq
      RABBITMQ_PORT: 5672
      RABBITMQ_USERNAME: request-processing-user
      RABBITMQ_PASSWORD: request-rabbit-password
      NOTARIZATION_PROCESSING_INTERNAL_URL: http://request-processing:8084
      NOTARIZATION_RABBITMQ_SSL: "false"
      NOTARIZATION_RABBITMQ_CA_PATH: /tmp/ca_certificate.pem
      NOTARIZATION_RABBITMQ_TLS_CERT_PATH: /tmp/client_certificate.pem
      NOTARIZATION_RABBITMQ_TLS_KEY_PATH: /tmp/client_key.pem
      QUARKUS_OPENTELEMETRY_TRACER_EXPORTER_OTLP_ENDPOINT: http://otel-collector:4317
      QUARKUS_REST_CLIENT_DSS_API_URL: http://dss:8080/services/rest
    volumes:
      - ../../../services/request-processing/deploy/docker-compose/infra/ca_certificate.pem:/tmp/ca_certificate.pem
      - ../../../services/request-processing/deploy/docker-compose/infra/client_notarization_certificate.pem:/tmp/client_certificate.pem
      - ../../../services/request-processing/deploy/docker-compose/infra/client_notarization_key.pem:/tmp/client_key.pem
    restart: on-failure
    networks:
      default:
        aliases:
          - request-processing
    deploy:
      resources:
        limits:
          memory: 1G
          cpus: '1'
        reservations:
          memory: 256M
          cpus: '0.5'
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:8084/q/health/ready"]
      interval: 20s
      timeout: 20s
      retries: 5

  oidc-identity-resolver-db:
    image: postgres:14
    container_name: oidc-identity-resolver-db
    ports:
      - "5432"
    environment:
      POSTGRES_USER: oidc-identity-resolver
      POSTGRES_PASSWORD: oidc-identity-resolver
      POSTGRES_DB: demo_identity_oidc_database
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U oidc-identity-resolver -d demo_identity_oidc_database"]
      interval: 5s
      timeout: 5s
      retries: 5

  oidc-identity-resolver-flyway:
    image: flyway/flyway:8.5
    container_name: oidc-identity-resolver-flyway
    command: "-locations=filesystem:/sql-migrations -url=jdbc:postgresql://oidc-identity-resolver-db:5432/demo_identity_oidc_database -schemas=public -user=oidc-identity-resolver -password=oidc-identity-resolver -connectRetries=60 migrate"
    volumes:
      - ../../../services/oidc-identity-resolver/src/main/jib/db-flyway/:/sql-migrations
    depends_on:
      oidc-identity-resolver-db:
        condition: service_healthy

  oidc-identity-resolver-java17:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/oidc-identity-resolver:native-java17-latest
    container_name: oidc-identity-resolver-java17
    depends_on:
    - oidc-identity-resolver-db
    ports:
      - "8085:8085"
    environment:
      QUARKUS_HTTP_PORT: 8085
      QUARKUS_HTTP_ACCESS_LOG_ENABLED: "true"
      # OIDC
      QUARKUS_OIDC_CLIENT_ID: portal-client
      QUARKUS_OIDC_CREDENTIALS_SECRET: portal-secret-12345
      QUARKUS_OIDC_AUTH_SERVER_URL: "http://keycloak:9194/realms/notarization-realm"
      DEMO_IDENTITY_OIDC_EXTERNAL_URL: http://localhost:8085
      REDIRECT_LOGIN_SUCCESS_URL: http://localhost:30123
      REDIRECT_LOGIN_FAILURE_URL: http://localhost:30124
      # Database
      QUARKUS_DATASOURCE_REACTIVE_URL: postgresql://oidc-identity-resolver-db:5432/demo_identity_oidc_database
      # QUARKUS_HIBERNATE_ORM_DATABASE_GENERATION: validate
      QUARKUS_HIBERNATE_ORM_DATABASE_GENERATION: drop-and-create
      QUARKUS_DATASOURCE_USERNAME: oidc-identity-resolver
      QUARKUS_DATASOURCE_PASSWORD: oidc-identity-resolver
    restart: on-failure
    networks:
      default:
        aliases:
          - oidc-identity-resolver
    deploy:
      resources:
        limits:
          memory: 1G
          cpus: '1'
        reservations:
          memory: 256M
          cpus: '0.25'

  scheduler-db:
    image: postgres:14
    container_name: scheduler-db
    ports:
      - "5432"
    environment:
      POSTGRES_USER: scheduler
      POSTGRES_PASSWORD: scheduler
      POSTGRES_DB: scheduler_database
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U scheduler -d scheduler_database"]
      interval: 5s
      timeout: 5s
      retries: 5

  scheduler-flyway:
    image: flyway/flyway:8.5
    container_name: scheduler-flyway
    command: "-locations=filesystem:/sql-migrations -url=jdbc:postgresql://scheduler-db:5432/scheduler_database -schemas=public -user=scheduler -password=scheduler -connectRetries=60 -baselineOnMigrate=true -table=flyway_quarkus_history migrate"
    volumes:
      - ../../../services/scheduler/src/main/jib/db-flyway/:/sql-migrations
    depends_on:
      scheduler-db:
        condition: service_healthy

  scheduler-native-java17:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/scheduler:java17-latest
    container_name: scheduler-native-java17
    depends_on:
      request-processing-native-java17:
        condition: service_healthy
      scheduler-db:
        condition: service_healthy
      profile-native-java17:
        condition: service_healthy
    ports:
      - "8087:8087"
    restart: on-failure
    environment:
      "QUARKUS_HTTP_PORT": 8087
      "QUARKUS_HTTP_ACCESS_LOG_ENABLED": "true"
      QUARKUS_OPENTELEMETRY_TRACER_EXPORTER_OTLP_ENDPOINT: http://otel-collector:4317
      QUARKUS_REST_CLIENT_REQUESTPROCESSING_API_URL: http://request-processing:8084
      QUARKUS_REST_CLIENT_REVOCATION_API_URL: http://revocation:8086
      QUARKUS_REST_CLIENT_PROFILE_API_URL: http://profile:8083
      QUARKUS_QUARTZ_CLUSTERED: "true"
      QUARKUS_QUARTZ_STORE_TYPE: jdbc-cmt
      # Datasource configuration.
      QUARKUS_DATASOURCE_DB_KIND: postgresql
      QUARKUS_DATASOURCE_USERNAME: scheduler
      QUARKUS_DATASOURCE_PASSWORD: scheduler
      QUARKUS_DATASOURCE_JDBC_URL: jdbc:postgresql://scheduler-db:5432/scheduler_database
      # Hibernate configuration
      QUARKUS_HIBERNATE_ORM_DATABASE_GENERATION: none
      QUARKUS_HIBERNATE_ORM_LOG_SQL: "true"
      QUARKUS_HIBERNATE_ORM_SQL_LOAD_SCRIPT: no-file
      # flyway configuration
      QUARKUS_FLYWAY_CONNECT_RETRIES: 10
      QUARKUS_FLYWAY_TABLE: flyway_quarkus_history
      QUARKUS_FLYWAY_MIGRATE_AT_START: "true"
      QUARKUS_FLYWAY_BASELINE_ON_MIGRATE: "true"
      QUARKUS_FLYWAY_BASELINE_VERSION: "1_0"
      QUARKUS_FLYWAY_BASELINE_DESCRIPTION: Quartz
      # quarz cron style (Seconds Minutes Hours 'Day Of Month' Month 'Day Of Week' Year)
      CRON_PRUNE_TERMINATED: "*/10 * * ? * * *"
      CRON_PRUNE_TIMEOUT: "*/5 * * ? * * *"
      CRON_PRUNE_SUBMISSION_TIMEOUT: "*/2 * * ? * * *"
      CRON_ISSUE_REVOCATION_CREDENTIALS: "0 * * ? * * *"
      CRON_PROFILE_REQUEST_OUTSTANDING_DIDS: "0 * * ? * * *"
    networks:
      default:
        aliases:
          - scheduler
    deploy:
      resources:
        limits:
          memory: 256M
          cpus: '0.25'
        reservations:
          memory: 128M
          cpus: '0.10'


  revocation-db:
    image: postgres:14
    container_name: revocation-db
    ports:
      - "5432"
    environment:
      POSTGRES_USER: revocation
      POSTGRES_PASSWORD: revocation
      POSTGRES_DB: revocation
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U revocation -d revocation"]
      interval: 5s
      timeout: 5s
      retries: 5

  revocation-flyway:
    image: flyway/flyway:8.5
    container_name: revocation-flyway
    command: "-locations=filesystem:/sql-migrations -url=jdbc:postgresql://revocation-db:5432/revocation -schemas=public -user=revocation -password=revocation -connectRetries=60 migrate"
    volumes:
      - ../../../services/revocation/src/main/jib/db-flyway/:/sql-migrations
    depends_on:
      revocation-db:
        condition: service_healthy

  revocation-native-java17:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/revocation:native-java17-latest
    container_name: revocation-java17
    ports:
      - "8086:8086"
    depends_on:
      - revocation-flyway
    environment:
      QUARKUS_HTTP_PORT: 8086
      QUARKUS_HTTP_ACCESS_LOG_ENABLED: "true"
      QUARKUS_DATASOURCE_USERNAME: revocation
      QUARKUS_DATASOURCE_PASSWORD: revocation
      QUARKUS_DATASOURCE_JDBC_URL: jdbc:postgresql://revocation-db:5432/revocation
      QUARKUS_REST_CLIENT_SSI_ISSUANCE_API_URL: "${SSI_SERVICE_URL:-http://ssi-issuance:8080}"
      REVOCATION_BASE_URL: http://localhost:8086
      REVOCATION_MIN_ISSUE_INTERVAL: PT0S
      QUARKUS_OPENTELEMETRY_ENABLED: "true"
      QUARKUS_OPENTELEMETRY_TRACER_EXPORTER_OTLP_ENDPOINT: http://otel-collector:4317
    restart: on-failure
    networks:
      default:
        aliases:
          - revocation
    deploy:
      resources:
        limits:
          memory: 1G
          cpus: '1'
        reservations:
          memory: 256M
          cpus: '0.25'
