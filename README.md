# Notarization-API
This project has been migrated to Eclipse Foundation, and it can be found under https://gitlab.eclipse.org/eclipse/xfsc/

The purpose of the Notarization API is to provide an authorization officer a software component to attest given master data and transform it to a digital verifiable credential representation. 

These tamper-proof digital claims about certain attributes are central to gaining the desired trust about provided self-descriptions of assets and participants in the distributed Gaia-X ecosystem.

The system provides API's to integrate the notarization component smoothly in external software systems for Non-IT operator usage (e.g., lawyers, notaries, governments, certifiers ...).

## Table of Contents
- [Notarization-API](#notarization-api)
  - [Table of Contents](#table-of-contents)
- [Quick start](#quick-start)
  - [Running Locally via Docker Compose](#running-locally-via-docker-compose)
- [Documentation](#documentation)

# Quick start

## Running Locally via Docker Compose

```bash
$ ./scripts/manage.sh start
```

For more information see [documentation/admin/install-docker-compose.md](./documentation/admin/install-docker-compose.md).

# Documentation

The complete documentation of the production see [documentation/README.md](documentation/README.md).
